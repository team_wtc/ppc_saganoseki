﻿namespace CES.ThermalPTZ
{
    partial class ConfigSetupForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.EnableAlarmBuzzerCheckBox = new System.Windows.Forms.CheckBox();
            this.AlarmTestButton = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.AutoPatrolIntervalNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.TemperatureWaitNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.ConfigSaveButton = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.MoveSpeedHighNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.TemperatureThresholdNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.MoveSpeedLowNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.AreaChangeWaitNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.AlarmCountNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.EnablePositionTweakCheckBox = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.AutoPatrolIntervalNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TemperatureWaitNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MoveSpeedHighNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TemperatureThresholdNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MoveSpeedLowNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AreaChangeWaitNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AlarmCountNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // EnableAlarmBuzzerCheckBox
            // 
            this.EnableAlarmBuzzerCheckBox.AutoSize = true;
            this.EnableAlarmBuzzerCheckBox.Location = new System.Drawing.Point(236, 213);
            this.EnableAlarmBuzzerCheckBox.Name = "EnableAlarmBuzzerCheckBox";
            this.EnableAlarmBuzzerCheckBox.Size = new System.Drawing.Size(301, 28);
            this.EnableAlarmBuzzerCheckBox.TabIndex = 13;
            this.EnableAlarmBuzzerCheckBox.Text = "温度異常発生時に警報ブザーを鳴らす";
            this.EnableAlarmBuzzerCheckBox.UseVisualStyleBackColor = true;
            // 
            // AlarmTestButton
            // 
            this.AlarmTestButton.ForeColor = System.Drawing.Color.Navy;
            this.AlarmTestButton.Location = new System.Drawing.Point(354, 373);
            this.AlarmTestButton.Name = "AlarmTestButton";
            this.AlarmTestButton.Size = new System.Drawing.Size(173, 31);
            this.AlarmTestButton.TabIndex = 24;
            this.AlarmTestButton.Text = "警報テスト";
            this.AlarmTestButton.UseVisualStyleBackColor = true;
            this.AlarmTestButton.Click += new System.EventHandler(this.AlarmTestButton_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.SystemColors.Control;
            this.label4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label4.Location = new System.Drawing.Point(76, 14);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(154, 24);
            this.label4.TabIndex = 0;
            this.label4.Text = "自動巡回実行間隔：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.SystemColors.Control;
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Location = new System.Drawing.Point(322, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(26, 24);
            this.label3.TabIndex = 2;
            this.label3.Text = "秒";
            // 
            // AutoPatrolIntervalNumericUpDown
            // 
            this.AutoPatrolIntervalNumericUpDown.BackColor = System.Drawing.Color.WhiteSmoke;
            this.AutoPatrolIntervalNumericUpDown.ForeColor = System.Drawing.Color.Navy;
            this.AutoPatrolIntervalNumericUpDown.Location = new System.Drawing.Point(236, 12);
            this.AutoPatrolIntervalNumericUpDown.Maximum = new decimal(new int[] {
            3600,
            0,
            0,
            0});
            this.AutoPatrolIntervalNumericUpDown.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.AutoPatrolIntervalNumericUpDown.Name = "AutoPatrolIntervalNumericUpDown";
            this.AutoPatrolIntervalNumericUpDown.Size = new System.Drawing.Size(80, 31);
            this.AutoPatrolIntervalNumericUpDown.TabIndex = 1;
            this.AutoPatrolIntervalNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.AutoPatrolIntervalNumericUpDown.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.SystemColors.Control;
            this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label2.Location = new System.Drawing.Point(322, 106);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(26, 24);
            this.label2.TabIndex = 8;
            this.label2.Text = "秒";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.Control;
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Location = new System.Drawing.Point(76, 106);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(154, 24);
            this.label1.TabIndex = 6;
            this.label1.Text = "温度情報取得間隔：";
            // 
            // TemperatureWaitNumericUpDown
            // 
            this.TemperatureWaitNumericUpDown.BackColor = System.Drawing.Color.WhiteSmoke;
            this.TemperatureWaitNumericUpDown.DecimalPlaces = 2;
            this.TemperatureWaitNumericUpDown.ForeColor = System.Drawing.Color.Navy;
            this.TemperatureWaitNumericUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.TemperatureWaitNumericUpDown.Location = new System.Drawing.Point(236, 104);
            this.TemperatureWaitNumericUpDown.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.TemperatureWaitNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.TemperatureWaitNumericUpDown.Name = "TemperatureWaitNumericUpDown";
            this.TemperatureWaitNumericUpDown.Size = new System.Drawing.Size(80, 31);
            this.TemperatureWaitNumericUpDown.TabIndex = 7;
            this.TemperatureWaitNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TemperatureWaitNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ConfigSaveButton
            // 
            this.ConfigSaveButton.ForeColor = System.Drawing.Color.Navy;
            this.ConfigSaveButton.Location = new System.Drawing.Point(143, 373);
            this.ConfigSaveButton.Name = "ConfigSaveButton";
            this.ConfigSaveButton.Size = new System.Drawing.Size(173, 31);
            this.ConfigSaveButton.TabIndex = 23;
            this.ConfigSaveButton.Text = "設定保存";
            this.ConfigSaveButton.UseVisualStyleBackColor = true;
            this.ConfigSaveButton.Click += new System.EventHandler(this.ConfigUpdateButton_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.SystemColors.Control;
            this.label9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label9.Location = new System.Drawing.Point(108, 66);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(122, 24);
            this.label9.TabIndex = 3;
            this.label9.Text = "警報発報温度：";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.SystemColors.Control;
            this.label10.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label10.Location = new System.Drawing.Point(322, 66);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(138, 24);
            this.label10.TabIndex = 5;
            this.label10.Text = "℃以上を発報対象";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.SystemColors.Control;
            this.label8.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label8.Location = new System.Drawing.Point(76, 293);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(154, 24);
            this.label8.TabIndex = 17;
            this.label8.Text = "エリア間移動速度：";
            // 
            // MoveSpeedHighNumericUpDown
            // 
            this.MoveSpeedHighNumericUpDown.BackColor = System.Drawing.Color.WhiteSmoke;
            this.MoveSpeedHighNumericUpDown.ForeColor = System.Drawing.Color.Navy;
            this.MoveSpeedHighNumericUpDown.Location = new System.Drawing.Point(236, 291);
            this.MoveSpeedHighNumericUpDown.Maximum = new decimal(new int[] {
            63,
            0,
            0,
            0});
            this.MoveSpeedHighNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.MoveSpeedHighNumericUpDown.Name = "MoveSpeedHighNumericUpDown";
            this.MoveSpeedHighNumericUpDown.Size = new System.Drawing.Size(80, 31);
            this.MoveSpeedHighNumericUpDown.TabIndex = 18;
            this.MoveSpeedHighNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.MoveSpeedHighNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // TemperatureThresholdNumericUpDown
            // 
            this.TemperatureThresholdNumericUpDown.BackColor = System.Drawing.Color.WhiteSmoke;
            this.TemperatureThresholdNumericUpDown.DecimalPlaces = 1;
            this.TemperatureThresholdNumericUpDown.ForeColor = System.Drawing.Color.Navy;
            this.TemperatureThresholdNumericUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.TemperatureThresholdNumericUpDown.Location = new System.Drawing.Point(236, 64);
            this.TemperatureThresholdNumericUpDown.Maximum = new decimal(new int[] {
            1300,
            0,
            0,
            65536});
            this.TemperatureThresholdNumericUpDown.Minimum = new decimal(new int[] {
            300,
            0,
            0,
            -2147418112});
            this.TemperatureThresholdNumericUpDown.Name = "TemperatureThresholdNumericUpDown";
            this.TemperatureThresholdNumericUpDown.Size = new System.Drawing.Size(80, 31);
            this.TemperatureThresholdNumericUpDown.TabIndex = 4;
            this.TemperatureThresholdNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TemperatureThresholdNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // MoveSpeedLowNumericUpDown
            // 
            this.MoveSpeedLowNumericUpDown.BackColor = System.Drawing.Color.WhiteSmoke;
            this.MoveSpeedLowNumericUpDown.ForeColor = System.Drawing.Color.Navy;
            this.MoveSpeedLowNumericUpDown.Location = new System.Drawing.Point(236, 254);
            this.MoveSpeedLowNumericUpDown.Maximum = new decimal(new int[] {
            63,
            0,
            0,
            0});
            this.MoveSpeedLowNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.MoveSpeedLowNumericUpDown.Name = "MoveSpeedLowNumericUpDown";
            this.MoveSpeedLowNumericUpDown.Size = new System.Drawing.Size(80, 31);
            this.MoveSpeedLowNumericUpDown.TabIndex = 15;
            this.MoveSpeedLowNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.MoveSpeedLowNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.SystemColors.Control;
            this.label7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label7.Location = new System.Drawing.Point(108, 256);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(122, 24);
            this.label7.TabIndex = 14;
            this.label7.Text = "巡回移動速度：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.SystemColors.Control;
            this.label5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label5.Location = new System.Drawing.Point(76, 330);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(154, 24);
            this.label5.TabIndex = 20;
            this.label5.Text = "エリア間移動待機：";
            // 
            // AreaChangeWaitNumericUpDown
            // 
            this.AreaChangeWaitNumericUpDown.BackColor = System.Drawing.Color.WhiteSmoke;
            this.AreaChangeWaitNumericUpDown.DecimalPlaces = 2;
            this.AreaChangeWaitNumericUpDown.ForeColor = System.Drawing.Color.Navy;
            this.AreaChangeWaitNumericUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.AreaChangeWaitNumericUpDown.Location = new System.Drawing.Point(236, 328);
            this.AreaChangeWaitNumericUpDown.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.AreaChangeWaitNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.AreaChangeWaitNumericUpDown.Name = "AreaChangeWaitNumericUpDown";
            this.AreaChangeWaitNumericUpDown.Size = new System.Drawing.Size(80, 31);
            this.AreaChangeWaitNumericUpDown.TabIndex = 21;
            this.AreaChangeWaitNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.AreaChangeWaitNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.SystemColors.Control;
            this.label6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label6.Location = new System.Drawing.Point(322, 330);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(26, 24);
            this.label6.TabIndex = 22;
            this.label6.Text = "秒";
            // 
            // AlarmCountNumericUpDown
            // 
            this.AlarmCountNumericUpDown.BackColor = System.Drawing.Color.WhiteSmoke;
            this.AlarmCountNumericUpDown.ForeColor = System.Drawing.Color.Navy;
            this.AlarmCountNumericUpDown.Location = new System.Drawing.Point(236, 141);
            this.AlarmCountNumericUpDown.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.AlarmCountNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.AlarmCountNumericUpDown.Name = "AlarmCountNumericUpDown";
            this.AlarmCountNumericUpDown.Size = new System.Drawing.Size(80, 31);
            this.AlarmCountNumericUpDown.TabIndex = 10;
            this.AlarmCountNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.AlarmCountNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.SystemColors.Control;
            this.label11.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label11.Location = new System.Drawing.Point(322, 143);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(170, 24);
            this.label11.TabIndex = 11;
            this.label11.Text = "回連続異常検知で発報";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.SystemColors.Control;
            this.label12.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label12.Location = new System.Drawing.Point(76, 143);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(154, 24);
            this.label12.TabIndex = 9;
            this.label12.Text = "温度異常連続回数：";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.SystemColors.Control;
            this.label13.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label13.Location = new System.Drawing.Point(322, 256);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(216, 24);
            this.label13.TabIndex = 16;
            this.label13.Text = "（1：再低速～63：最高速）";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.SystemColors.Control;
            this.label14.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label14.Location = new System.Drawing.Point(322, 293);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(216, 24);
            this.label14.TabIndex = 19;
            this.label14.Text = "（1：再低速～63：最高速）";
            // 
            // EnablePositionTweakCheckBox
            // 
            this.EnablePositionTweakCheckBox.AutoSize = true;
            this.EnablePositionTweakCheckBox.Location = new System.Drawing.Point(237, 178);
            this.EnablePositionTweakCheckBox.Name = "EnablePositionTweakCheckBox";
            this.EnablePositionTweakCheckBox.Size = new System.Drawing.Size(301, 28);
            this.EnablePositionTweakCheckBox.TabIndex = 12;
            this.EnablePositionTweakCheckBox.Text = "温度異常発生時に画角調整を実行する";
            this.EnablePositionTweakCheckBox.UseVisualStyleBackColor = true;
            // 
            // ConfigSetupForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(587, 416);
            this.Controls.Add(this.EnablePositionTweakCheckBox);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.AlarmCountNumericUpDown);
            this.Controls.Add(this.EnableAlarmBuzzerCheckBox);
            this.Controls.Add(this.AlarmTestButton);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.AutoPatrolIntervalNumericUpDown);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TemperatureWaitNumericUpDown);
            this.Controls.Add(this.ConfigSaveButton);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.MoveSpeedHighNumericUpDown);
            this.Controls.Add(this.TemperatureThresholdNumericUpDown);
            this.Controls.Add(this.MoveSpeedLowNumericUpDown);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.AreaChangeWaitNumericUpDown);
            this.Controls.Add(this.label6);
            this.Font = new System.Drawing.Font("メイリオ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Name = "ConfigSetupForm";
            this.Text = "動作設定";
            this.Load += new System.EventHandler(this.ConfigSetupForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.AutoPatrolIntervalNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TemperatureWaitNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MoveSpeedHighNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TemperatureThresholdNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MoveSpeedLowNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AreaChangeWaitNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AlarmCountNumericUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox EnableAlarmBuzzerCheckBox;
        private System.Windows.Forms.Button AlarmTestButton;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown AutoPatrolIntervalNumericUpDown;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown TemperatureWaitNumericUpDown;
        private System.Windows.Forms.Button ConfigSaveButton;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown MoveSpeedHighNumericUpDown;
        private System.Windows.Forms.NumericUpDown TemperatureThresholdNumericUpDown;
        private System.Windows.Forms.NumericUpDown MoveSpeedLowNumericUpDown;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown AreaChangeWaitNumericUpDown;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown AlarmCountNumericUpDown;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.CheckBox EnablePositionTweakCheckBox;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CES.ThermalPTZ.Threads
{
    public abstract class QueueThread<T> : AppThreadBase
    {
        readonly Queue<T> _Queue;
        readonly object _QueueLock;

        public QueueThread( string ThreadName = null, ThreadPriority Priority = ThreadPriority.Normal )
            : base( ThreadName, Priority )
        {
            _Queue = new Queue<T>();
            _QueueLock = new object();
        }

        public virtual void Enqueue( T Item )
        {
            lock ( _QueueLock )
            {
                _Queue.Enqueue( Item );
            }
        }

        protected T Dequeue()
        {
            while ( true )
            {
                int Count = 0;
                lock ( _QueueLock )
                {
                    Count = _Queue.Count;
                }

                if ( Count <= 0 )
                {
                    Thread.Sleep( 10 );
                    continue;
                }

                lock ( _QueueLock )
                {
                    return _Queue.Dequeue();
                }
            }
        }
        protected abstract bool ItemRoutine( T Item );

        protected override void ThreadRoutine()
        {
            while ( ItemRoutine( Dequeue() ) == true )
                ;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Threading;

namespace CES.ThermalPTZ.Threads
{
    public abstract class IntervalThread : AppThreadBase
    {
        const int DEFAULT_INTERVAL = 100;

        readonly int _Interval = DEFAULT_INTERVAL;

        public IntervalThread( string ThreadName, int Interval = DEFAULT_INTERVAL )
            : base( ThreadName )
        {
            _Interval = Interval;
        }

        protected abstract bool TimerRoutine();

        protected override void ThreadRoutine()
        {
            try
            {
                do
                {
                    Thread.Sleep( _Interval );
                } while ( TimerRoutine() == true );
            }
            catch ( ThreadAbortException )
            {
                throw;
            }
            catch ( Exception )
            {
                return;
            }
        }
    }
}

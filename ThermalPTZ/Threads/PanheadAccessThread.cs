﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace CES.ThermalPTZ.Threads
{
    using CH_555;
    using static Properties.Settings;

    public class PanheadAccessThread : EventSyncThread
    {
        public event EventHandler<PanheadPositionEventArgs> Position;
        public event EventHandler PanheadError;

        const int QUERY_INTERVAL = 500;
        const int MAX_QUERY_ERROR_COUNT = 30;

        public enum PANHEAD_CNTL_COMMAND
        {
            STOP,
            PAN_LEFT,
            PAN_RIGHT,
            PAN_STOP,
            TILT_UP,
            TILT_DOWN,
            TILT_STOP,
            PAN_LEFT_TILT_UP,
            PAN_LEFT_TILT_DOWN,
            PAN_RIGHT_TILT_UP,
            PAN_RIGHT_TILT_DOWN,
            SET_PRESET,
            CLEAR_PRESET,
            GOTO_PRESET,
            SET_SPEED,
            ALL_CLEAR_PRESET
        }

        readonly PanHeadIO _PanHeadIO;

        PANHEAD_CNTL_COMMAND? _Command = null;
        PANHEAD_CNTL_COMMAND? _LastSendCommand = null;
        byte _MoveSpeed = 0;
        byte _PresetSpeed = 0;
        byte _PresetNumber = 0;

        float? _LastPan = null;
        float? _LastTilt = null;

        bool _PanQuery = true;
        int _QueryErrorCount = 0;
        bool _QueryErrorSend = false;

        public PanheadAccessThread( string Address, int Port, byte PanheadId ) : base( QUERY_INTERVAL, "Panhead" )
        {
            _PanHeadIO = new PanHeadIO( Address, Port, PanheadId );
        }

        public void ClearErrorCount()
        {
            _QueryErrorCount = 0;
            _QueryErrorSend = false;
        }

        public void GetPosition( out float Pan, out float Tilt )
        {
            if ( ( _LastPan.HasValue == false ) || ( _LastPan.Value.Equals( float.NaN ) == true ) )
            {
                throw new ApplicationException( "PAN 位置情報が取得されていません。" );
            }
            if ( ( _LastTilt.HasValue == false ) || ( _LastTilt.Value.Equals( float.NaN ) == true ) )
            {
                throw new ApplicationException( "TILT 位置情報が取得されていません。" );
            }

            Pan = _LastPan.Value;
            Tilt = _LastTilt.Value;
        }

        public bool GotoPreset( byte PresetNo )
        {
            try
            {
                _Command = PANHEAD_CNTL_COMMAND.GOTO_PRESET;
                _PresetNumber = PresetNo;
                DoEventAction();
                while ( _Command.HasValue == true )
                {
                    Thread.Sleep( 10 );
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool ClearPreset( byte PresetNo )
        {
            try
            {
                _Command = PANHEAD_CNTL_COMMAND.CLEAR_PRESET;
                _PresetNumber = PresetNo;
                DoEventAction();
                while ( _Command.HasValue == true )
                {
                    Thread.Sleep( 10 );
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> AllClearPresetAsync()
        {
            return await Task.Factory.StartNew<bool>( () =>
            {
                try
                {
                    _Command = PANHEAD_CNTL_COMMAND.ALL_CLEAR_PRESET;
                    DoEventAction();
                    while ( _Command.HasValue == true )
                    {
                        Thread.Sleep( 10 );
                    }
                    return true;
                }
                catch
                {
                    return false;
                }
            } );
        }

        public bool PanTilt(bool PanStop, bool Left, bool TiltStop, bool TiltUp, byte Speed )
        {
            try
            {
                if ( PanStop == true )
                {
                    if ( TiltStop == true )
                    {
                        _Command = PANHEAD_CNTL_COMMAND.STOP;
                    }
                    else if ( TiltUp == true )
                    {
                        _Command = PANHEAD_CNTL_COMMAND.TILT_UP;
                    }
                    else
                    {
                        _Command = PANHEAD_CNTL_COMMAND.TILT_DOWN;
                    }
                }
                else if ( Left == true )
                {
                    if ( TiltStop == true )
                    {
                        _Command = PANHEAD_CNTL_COMMAND.PAN_LEFT;
                    }
                    else if ( TiltUp == true )
                    {
                        _Command = PANHEAD_CNTL_COMMAND.PAN_LEFT_TILT_UP;
                    }
                    else
                    {
                        _Command = PANHEAD_CNTL_COMMAND.PAN_LEFT_TILT_DOWN;
                    }
                }
                else
                {
                    if ( TiltStop == true )
                    {
                        _Command = PANHEAD_CNTL_COMMAND.PAN_RIGHT;
                    }
                    else if ( TiltUp == true )
                    {
                        _Command = PANHEAD_CNTL_COMMAND.PAN_RIGHT_TILT_UP;
                    }
                    else
                    {
                        _Command = PANHEAD_CNTL_COMMAND.PAN_RIGHT_TILT_DOWN;
                    }
                }

                _MoveSpeed = Speed;
                DoEventAction();
                while ( _Command.HasValue == true )
                {
                    Thread.Sleep( 10 );
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool SetPreset( byte PresetNo )
        {
            try
            {
                _Command = PANHEAD_CNTL_COMMAND.SET_PRESET;
                _PresetNumber = PresetNo;
                DoEventAction();
                while ( _Command.HasValue == true )
                {
                    Thread.Sleep( 10 );
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool SetPresetSpeed( byte Speed )
        {
            try
            {
                _Command = PANHEAD_CNTL_COMMAND.SET_SPEED;
                _PresetSpeed = Speed;
                DoEventAction();
                while ( _Command.HasValue == true )
                {
                    Thread.Sleep( 10 );
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool MoveStop()
        {
            try
            {
                _Command = PANHEAD_CNTL_COMMAND.STOP;
                if ( ( _LastSendCommand.HasValue == true ) &&
                     ( _LastSendCommand.Value == _Command.Value ) )
                {
                    return true;
                }
                DoEventAction();
                while ( _Command.HasValue == true )
                {
                    Thread.Sleep( 10 );
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Tilt( bool Up, byte Speed )
        {
            try
            {
                if ( Up == true )
                {
                    _Command = PANHEAD_CNTL_COMMAND.TILT_UP;
                }
                else
                {
                    _Command = PANHEAD_CNTL_COMMAND.TILT_DOWN;
                }

                if ( ( _LastSendCommand.HasValue == true ) &&
                   ( _LastSendCommand.Value == _Command.Value ) &&
                   ( _MoveSpeed == Speed ) )
                {
                    return true;
                }

                _MoveSpeed = Speed;
                DoEventAction();
                while ( _Command.HasValue == true )
                {
                    Thread.Sleep( 10 );
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        protected override void TimeoutCallback()
        {
            try
            {
                if ( Default.EnablePanheadQuery == false )
                {
                    return;
                }

                float Pan = float.NaN;
                float Tilt = float.NaN;
                if ( _PanQuery == true )
                {
                    Pan = _PanHeadIO.PanPosition();
                    if ( _LastTilt.HasValue == true )
                    {
                        Tilt = _LastTilt.Value;
                    }
                }
                else
                {
                    Tilt = _PanHeadIO.TiltPosition();
                    if ( _LastPan.HasValue == true )
                    {
                        Pan = _LastPan.Value;
                    }
                }

                if ( ( float.IsNaN( Pan ) == true ) || ( Pan == float.MaxValue ) || ( float.IsNaN( Tilt ) == true ) || ( Tilt == float.MaxValue ) )
                {
                    _QueryErrorCount++;
                    if ( ( _QueryErrorCount > MAX_QUERY_ERROR_COUNT ) && ( _QueryErrorSend == false ) )
                    {
                        _QueryErrorSend = true;

                        if ( PanheadError != null )
                        {
                            if ( ( PanheadError.Target != null ) && ( PanheadError.Target is Control Cntl ) )
                            {
                                Cntl.BeginInvoke( PanheadError, this, EventArgs.Empty );
                            }
                            else
                            {
                                PanheadError( this, EventArgs.Empty );
                            }
                        }
                    }
                }
                else
                {
                    _QueryErrorCount = 0;
                    _QueryErrorSend = false;
                }

                if ( ( _LastPan.HasValue == true ) && ( _LastPan.Value.Equals( Pan ) == true ) &&
                    ( _LastTilt.HasValue == true ) && ( _LastTilt.Value.Equals( Tilt ) == true ) )
                {
                    return;
                }

                if ( Pan.Equals( float.MaxValue ) == true )
                {
                    Trace.WriteLine( $"雲台異常 PAN 不正応答(FF FF)" );
                    //return;
                }
                if ( Tilt.Equals( float.MaxValue ) == true )
                {
                    Trace.WriteLine( $"雲台異常 TILT 不正応答(FF FF)" );
                    //return;
                }

                _LastPan = Pan;
                _LastTilt = Tilt;

                if ( Position != null )
                {
                    if ( ( Position.Target != null ) && ( Position.Target is Control Cntl ) )
                    {
                        Cntl.BeginInvoke( Position, this, new PanheadPositionEventArgs( _LastPan.Value, _LastTilt.Value ) );
                    }
                    else
                    {
                        Position( this, new PanheadPositionEventArgs( _LastPan.Value, _LastTilt.Value ) );
                    }
                }
            }
            catch ( ThreadAbortException )
            {
                throw;
            }
            catch ( Exception e )
            {
                Trace.WriteLine( $"Panhead Query error. {e.Message}" );
            }
            finally
            {
                _PanQuery = ( _PanQuery == false );
            }
        }

        protected override void Terminate()
        {
            _PanHeadIO?.Dispose();
            base.Terminate();
        }

        protected override void EventCallback()
        {
            if ( _Command.HasValue == false )
            {
                return;
            }

            try
            {
                switch ( _Command.Value )
                {
                    case PANHEAD_CNTL_COMMAND.STOP:
                        _PanHeadIO.PanTilt( PanHeadCommand.PAN_TILT_MOVE_MODE.PAN_STOP, PanHeadCommand.PAN_TILT_MOVE_MODE.TILT_STOP, 0 );
                        break;

                    case PANHEAD_CNTL_COMMAND.SET_SPEED:
                        _PanHeadIO.PresetSpeed( _PresetSpeed );
                        break;
                    case PANHEAD_CNTL_COMMAND.SET_PRESET:
                        _PanHeadIO.SetPreset( _PresetNumber );
                        break;
                    case PANHEAD_CNTL_COMMAND.GOTO_PRESET:
                        _PanHeadIO.GotoPreset( _PresetNumber );
                        break;
                    case PANHEAD_CNTL_COMMAND.CLEAR_PRESET:
                        _PanHeadIO.ClearPreset( _PresetNumber );
                        break;
                    case PANHEAD_CNTL_COMMAND.ALL_CLEAR_PRESET:
                        _AllClearPreset();
                        break;

                    case PANHEAD_CNTL_COMMAND.PAN_LEFT:
                        _PanHeadIO.PanTilt( PanHeadCommand.PAN_TILT_MOVE_MODE.PAN_LEFT, PanHeadCommand.PAN_TILT_MOVE_MODE.TILT_STOP, _MoveSpeed );
                        break;
                    case PANHEAD_CNTL_COMMAND.PAN_RIGHT:
                        _PanHeadIO.PanTilt( PanHeadCommand.PAN_TILT_MOVE_MODE.PAN_RIGHT, PanHeadCommand.PAN_TILT_MOVE_MODE.TILT_STOP, _MoveSpeed );
                        break;
                    case PANHEAD_CNTL_COMMAND.PAN_LEFT_TILT_UP:
                        _PanHeadIO.PanTilt( PanHeadCommand.PAN_TILT_MOVE_MODE.PAN_LEFT, PanHeadCommand.PAN_TILT_MOVE_MODE.TILT_UP, _MoveSpeed );
                        break;
                    case PANHEAD_CNTL_COMMAND.PAN_LEFT_TILT_DOWN:
                        _PanHeadIO.PanTilt( PanHeadCommand.PAN_TILT_MOVE_MODE.PAN_LEFT, PanHeadCommand.PAN_TILT_MOVE_MODE.TILT_DOWN, _MoveSpeed );
                        break;
                    case PANHEAD_CNTL_COMMAND.PAN_RIGHT_TILT_UP:
                        _PanHeadIO.PanTilt( PanHeadCommand.PAN_TILT_MOVE_MODE.PAN_RIGHT, PanHeadCommand.PAN_TILT_MOVE_MODE.TILT_UP, _MoveSpeed );
                        break;
                    case PANHEAD_CNTL_COMMAND.PAN_RIGHT_TILT_DOWN:
                        _PanHeadIO.PanTilt( PanHeadCommand.PAN_TILT_MOVE_MODE.PAN_RIGHT, PanHeadCommand.PAN_TILT_MOVE_MODE.TILT_DOWN, _MoveSpeed );
                        break;
                    case PANHEAD_CNTL_COMMAND.TILT_DOWN:
                        _PanHeadIO.PanTilt( PanHeadCommand.PAN_TILT_MOVE_MODE.PAN_STOP, PanHeadCommand.PAN_TILT_MOVE_MODE.TILT_DOWN, _MoveSpeed );
                        break;
                    case PANHEAD_CNTL_COMMAND.TILT_UP:
                        _PanHeadIO.PanTilt( PanHeadCommand.PAN_TILT_MOVE_MODE.PAN_STOP, PanHeadCommand.PAN_TILT_MOVE_MODE.TILT_UP, _MoveSpeed );
                        break;
                    default:
                        return;
                }
            }
            catch
            {

            }
            finally
            {
                _LastSendCommand = _Command.Value;
                _Command = null;
                TimeoutCallback();
            }
        }

        private void _AllClearPreset()
        {
            try
            {
                for ( byte PresetNo = Data.PresetTable.PRESET_NO_MIN ;
                    PresetNo <= Data.PresetTable.PRESET_NO_MAX ; PresetNo++ )
                {
                    _PanHeadIO.ClearPreset( PresetNo );
                }
            }
            catch
            {
            }
        }
    }

    public class PanheadPositionEventArgs : EventArgs
    {
        public PanheadPositionEventArgs( float PanPosition, float TiltPosition )
            : base()
        {
            Pan = PanPosition;
            Tilt = TiltPosition;
        }
        public float Pan
        {
            get;
            private set;
        }

        public float Tilt
        {
            get;
            private set;
        }
    }
}

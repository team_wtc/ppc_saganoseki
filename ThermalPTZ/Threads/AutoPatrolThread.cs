﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Diagnostics;
using System.Windows.Forms;

namespace CES.ThermalPTZ.Threads
{
    using Data;
    using static Properties.Settings;

    public class AutoPatrolThread : IntervalThread
    {
        const int PATROL_INTERVAL = 50;

        public event EventHandler<AutoPatrolEventArgs> AutoPatrol;

        public enum PATROL_MODE
        {
            /// <summary>
            /// 停止中
            /// </summary>
            IDLE,
            /// <summary>
            /// 開始
            /// </summary>
            PATROL_START,
            /// <summary>
            /// 監視中（Preset移動中）
            /// </summary>
            PATROL_RUNNING,
            /// <summary>
            /// オートフォーカス実行中
            /// </summary>
            PATROL_WAIT_AUTO_FOCUS,
            /// <summary>
            /// エリア移動待機中
            /// </summary>
            PATROL_WAIT_AREA_CHANGE,
            /// <summary>
            /// 一時停止中
            /// </summary>
            PATROL_PAUSE,
            /// <summary>
            /// 次回監視待機中
            /// </summary>
            PATROL_INTERVAL,
            /// <summary>
            /// 再開待機中
            /// </summary>
            PATROL_WAIT_RESUME,
            /// <summary>
            /// 再開
            /// </summary>
            PATROL_RESUME,
            /// <summary>
            /// 温度異常発生
            /// </summary>
            ALARM,
            /// <summary>
            /// 発生位置に移動中
            /// </summary>
            ALARM_POSITION,
            /// <summary>
            /// 発生位置のスナップショット取得
            /// </summary>
            ALARM_SNAPSHOT,
            /// <summary>
            /// 監視終了
            /// </summary>
            TERMINATE
        }

        readonly Stopwatch _Stopwatch = new Stopwatch();
        readonly PresetTable _PresetTable = null;
        readonly PanheadAccessThread _PanheadAccessThread = null;
        readonly CameraAccessThread _CameraAccessThread = null;

        int _CurrentAreaIndex = 0;
        int _CurrentPresetIndex = 0;

        PATROL_MODE _CurrentPatrolMode = PATROL_MODE.IDLE;
        Dictionary<int, List<AutoPatrolInfo>> _PatrolTable = null;
        int[] AreaArray = null;
        long _NextMoveTime = 0L;

        int? _TemperaturePosX = null;
        int? _TemperaturePosY = null;
        float? _CurrentTemperature = null;
        float? _AlarmTemperature = null;

        float? _CurrentPan = null;
        float? _CurrentTilt = null;

        public AutoPatrolThread( PresetTable Table, PanheadAccessThread PanheadThread, CameraAccessThread CameraThread )
            : base( "Patrol", PATROL_INTERVAL )
        {
            _PresetTable = Table;
            _PanheadAccessThread = PanheadThread;
            _CameraAccessThread = CameraThread;
        }

        public void SetPosition( float Pan, float Tilt )
        {
            _CurrentPan = Pan;
            _CurrentTilt = Tilt;
        }

        public void SetTemperaturePosition( int PosX, int PosY, float Temperature )
        {
            _TemperaturePosX = PosX;
            _TemperaturePosY = PosY;
            _CurrentTemperature = Temperature;
        }

        public bool IsPatrolRunning
        {
            get => ( ( _CurrentPatrolMode == PATROL_MODE.PATROL_RUNNING ) || ( _CurrentPatrolMode == PATROL_MODE.PATROL_WAIT_AUTO_FOCUS ) || ( _CurrentPatrolMode == PATROL_MODE.PATROL_WAIT_AREA_CHANGE ) );
        }

        public bool IsPatrolStop
        {
            get => ( _CurrentPatrolMode == PATROL_MODE.IDLE );
        }

        public bool IsPatrolPause
        {
            get => ( ( _CurrentPatrolMode == PATROL_MODE.PATROL_PAUSE ) || ( _CurrentPatrolMode == PATROL_MODE.PATROL_WAIT_RESUME ) );
        }

        public bool IsPatrolAlarm
        {
            get => ( ( _CurrentPatrolMode == PATROL_MODE.ALARM ) || ( _CurrentPatrolMode == PATROL_MODE.ALARM_POSITION ) );
        }

        public void StartPatrol()
        {
            _PatrolTable = _PresetTable.AutoPatrolTable();
            if ( _PatrolTable.Count <= 0 )
            {
                return;
            }

            AreaArray = _PatrolTable.Keys.ToArray<int>();

            if ( ( _CurrentAreaIndex < 0 ) ||
                ( _CurrentAreaIndex >= AreaArray.Length ) )
            {
                _CurrentAreaIndex = 0;
                _CurrentPresetIndex = 0;
            }

            if ( ( _CurrentPatrolMode == PATROL_MODE.PATROL_PAUSE ) || ( _CurrentPatrolMode == PATROL_MODE.PATROL_WAIT_RESUME ) )
            {
                _CurrentAreaIndex = 0;
                _CurrentPresetIndex = 0;
            }

            _CurrentPatrolMode = PATROL_MODE.PATROL_START;
        }

        public void SuspendPatrol()
        {
            _CurrentPatrolMode = PATROL_MODE.PATROL_PAUSE;
        }
        public void AlarmPatrol()
        {
            if ( ( _CurrentPatrolMode != PATROL_MODE.PATROL_RUNNING ) && ( _CurrentPatrolMode != PATROL_MODE.PATROL_WAIT_AUTO_FOCUS ) )
            {
                return;
            }

            if ( ( _TemperaturePosX.HasValue == false ) || ( _TemperaturePosY.HasValue == false ) || ( _CurrentTemperature.HasValue == false ) )
            {
                return;
            }
            Trace.WriteLine( $"Alarm発生 X={_TemperaturePosX.Value} Y={_TemperaturePosY.Value} {_CurrentTemperature.Value:F1}℃" );
            _AlarmTemperature = _CurrentTemperature.Value;
            _CurrentPatrolMode = PATROL_MODE.ALARM;
        }

        public void ResumePatrol()
        {
            _CurrentPatrolMode = PATROL_MODE.PATROL_RESUME;
        }

        protected override bool Init()
        {
            _PatrolTable = _PresetTable.AutoPatrolTable();
            if ( _PatrolTable.Count <= 0 )
            {
                return true;
            }

            AreaArray = _PatrolTable.Keys.ToArray<int>();
            _CurrentAreaIndex = 0;
            _CurrentPresetIndex = 0;

            if ( ( _PatrolTable[ AreaArray[ _CurrentAreaIndex ] ] == null ) || ( _PatrolTable[ AreaArray[ _CurrentAreaIndex ] ].Count <= 0 ) )
            {
                return true;
            }

            _PanheadAccessThread.SetPresetSpeed( 63 );
            _PanheadAccessThread.GotoPreset( Convert.ToByte( _PatrolTable[ AreaArray[ _CurrentAreaIndex ] ][ _CurrentPresetIndex ].PresetNumber ) );
            //  2020/10/14 現地修正　GotoPreset3回出し
            Thread.Sleep( 500 );
            _PanheadAccessThread.GotoPreset( Convert.ToByte( _PatrolTable[ AreaArray[ _CurrentAreaIndex ] ][ _CurrentPresetIndex ].PresetNumber ) );
            Thread.Sleep( 500 );
            _PanheadAccessThread.GotoPreset( Convert.ToByte( _PatrolTable[ AreaArray[ _CurrentAreaIndex ] ][ _CurrentPresetIndex ].PresetNumber ) );

            return true;
        }

        protected override bool TimerRoutine()
        {
            if ( _CurrentPatrolMode == PATROL_MODE.IDLE )
            {
                return true;
            }
            if ( _CurrentPatrolMode == PATROL_MODE.PATROL_START )
            {
                _Start();
                return true;
            }
            if ( ( _CurrentPatrolMode == PATROL_MODE.PATROL_RUNNING ) ||
                ( _CurrentPatrolMode == PATROL_MODE.PATROL_WAIT_AUTO_FOCUS ) ||
                ( _CurrentPatrolMode == PATROL_MODE.PATROL_WAIT_AREA_CHANGE ) )
            {
                PatorolEvent();
                _CheckNext();
                return true;
            }
            if ( _CurrentPatrolMode == PATROL_MODE.PATROL_INTERVAL )
            {
                if ( ( _Stopwatch.ElapsedMilliseconds < _NextMoveTime ) )
                {
                    IntervalEvent();
                }
                else
                {
                    _Start();
                }
                return true;
            }
            if ( _CurrentPatrolMode == PATROL_MODE.PATROL_PAUSE )
            {
                _Pause();
                return true;
            }
            if ( _CurrentPatrolMode == PATROL_MODE.PATROL_WAIT_RESUME )
            {
                return true;
            }
            if ( _CurrentPatrolMode == PATROL_MODE.PATROL_RESUME )
            {
                _Resume();
                return true;
            }
            if ( _CurrentPatrolMode == PATROL_MODE.ALARM )
            {
                _Alarm();
                return true;
            }
            if ( _CurrentPatrolMode == PATROL_MODE.ALARM_POSITION )
            {
                _AlarmPosition();
                PatorolEvent();
                return true;
            }
            if ( _CurrentPatrolMode == PATROL_MODE.ALARM_SNAPSHOT )
            {
                _Snapshot();
                return true;
            }

            return false;
        }

        void PatorolEvent()
        {
            if ( AutoPatrol != null )
            {
                if ( ( AutoPatrol.Target != null ) && ( AutoPatrol.Target is Control Cntl ) )
                {
                    Cntl.BeginInvoke(
                        AutoPatrol,
                        this,
                        new AutoPatrolEventArgs(
                            AreaArray[ _CurrentAreaIndex ],
                            _PresetTable.AreaName( AreaArray[ _CurrentAreaIndex ] ),
                            _PatrolTable[ AreaArray[ _CurrentAreaIndex ] ][ _CurrentPresetIndex ].PresetNumber,
                            _PatrolTable[ AreaArray[ _CurrentAreaIndex ] ][ _CurrentPresetIndex ].Pan,
                            _PatrolTable[ AreaArray[ _CurrentAreaIndex ] ][ _CurrentPresetIndex ].Tilt,
                            ( _Stopwatch.ElapsedMilliseconds >= _NextMoveTime ) ? 0L : ( _NextMoveTime - _Stopwatch.ElapsedMilliseconds ),
                            _CurrentPatrolMode ) );
                }
                else
                {
                    AutoPatrol(
                        this,
                        new AutoPatrolEventArgs(
                            AreaArray[ _CurrentAreaIndex ],
                            _PresetTable.AreaName( AreaArray[ _CurrentAreaIndex ] ),
                            _PatrolTable[ AreaArray[ _CurrentAreaIndex ] ][ _CurrentPresetIndex ].PresetNumber,
                            _PatrolTable[ AreaArray[ _CurrentAreaIndex ] ][ _CurrentPresetIndex ].Pan,
                            _PatrolTable[ AreaArray[ _CurrentAreaIndex ] ][ _CurrentPresetIndex ].Tilt,
                            ( _Stopwatch.ElapsedMilliseconds >= _NextMoveTime ) ? 0L : ( _NextMoveTime - _Stopwatch.ElapsedMilliseconds ),
                            _CurrentPatrolMode ) );
                }
            }
        }

        void IntervalEvent()
        {
            if ( AutoPatrol != null )
            {
                if ( ( AutoPatrol.Target != null ) && ( AutoPatrol.Target is Control Cntl ) )
                {
                    Cntl.BeginInvoke(
                        AutoPatrol,
                        this,
                        new AutoPatrolEventArgs(
                            -1,
                            string.Empty,
                            -1,
                            float.NaN,
                            float.NaN,
                            ( _Stopwatch.ElapsedMilliseconds >= _NextMoveTime ) ? 0L : ( _NextMoveTime - _Stopwatch.ElapsedMilliseconds ),
                            _CurrentPatrolMode ) );
                }
                else
                {
                    AutoPatrol(
                        this,
                        new AutoPatrolEventArgs(
                            -1,
                            string.Empty,
                            -1,
                            float.NaN,
                            float.NaN,
                            ( _Stopwatch.ElapsedMilliseconds >= _NextMoveTime ) ? 0L : ( _NextMoveTime - _Stopwatch.ElapsedMilliseconds ),
                            _CurrentPatrolMode ) );
                }
            }
        }

        void _Start()
        {
            if ( ( _PatrolTable.ContainsKey( AreaArray[ _CurrentAreaIndex ] ) == true ) &&
                ( _PatrolTable[ AreaArray[ _CurrentAreaIndex ] ] != null ) &&
                ( _PatrolTable[ AreaArray[ _CurrentAreaIndex ] ].Count > 0 ) &&
                ( _CurrentPresetIndex < _PatrolTable[ AreaArray[ _CurrentAreaIndex ] ].Count ) )
            {
                if ( _PatrolTable[ AreaArray[ _CurrentAreaIndex ] ][ _CurrentPresetIndex ].FastMove == true )
                {
                    _PanheadAccessThread.SetPresetSpeed( Default.AreaChangePresetSpeed );
                }
                else
                {
                    _PanheadAccessThread.SetPresetSpeed( Default.PatrolPresetSpeed );
                }

                Trace.WriteLine(
                    $"Start GoTo Preset #{_PatrolTable[ AreaArray[ _CurrentAreaIndex ] ][ _CurrentPresetIndex ].PresetNumber} " +
                    $"Pan={_PatrolTable[ AreaArray[ _CurrentAreaIndex ] ][ _CurrentPresetIndex ].Pan:F2} " +
                    $"Tilt={_PatrolTable[ AreaArray[ _CurrentAreaIndex ] ][ _CurrentPresetIndex ].Tilt:F2}" );

                _PanheadAccessThread.GotoPreset( Convert.ToByte( _PatrolTable[ AreaArray[ _CurrentAreaIndex ] ][ _CurrentPresetIndex ].PresetNumber ) );
                //  2020/10/14 現地修正　GotoPreset3回出し
                Thread.Sleep( 500 );
                _PanheadAccessThread.GotoPreset( Convert.ToByte( _PatrolTable[ AreaArray[ _CurrentAreaIndex ] ][ _CurrentPresetIndex ].PresetNumber ) );
                Thread.Sleep( 500 );
                _PanheadAccessThread.GotoPreset( Convert.ToByte( _PatrolTable[ AreaArray[ _CurrentAreaIndex ] ][ _CurrentPresetIndex ].PresetNumber ) );

                _Stopwatch.Restart();
                _CurrentPatrolMode = PATROL_MODE.PATROL_RUNNING;
            }
        }

        void _CheckNext()
        {
            const long DEFAULT_AUTO_FOCUS_WAIT_TIME = 10L * 1000L;

            if ( _CurrentPatrolMode == PATROL_MODE.PATROL_RUNNING )
            {
                if ( ( _CurrentPan.HasValue == false ) || ( _CurrentTilt.HasValue == false ) )
                {
                    return;
                }

                //  2020/09/28  Pan、TiltのQuery誤差修正  H.Onuki
                if ( ( float.IsNaN( _CurrentPan.Value ) == true ) || ( float.IsNaN(_CurrentTilt.Value) == true ) )
                {
                    return;
                }

                float PanMin = Convert.ToSingle( Math.Round( _PatrolTable[ AreaArray[ _CurrentAreaIndex ] ][ _CurrentPresetIndex ].Pan - 0.01F, 2, MidpointRounding.AwayFromZero ) );
                float PanMax = Convert.ToSingle( Math.Round( _PatrolTable[ AreaArray[ _CurrentAreaIndex ] ][ _CurrentPresetIndex ].Pan + 0.01F, 2, MidpointRounding.AwayFromZero ) );
                float TiltMin = Convert.ToSingle( Math.Round( _PatrolTable[ AreaArray[ _CurrentAreaIndex ] ][ _CurrentPresetIndex ].Tilt - 0.01F, 2, MidpointRounding.AwayFromZero ) );
                float TiltMax = Convert.ToSingle( Math.Round( _PatrolTable[ AreaArray[ _CurrentAreaIndex ] ][ _CurrentPresetIndex ].Tilt + 0.01F, 2, MidpointRounding.AwayFromZero ) );

                if ( ( _CurrentPan.Value < PanMin ) || ( _CurrentPan.Value > PanMax ) || ( _CurrentTilt.Value < TiltMin ) || ( _CurrentTilt.Value > TiltMax ) )
                {
                    return;
                }

                //if ( ( _CurrentPan.Value.Equals( _PatrolTable[ AreaArray[ _CurrentAreaIndex ] ][ _CurrentPresetIndex ].Pan ) == false ) ||
                //    ( _CurrentTilt.Value.Equals( _PatrolTable[ AreaArray[ _CurrentAreaIndex ] ][ _CurrentPresetIndex ].Tilt ) == false ) )
                //{
                //    return;
                //}

                if ( _PatrolTable[ AreaArray[ _CurrentAreaIndex ] ][ _CurrentPresetIndex ].AutoFocus == true )
                {
                    _CameraAccessThread?.ExecAutoFocus();
                    _CurrentPatrolMode = PATROL_MODE.PATROL_WAIT_AUTO_FOCUS;
                    if ( _PatrolTable[ AreaArray[ _CurrentAreaIndex ] ][ _CurrentPresetIndex ].AutoFocusWaitTime <= 0 )
                    {
                        _NextMoveTime = DEFAULT_AUTO_FOCUS_WAIT_TIME;
                    }
                    else
                    {
                        _NextMoveTime = Convert.ToInt64( _PatrolTable[ AreaArray[ _CurrentAreaIndex ] ][ _CurrentPresetIndex ].AutoFocusWaitTime * 1000 );
                    }
                    _Stopwatch.Restart();
                }
                else
                {
                    if ( _CurrentPresetIndex < ( _PatrolTable[ AreaArray[ _CurrentAreaIndex ] ].Count - 1 ) )
                    {
                        _CurrentPresetIndex++;
                        _Start();
                    }
                    else
                    {
                        if ( _CurrentAreaIndex < ( AreaArray.Length - 1 ) )
                        {
                            _CurrentPatrolMode = PATROL_MODE.PATROL_WAIT_AREA_CHANGE;
                            _NextMoveTime = Convert.ToInt64( Default.AreaChangeWaitTime * 1000 );
                        }
                        else
                        {
                            _CurrentAreaIndex = 0;
                            _CurrentPresetIndex = 0;
                            _NextMoveTime = Default.AutoPatrolInterval * 1000L;
                            _CurrentPatrolMode = PATROL_MODE.PATROL_INTERVAL;

                            _PanheadAccessThread.SetPresetSpeed( 63 );
                            _PanheadAccessThread.GotoPreset( Convert.ToByte( _PatrolTable[ AreaArray[ _CurrentAreaIndex ] ][ _CurrentPresetIndex ].PresetNumber ) );
                            //  2020/10/14 現地修正　GotoPreset3回出し
                            Thread.Sleep( 500 );
                            _PanheadAccessThread.GotoPreset( Convert.ToByte( _PatrolTable[ AreaArray[ _CurrentAreaIndex ] ][ _CurrentPresetIndex ].PresetNumber ) );
                            Thread.Sleep( 500 );
                            _PanheadAccessThread.GotoPreset( Convert.ToByte( _PatrolTable[ AreaArray[ _CurrentAreaIndex ] ][ _CurrentPresetIndex ].PresetNumber ) );
                        }

                        _Stopwatch.Restart();
                    }
                }
                return;
            }

            if ( _CurrentPatrolMode == PATROL_MODE.PATROL_WAIT_AUTO_FOCUS )
            {
                if ( _Stopwatch.ElapsedMilliseconds < _NextMoveTime )
                {
                    return;
                }

                if ( _CurrentPresetIndex < ( _PatrolTable[ AreaArray[ _CurrentAreaIndex ] ].Count - 1 ) )
                {
                    _CurrentPresetIndex++;
                    _Start();
                }
                else
                {
                    if ( _CurrentAreaIndex < ( AreaArray.Length - 1 ) )
                    {
                        _CurrentPatrolMode = PATROL_MODE.PATROL_WAIT_AREA_CHANGE;
                        _NextMoveTime = Convert.ToInt64( Default.AreaChangeWaitTime * 1000 );
                    }
                    else
                    {
                        _CurrentAreaIndex = 0;
                        _CurrentPresetIndex = 0;
                        _NextMoveTime = Default.AutoPatrolInterval * 1000L;
                        _CurrentPatrolMode = PATROL_MODE.PATROL_INTERVAL;

                        _PanheadAccessThread.SetPresetSpeed( 63 );
                        _PanheadAccessThread.GotoPreset( Convert.ToByte( _PatrolTable[ AreaArray[ _CurrentAreaIndex ] ][ _CurrentPresetIndex ].PresetNumber ) );
                        //  2020/10/14 現地修正　GotoPreset3回出し
                        Thread.Sleep( 500 );
                        _PanheadAccessThread.GotoPreset( Convert.ToByte( _PatrolTable[ AreaArray[ _CurrentAreaIndex ] ][ _CurrentPresetIndex ].PresetNumber ) );
                        Thread.Sleep( 500 );
                        _PanheadAccessThread.GotoPreset( Convert.ToByte( _PatrolTable[ AreaArray[ _CurrentAreaIndex ] ][ _CurrentPresetIndex ].PresetNumber ) );
                    }
                    _Stopwatch.Restart();
                }
                return;
            }
            //else if ( _CurrentAreaIndex < ( AreaArray.Length - 1 ) )
            //{
            //    _CurrentAreaIndex++;
            //    _CurrentPresetIndex = 0;
            //    _Start();
            //}
            //else
            //{
            //    _CurrentAreaIndex = 0;
            //    _CurrentPresetIndex = 0;
            //    _NextMoveTime = Default.AutoPatrolInterval * 1000L;
            //    _CurrentPatrolMode = PATROL_MODE.PATROL_INTERVAL;

            //    _PanheadAccessThread.SetPresetSpeed( 63 );
            //    _PanheadAccessThread.GotoPreset( Convert.ToByte( _PatrolTable[ AreaArray[ _CurrentAreaIndex ] ][ _CurrentPresetIndex ].PresetNumber ) );

            //    _Stopwatch.Restart();
            //}
            if ( _CurrentPatrolMode == PATROL_MODE.PATROL_WAIT_AREA_CHANGE )
            {
                if ( _Stopwatch.ElapsedMilliseconds < _NextMoveTime )
                {
                    return;
                }

                if ( _CurrentAreaIndex < ( AreaArray.Length - 1 ) )
                {
                    _CurrentAreaIndex++;
                    _CurrentPresetIndex = 0;
                    _Start();
                }
                else
                {
                    _CurrentAreaIndex = 0;
                    _CurrentPresetIndex = 0;
                    _NextMoveTime = Default.AutoPatrolInterval * 1000L;
                    _CurrentPatrolMode = PATROL_MODE.PATROL_INTERVAL;

                    _PanheadAccessThread.SetPresetSpeed( 63 );
                    _PanheadAccessThread.GotoPreset( Convert.ToByte( _PatrolTable[ AreaArray[ _CurrentAreaIndex ] ][ _CurrentPresetIndex ].PresetNumber ) );
                    //  2020/10/14 現地修正　GotoPreset3回出し
                    Thread.Sleep( 500 );
                    _PanheadAccessThread.GotoPreset( Convert.ToByte( _PatrolTable[ AreaArray[ _CurrentAreaIndex ] ][ _CurrentPresetIndex ].PresetNumber ) );
                    Thread.Sleep( 500 );
                    _PanheadAccessThread.GotoPreset( Convert.ToByte( _PatrolTable[ AreaArray[ _CurrentAreaIndex ] ][ _CurrentPresetIndex ].PresetNumber ) );

                    _Stopwatch.Restart();
                }
            }
        }

        void _Pause()
        {
            _Stopwatch.Stop();
            _PanheadAccessThread.MoveStop();
            _CurrentPatrolMode = PATROL_MODE.PATROL_WAIT_RESUME;
        }

        void _Alarm()
        {
            _Stopwatch.Stop();
            _PanheadAccessThread.MoveStop();

            if ( Default.PositionTweak == true )
            {
                _CurrentPatrolMode = PATROL_MODE.ALARM_POSITION;
            }
            else
            {
                _CurrentPatrolMode = PATROL_MODE.ALARM_SNAPSHOT;
            }
        }

        void _Resume()
        {
            _PatrolTable = _PresetTable.AutoPatrolTable();
            if ( _PatrolTable.Count <= 0 )
            {
                return;
            }

            AreaArray = _PatrolTable.Keys.ToArray<int>();

            try
            {
                if ( _PatrolTable[ AreaArray[ _CurrentAreaIndex ] ][ _CurrentPresetIndex ].FastMove == true )
                {
                    _PanheadAccessThread.SetPresetSpeed( Default.AreaChangePresetSpeed );
                }
                else
                {
                    _PanheadAccessThread.SetPresetSpeed( Default.PatrolPresetSpeed );
                }

                _PanheadAccessThread.GotoPreset( Convert.ToByte( _PatrolTable[ AreaArray[ _CurrentAreaIndex ] ][ _CurrentPresetIndex ].PresetNumber ) );
                //  2020/10/14 現地修正　GotoPreset3回出し
                Thread.Sleep( 500 );
                _PanheadAccessThread.GotoPreset( Convert.ToByte( _PatrolTable[ AreaArray[ _CurrentAreaIndex ] ][ _CurrentPresetIndex ].PresetNumber ) );
                Thread.Sleep( 500 );
                _PanheadAccessThread.GotoPreset( Convert.ToByte( _PatrolTable[ AreaArray[ _CurrentAreaIndex ] ][ _CurrentPresetIndex ].PresetNumber ) );
                _Stopwatch.Start();
                _CurrentPatrolMode = PATROL_MODE.PATROL_RUNNING;

                Trace.WriteLine(
                    $"Resume GoTo Preset #{_PatrolTable[ AreaArray[ _CurrentAreaIndex ] ][ _CurrentPresetIndex ].PresetNumber} " +
                    $"Pan={_PatrolTable[ AreaArray[ _CurrentAreaIndex ] ][ _CurrentPresetIndex ].Pan:F2} " +
                    $"Tilt={_PatrolTable[ AreaArray[ _CurrentAreaIndex ] ][ _CurrentPresetIndex ].Tilt:F2}" );
            }
            catch
            {
                _CurrentAreaIndex = 0;
                _CurrentPresetIndex = 0;
                _Start();
            }
        }

        void _AlarmPosition()
        {
            const int LEFT_LIMIT = ( 640 / 2 ) - 20;
            const int RIGHT_LIMIT = ( 640 / 2 ) + 20;
            const int UP_LIMIT = ( 480 / 2 ) - 20;
            const int DOWN_LIMIT = ( 480 / 2 ) + 20;
            const byte MOVE_SPEED = 3;

            if ( ( _TemperaturePosX.HasValue == false ) || ( _TemperaturePosY.HasValue == false ) || ( _CurrentTemperature.HasValue == false ) )
            {
                _CurrentPatrolMode = PATROL_MODE.PATROL_WAIT_RESUME;
                return;
            }

            Trace.WriteLine( $"X={_TemperaturePosX.Value} Y={_TemperaturePosY.Value}" );

            if ( _TemperaturePosX.Value < LEFT_LIMIT )
            {
                Trace.WriteLine( $"Alarm発生位置へ移動 (PAN = LEFT)" );
                _PanheadAccessThread.PanTilt( false, true, true, true, MOVE_SPEED );
            }
            else if ( _TemperaturePosX.Value > RIGHT_LIMIT )
            {
                Trace.WriteLine( $"Alarm発生位置へ移動 (PAN = RIGHT)" );
                _PanheadAccessThread.PanTilt( false, false, true, true, MOVE_SPEED );
            }
            else if ( _TemperaturePosY.Value < UP_LIMIT )
            {
                Trace.WriteLine( $"Alarm発生位置へ移動 (TILT = UP)" );
                _PanheadAccessThread.PanTilt( true, true, false, true, MOVE_SPEED );
            }
            else if ( _TemperaturePosY.Value > DOWN_LIMIT )
            {
                Trace.WriteLine( $"Alarm発生位置へ移動 (TILT = DOWN)" );
                _PanheadAccessThread.PanTilt( true, true, false, false, MOVE_SPEED );
            }
            else
            {
                Trace.WriteLine( $"Alarm発生位置で停止" );
                _PanheadAccessThread.MoveStop();
                //_CameraAccessThread.ExecAutoFocus();
                //Thread.Sleep( 10000 );
                _CurrentPatrolMode = PATROL_MODE.ALARM_SNAPSHOT;
            }
        }

        void _Snapshot()
        {
            if ( _AlarmTemperature.HasValue == true )
            {
                _CameraAccessThread.GetAlarmSnapshot(
                    AreaArray[ _CurrentAreaIndex ],
                    DateTime.Now,
                    _PresetTable.AreaName( AreaArray[ _CurrentAreaIndex ] ),
                    _AlarmTemperature.Value );
                _AlarmTemperature = null;
            }

            _CurrentPatrolMode = PATROL_MODE.PATROL_WAIT_RESUME;
        }
    }

    public class AutoPatrolEventArgs : EventArgs
    {
        public AutoPatrolEventArgs(int AreaNo, string Name, int PresetNo,float PresetPan,float PresetTilt, long Wait , AutoPatrolThread.PATROL_MODE CurrentPatrolMode ) : base()
        {
            AreaNumber = AreaNo;
            AreaName = Name;
            PresetNumber = PresetNo;
            Pan = PresetPan;
            Tilt = PresetTilt;
            WaitTime = Convert.ToSingle( Wait ) / 1000.0F;
            PatrolMode = CurrentPatrolMode;
        }

        public string AreaName
        {
            get;
            private set;
        }

        public int AreaNumber
        {
            get;
            private set;
        }

        public int PresetNumber
        {
            get;
            private set;
        }

        public float Pan
        {
            get;
            private set;
        }

        public float Tilt
        {
            get;
            private set;
        }

        public float WaitTime
        {
            get;
            private set;
        }

        public AutoPatrolThread.PATROL_MODE PatrolMode
        {
            get;
            private set;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Windows.Forms;
using System.Diagnostics;

namespace CES.ThermalPTZ
{
    public enum CAMERA_COMMAND
    {
        AUTO_FOCUS,
        TEMPERATURE_INFO,
        TEMPERATURE_IMAGE,
    }
}

namespace CES.ThermalPTZ.Threads
{
    using Camera;
    using static Properties.Settings;

    public class CameraAccessThread : IntervalThread
    {
        const int CAMERA_ACCESS_INTERVAL = 100;
        public event EventHandler<TemperatureInfoEventArgs> TemperatureInfo;
        public event EventHandler<TemperatureImageEventArgs> TemperatureImage;

        readonly IICAPI _IICAPI;
        readonly NetworkCredential _NetworkCredential;
        readonly WebClient _WebClient;

        public enum CAMERA_COMMAND
        {
            AUTOFOCUS,
            MANUALFOCUS,
            SNAPSHOT,
            ALARM_SNAPSHOT,
            TEMPERATURE
        }

        CAMERA_COMMAND? _Command;

        //  オートフォーカス実行設定
        //bool _EnableAutoFocus = false;
        //DateTime? _NextAutoFocusTime = null;

        //  温度情報取得設定
        readonly bool _EnableThermalInfo = true;
        DateTime? _NextThermalInfoTime = null;

        int? _AlarmArea;
        DateTime? _AlarmTimestamp;
        string _AlarmAreaName;
        float? _AlarmTemperature;

        int _FocusValue = 0;

        public CameraAccessThread( string CameraAddress, string CameraAccount, string CameraPassword, int CameraPort = 80, int Channel = 1 )
            : base( "camera", CAMERA_ACCESS_INTERVAL )
        {
            _IICAPI = new IICAPI( CameraAddress, CameraPort, Channel );
            _NetworkCredential = new NetworkCredential( CameraAccount, CameraPassword );
            _WebClient = new WebClient()
            {
                Credentials = _NetworkCredential
            };
        }

        protected override bool Init()
        {
            return base.Init();
        }

        public void GetAlarmSnapshot( int Area, DateTime Timestamp, string AreaName, float Temperature )
        {
            WaitCommandExec();
            _AlarmArea = Area;
            _AlarmAreaName = AreaName;
            _AlarmTimestamp = Timestamp;
            _AlarmTemperature = Temperature;
            _Command = CAMERA_COMMAND.ALARM_SNAPSHOT;
        }

        public void GetNormalSnapshot()
        {
            WaitCommandExec();
            _Command = CAMERA_COMMAND.SNAPSHOT;
        }

        public void ExecAutoFocus()
        {
            WaitCommandExec();
            _Command = CAMERA_COMMAND.AUTOFOCUS;
        }
        public void StartPatrol()
        {
            //StartAutoFocus();
            StartThermalInfo();
        }
        public void StopPatrol()
        {
            //StopAutoFocus();
            StopThermalInfo();
        }

        public void SetManualFocus( int Value )
        {
            WaitCommandExec();
            _FocusValue = Value;
            _Command = CAMERA_COMMAND.MANUALFOCUS;
        }

        //public void StartAutoFocus()
        //{
        //    _EnableAutoFocus = false;
        //    _NextAutoFocusTime = null;
        //}
        //public void StopAutoFocus()
        //{
        //    _EnableAutoFocus = false;
        //}
        public void StartThermalInfo()
        {
            _NextThermalInfoTime = null;
        }
        public void StopThermalInfo()
        {
        }

        protected override bool TimerRoutine()
        {
            if ( _Command.HasValue == true )
            {
                if ( ( _Command.Value == CAMERA_COMMAND.ALARM_SNAPSHOT ) ||
                    ( _Command.Value == CAMERA_COMMAND.SNAPSHOT ) )
                {
                    Snapshot();
                }
                else if ( _Command.Value == CAMERA_COMMAND.AUTOFOCUS )
                {
                    AutoFocus();
                }
                else if ( _Command == CAMERA_COMMAND.MANUALFOCUS )
                {
                    ManualFocus();
                }
                _Command = null;
                return true;
            }
            //if ( _EnableAutoFocus == true )
            //{
            //    if ( ( _NextAutoFocusTime.HasValue == false ) ||
            //        ( _NextAutoFocusTime.Value <= DateTime.Now ) )
            //    {
            //        AutoFocus();

            //        _NextAutoFocusTime = DateTime.Now.AddSeconds( Default.AreaChangeWaitTime );
            //    }
            //}
            if ( _EnableThermalInfo == true )
            {
                if ( ( _NextThermalInfoTime.HasValue == false ) ||
                    ( _NextThermalInfoTime.Value <= DateTime.Now ) )
                {
                    GetTemperatureInfo();

                    _NextThermalInfoTime = DateTime.Now.AddSeconds( Default.TemperatureWaitTime );
                }
            }

            return true;
        }

        void AutoFocus()
        {
            try
            {
                var Url = _IICAPI.ExecAutoFocusUrl();
                var res = _WebClient.DownloadString( Url );
                Trace.WriteLine( $"AutoFocus URL:{Url}" );
                Trace.WriteLine( $"AutoFocus RES:{res}" );
            }
            catch (Exception e )
            {
                Trace.WriteLine( e );
            }
        }

        void ManualFocus()
        {
            try
            {
                var Url = _IICAPI.ManualFocusUrl( _FocusValue );
                var res = _WebClient.DownloadString( Url );
                Trace.WriteLine( $"ManualFocus URL:{Url}" );
                Trace.WriteLine( $"ManualFocus RES:{res}" );
            }
            catch ( Exception e )
            {
                Trace.WriteLine( e );
            }
        }

        void GetTemperatureInfo()
        {
            try
            {
                var res = _WebClient.DownloadString( _IICAPI.TemperatureInfoUrl() );

                if ( _IICAPI.AnalyzeTemperatureInfo( res, out int PosX, out int PosY, out float MaxTemp ) == true )
                {
                    if ( TemperatureInfo != null )
                    {
                        if ( ( TemperatureInfo.Target != null ) && ( TemperatureInfo.Target is Control Cntl ) )
                        {
                            Cntl.BeginInvoke( TemperatureInfo, this, new TemperatureInfoEventArgs( PosX, PosY, MaxTemp ) );
                        }
                        else
                        {
                            TemperatureInfo( this, new TemperatureInfoEventArgs( PosX, PosY, MaxTemp ) );
                        }
                    }
                }
            }
            catch ( Exception e )
            {
                Trace.WriteLine( e );
                if ( TemperatureInfo != null )
                {
                    if ( ( TemperatureInfo.Target != null ) && ( TemperatureInfo.Target is Control Cntl ) )
                    {
                        Cntl.BeginInvoke( TemperatureInfo, this, new TemperatureInfoEventArgs( -1, -1, float.NaN ) );
                    }
                    else
                    {
                        TemperatureInfo( this, new TemperatureInfoEventArgs( -1, -1, float.NaN ) );
                    }
                }
            }
        }

        void Snapshot()
        {
            int TempAreaId = 0;
            string TempAreaName = string.Empty;
            DateTime TempTimeStamp = DateTime.Now;
            float TempTemperature = 0F;
            bool AlarmSnapshot = false;

            if ( _Command == CAMERA_COMMAND.ALARM_SNAPSHOT )
            {
                if ( ( _AlarmArea.HasValue == false ) || ( string.IsNullOrWhiteSpace( _AlarmAreaName ) == true ) || ( _AlarmTimestamp.HasValue == false ) || ( _AlarmTemperature.HasValue == false ) )
                {
                    return;
                }

                TempAreaId = _AlarmArea.Value;
                TempAreaName = _AlarmAreaName;
                TempTimeStamp = _AlarmTimestamp.Value;
                TempTemperature = _AlarmTemperature.Value;
                AlarmSnapshot = true;
            }

            try
            {
                var res = _WebClient.DownloadString( _IICAPI.TemperatureInfoUrl() );

                if ( _IICAPI.AnalyzeTemperatureInfo( res, out int PosX, out int PosY, out float MaxTemp ) == true )
                {
                    if ( TemperatureImage != null )
                    {
                        Image _Snapshot = null;
                        var ImageBytes = _WebClient.DownloadData( _IICAPI.StillImageUrl() );

                        using ( var Mem = new MemoryStream( ImageBytes ) )
                        {
                            _Snapshot = Image.FromStream( Mem );
                        }

                        //if ( _Command == CAMERA_COMMAND.ALARM_SNAPSHOT )
                        //{
                        //    DrawCircle( _Snapshot, PosX, PosY );
                        //}

                        if ( ( TemperatureImage.Target != null ) && ( TemperatureImage.Target is Control Cntl ) )
                        {
                            Cntl.BeginInvoke(
                                TemperatureImage,
                                this,
                                new TemperatureImageEventArgs(
                                    PosX,
                                    PosY,
                                    TempTemperature,
                                    TempAreaId,
                                    TempAreaName,
                                    TempTimeStamp,
                                    _Snapshot,
                                    AlarmSnapshot ) );
                        }
                        else
                        {
                            TemperatureImage(
                                this,
                                new TemperatureImageEventArgs(
                                    PosX,
                                    PosY,
                                    TempTemperature,
                                    TempAreaId,
                                    TempAreaName,
                                    TempTimeStamp,
                                    _Snapshot,
                                    AlarmSnapshot ) );
                        }
                    }
                }
            }
            catch ( Exception e )
            {
                Trace.WriteLine( e );
            }
            finally
            {
                _AlarmTemperature = null;
                _AlarmArea = null;
                _AlarmAreaName = string.Empty;
                _AlarmTimestamp = null;
            }
        }

        void DrawCircle( Image Snapshot, int PosX, int PosY )
        {
            if ( Snapshot == null )
            {
                return;
            }

            using ( var Graph = Graphics.FromImage( Snapshot ) )
            using ( var CirclePen = new Pen( Color.White, 5F ) )
            using ( var CircleBrush = new SolidBrush( Color.FromArgb( 64, 255, 255, 255 ) ) )
            {
                Graph.CompositingQuality = CompositingQuality.GammaCorrected;
                Graph.DrawEllipse( CirclePen, new Rectangle( new Point( PosX - 50, PosY - 50 ), new Size( 100, 100 ) ) );
                Graph.FillEllipse( CircleBrush, new Rectangle( new Point( PosX - 50, PosY - 50 ), new Size( 100, 100 ) ) );
            }
        }

        void FocusArea()
        {
            try
            {
                var Url = _IICAPI.GetFocusAreaUrl();
                var res = _WebClient.DownloadString( Url );
                Trace.WriteLine( $"AutoFocus URL:{Url}" );
                Trace.WriteLine( $"AutoFocus RES:{res}" );
            }
            catch ( Exception e )
            {
                Trace.WriteLine( e );
            }
        }

        void WaitCommandExec()
        {
            while ( _Command.HasValue == true )
            {
                Thread.Sleep( 5 );
            }
        }
    }

    public class TemperatureInfoEventArgs : EventArgs
    {
        public TemperatureInfoEventArgs( int PosX, int PosY, float Temp ) : base()
        {
            X = PosX;
            Y = PosY;
            Temperature = Temp;
        }

        public int X
        {
            get;
            private set;
        }

        public int Y
        {
            get;
            private set;
        }

        public float Temperature
        {
            get;
            private set;
        }
    }

    public class TemperatureImageEventArgs : TemperatureInfoEventArgs
    {
        public TemperatureImageEventArgs( int PosX, int PosY, float Temperature, int AlarmAreaNumber, string AlarmAreaName, DateTime AlarmTimestamp, Image Snapshot, bool AlarmSnapshot )
            : base( PosX, PosY, Temperature )
        {
            AreaNumber = AlarmAreaNumber;
            AreaName = AlarmAreaName;
            Timestamp = AlarmTimestamp;
            Image = Snapshot;
            Alarm = AlarmSnapshot;
        }

        public int AreaNumber
        {
            get;
            private set;
        }

        public string AreaName
        {
            get;
            private set;
        }

        public DateTime Timestamp
        {
            get;
            private set;
        }

        public Image Image
        {
            get;
            private set;
        }

        public bool Alarm
        {
            get;
            private set;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CES.ThermalPTZ.Threads
{
    public abstract class OneShotThread : AppThreadBase
    {
        public OneShotThread( string ThreadName = null, ThreadPriority Priority = ThreadPriority.Normal )
            :base( ThreadName , Priority )
        {
        }

        protected abstract void OneShotRoutine();

        protected override void ThreadRoutine()
        {
            OneShotRoutine();
        }
    }
}

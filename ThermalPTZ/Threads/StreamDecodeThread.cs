﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Drawing;
using System.Net;
using System.Windows.Forms;
using System.Diagnostics;

using RtspClientSharp;
using RtspClientSharp.RawFrames;
using RtspClientSharp.RawFrames.Video;

using OpenH264Lib;

namespace CES.ThermalPTZ.Threads
{
    public class StreamDecodeThread : OneShotThread
    {
        const string OPEN_H264_DLL_NAME = @".\Lib\openh264-1.8.0-win64.dll";

        public event EventHandler<ImageDecodedEventArgs> ImageDecoded;

        readonly string _RtspUrl;
        readonly string _RtspAccount;
        readonly string _RtspPassowrd;
        readonly ConnectionParameters _ConnectionParameters;
        readonly Decoder _Decoder = new Decoder( OPEN_H264_DLL_NAME );

        CancellationTokenSource _RtspConnectCancellationTokenSource = null;
        CancellationTokenSource _RtspReceiveCancellationTokenSource = null;
        RtspClient _RtspClient;

        public StreamDecodeThread(string Url,string Account,string Password)
            : base( "Stream" )
        {
            Trace.WriteLine( $"Decoding Thread Connection Stream Url={Url}" );
            _RtspUrl = Url;
            _RtspAccount = Account;
            _RtspPassowrd = Password;
            _ConnectionParameters = new ConnectionParameters(
                new Uri( _RtspUrl ),
                new NetworkCredential( _RtspAccount, _RtspPassowrd ) )
            {
                ConnectTimeout = TimeSpan.FromSeconds( 20 ),
                ReceiveTimeout = TimeSpan.FromSeconds( 10 ),
                RtpTransport = RtpTransportProtocol.TCP
            };
        }

        protected override bool Init()
        {
            return OpenURL();
        }

        protected override void OneShotRoutine()
        {
            while ( true )
            {
                try
                {
                    if ( OpenURL() == false )
                    {
                        Thread.Sleep( 1000 );
                        continue;
                    }

                    if ( _RtspReceiveCancellationTokenSource == null )
                    {
                        _RtspReceiveCancellationTokenSource = new CancellationTokenSource();
                    }
                    _RtspClient.ReceiveAsync( _RtspReceiveCancellationTokenSource.Token ).Wait();
                }
                catch ( ThreadAbortException )
                {
                    throw;
                }
                catch ( AccessViolationException )
                {
                    throw;
                }
                catch ( AggregateException ae)
                {
                    Exception inner = ae.InnerException;
                    while ( inner != null )
                    {
                        Trace.WriteLine( $"_RtspClient ReceiveAsync NG. {inner.Message}" );
                        inner = inner.InnerException;
                    }
                }
                catch ( Exception e)
                {
                    Trace.WriteLine( $"_RtspClient ReceiveAsync NG. {e.Message}" );
                }
                finally
                {
                    _RtspConnectCancellationTokenSource?.Dispose();
                    _RtspReceiveCancellationTokenSource?.Dispose();
                    _RtspClient?.Dispose();
                    Trace.WriteLine( $"_RtspClient Dispose()" );

                    _RtspConnectCancellationTokenSource = null;
                    _RtspReceiveCancellationTokenSource = null;
                    _RtspClient = null;
                }
            }
        }

        private bool OpenURL()
        {
            if ( ( _RtspClient != null ) &&
                ( _RtspConnectCancellationTokenSource != null ) &&
                ( _RtspConnectCancellationTokenSource.IsCancellationRequested == false ) )
            {
                return true;
            }


            try
            {
                _RtspConnectCancellationTokenSource?.Dispose();
                _RtspClient?.Dispose();

                _RtspConnectCancellationTokenSource = new CancellationTokenSource();
                _RtspClient = new RtspClient( _ConnectionParameters );
                _RtspClient.FrameReceived += this._RtspClient_FrameReceived;

                _RtspClient.ConnectAsync( _RtspConnectCancellationTokenSource.Token ).Wait();

                Trace.WriteLine( $"_RtspClient ConnectAsync OK. {_RtspClient.ConnectionParameters.ConnectionUri}" );
                return true;
            }
            catch ( ThreadAbortException )
            {
                throw;
            }
            catch ( Exception e )
            {
                Trace.WriteLine( $"_RtspClient ConnectAsync NG. {e.Message}" );

                _RtspConnectCancellationTokenSource?.Dispose();
                _RtspConnectCancellationTokenSource = null;

                _RtspClient?.Dispose();
                _RtspClient = null;

                Thread.Sleep( 10000 );
            }

            return false;
        }

        private void _RtspClient_FrameReceived( object sender, RawFrame e )
        {
            try
            {
                List<byte> EncodedImageBuffer = new List<byte>();

                if ( e is RawH264IFrame IFrame )
                {
                    var SpsPpsBuffer = new byte[ IFrame.SpsPpsSegment.Count ];
                    Array.Copy( IFrame.SpsPpsSegment.Array, IFrame.SpsPpsSegment.Offset, SpsPpsBuffer, 0, IFrame.SpsPpsSegment.Count );
                    EncodedImageBuffer.AddRange( SpsPpsBuffer );
                }

                var FrameBuffer = new byte[ e.FrameSegment.Count ];
                Array.Copy( e.FrameSegment.Array, e.FrameSegment.Offset, FrameBuffer, 0, e.FrameSegment.Count );
                EncodedImageBuffer.AddRange( FrameBuffer );

                DecodedImageEvent( _Decoder.Decode( EncodedImageBuffer.ToArray(), EncodedImageBuffer.Count ) );

                Thread.Sleep( 5 );
            }
            catch ( AggregateException )
            {
                return;
            }
        }

        private void DecodedImageEvent( Bitmap DecodedImage )
        {
            if ( ImageDecoded != null )
            {
                if ( ( ImageDecoded.Target != null ) && ( ImageDecoded.Target is Control Cntl ) )
                {
                    Cntl.BeginInvoke( ImageDecoded, this, new ImageDecodedEventArgs( DecodedImage ) );
                }
                else
                {
                    ImageDecoded( this, new ImageDecodedEventArgs( DecodedImage ) );
                }
            }
        }
    }

    public class ImageDecodedEventArgs : EventArgs
    {
        public ImageDecodedEventArgs(Bitmap DecodedImage) : base()
        {
            Image = DecodedImage;
        }

        public Image Image
        {
            get;
            private set;
        }
    }
}

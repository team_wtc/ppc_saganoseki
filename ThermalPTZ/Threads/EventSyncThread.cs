﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace CES.ThermalPTZ.Threads
{
    public abstract class EventSyncThread : AppThreadBase
    {
        readonly protected int _Timeout;
        readonly protected AutoResetEvent _AutoResetEvent;

        public EventSyncThread( int Timeout = Timeout.Infinite, string ThreadName = null, ThreadPriority Priority = ThreadPriority.Normal )
             : base( ThreadName, Priority )
        {
            _Timeout = Timeout;
            _AutoResetEvent = new AutoResetEvent( false );
        }

        protected virtual void TimeoutCallback()
        {
        }

        protected void DoEventAction()
        {
            _AutoResetEvent.Set();
        }
        protected abstract void EventCallback();


        protected override void ThreadRoutine()
        {
            while ( true )
            {
                if ( _AutoResetEvent.WaitOne( _Timeout ) == false )
                {
                    TimeoutCallback();
                    continue;
                }

                EventCallback();
            }
        }
    }
}

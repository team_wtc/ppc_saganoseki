﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CES.ThermalPTZ.Threads
{
    public abstract class AppThreadBase
    {
        protected readonly Thread _Thread;

        public AppThreadBase( string ThreadName = null, ThreadPriority Priority = ThreadPriority.Normal )
        {
            _Thread = new Thread( _CoreRoutine )
            {
                IsBackground = true,
                Name = ( string.IsNullOrWhiteSpace( ThreadName ) == false ) ? ThreadName : string.Empty,
                Priority = Priority
            };
        }

        public void Start()
        {
            _Thread.Start();
        }

        public void Stop()
        {
            if ( _Thread.IsAlive == true )
            {
                _Thread.Abort();
                _Thread.Join();
            }
        }

        public void Wait()
        {
            _Thread.Join();
        }

        public bool IsAlive => _Thread.IsAlive;

        protected virtual bool Init()
        {
            return true;
        }

        protected virtual void Terminate()
        {
        }

        protected abstract void ThreadRoutine();

        private void _CoreRoutine()
        {
            if ( Init() == false )
            {
                return;
            }

            try
            {
                ThreadRoutine();
            }
            catch ( ThreadAbortException )
            {
                Thread.ResetAbort();
                return;
            }
            catch
            {
                throw;
            }
            finally
            {
                Terminate();
            }
        }
    }
}

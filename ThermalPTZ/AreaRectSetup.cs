﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CES.ThermalPTZ
{
    public partial class AreaRectSetup : Form
    {
        const string LAYOUT_BACK_IMAGE = @".\Images\layout.jpg";
        readonly Data.PresetTable _PresetTable;
        readonly AreaRectPictureBox _AreaRectPictureBox;

        public AreaRectSetup()
        {
            InitializeComponent();
            _PresetTable = null;
        }

        public AreaRectSetup( Data.PresetTable PresetTable ) : this()
        {
            _PresetTable = PresetTable;
            _AreaRectPictureBox = new AreaRectPictureBox
            {
                BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center,
                Location = new System.Drawing.Point( 280, 12 ),
                Name = "AreaPictureBox",
                Size = new System.Drawing.Size( 800, 565 ),
                TabIndex = 2,
                TabStop = false
            };
            _AreaRectPictureBox.NewAreaRect += this._AreaRectPictureBox_NewAreaRect;
            this.Controls.Add( _AreaRectPictureBox );

        }

        private void _AreaRectPictureBox_NewAreaRect( object sender, NewAreaRectEventArgs e )
        {
            if ( e.HasRect == false )
            {
                NewAreaLeftLabel.Text = string.Empty;
                NewAreaTopLabel.Text = string.Empty;
                NewAreaWidthLabel.Text = string.Empty;
                NewAreaHeightLabel.Text = string.Empty;
            }
            else
            {
                NewAreaLeftLabel.Text = $"{e.Rect.Left}";
                NewAreaTopLabel.Text = $"{e.Rect.Top}";
                NewAreaWidthLabel.Text = $"{e.Rect.Width}";
                NewAreaHeightLabel.Text = $"{e.Rect.Height}";
            }
        }

        private void AlarmRectSetup_Load( object sender, EventArgs e )
        {
            AreaSelectComboBox.DataSource = _PresetTable.CreateAreaComboItem();
            AreaSelectComboBox.DisplayMember = "Value";
            AreaSelectComboBox.ValueMember = "Key";
            _AreaRectPictureBox.BackgroundImage = Image.FromFile( LAYOUT_BACK_IMAGE );
        }

        private void AreaSelectComboBox_SelectedIndexChanged( object sender, EventArgs e )
        {
            System.Diagnostics.Debug.WriteLine( $"AreaSelectComboBox.SelectedIndex={AreaSelectComboBox.SelectedIndex}" );
            if ( AreaSelectComboBox.SelectedItem is KeyValuePair<int, string> AreaInfo )
            {
                if ( _PresetTable.AreaRect( AreaInfo.Key, out int AreaLeft, out int AreaTop, out int AreaWidth, out int AreaHeight ) == true )
                {
                    _AreaRectPictureBox.SetAreaRect( AreaLeft, AreaTop, AreaWidth, AreaHeight );
                    AreaLeftLabel.Text = $"{AreaLeft}";
                    AreaTopLabel.Text = $"{AreaTop}";
                    AreaWidthLabel.Text = $"{AreaWidth}";
                    AreaHeightLabel.Text = $"{AreaHeight}";
                    return;
                }
            }
            _AreaRectPictureBox.ClearAreaRect();
            AreaLeftLabel.Text = string.Empty;
            AreaTopLabel.Text = string.Empty;
            AreaWidthLabel.Text = string.Empty;
            AreaHeightLabel.Text = string.Empty;
        }

        private void AreaUpdateButton_Click( object sender, EventArgs e )
        {
            if ( _AreaRectPictureBox.GetAreaRect( out int NewAreaLeft, out int NewAreaTop, out int NewAreaWidth, out int NewAreaHeight ) == true )
            {
                if ( AreaSelectComboBox.SelectedItem is KeyValuePair<int, string> AreaInfo )
                {
                    _PresetTable.UpdateAreaRect( AreaInfo.Key, NewAreaLeft, NewAreaTop, NewAreaWidth, NewAreaHeight );

                    if ( _PresetTable.AreaRect( AreaInfo.Key, out int AreaLeft, out int AreaTop, out int AreaWidth, out int AreaHeight ) == true )
                    {
                        _AreaRectPictureBox.SetAreaRect( AreaLeft, AreaTop, AreaWidth, AreaHeight );
                        AreaLeftLabel.Text = $"{AreaLeft}";
                        AreaTopLabel.Text = $"{AreaTop}";
                        AreaWidthLabel.Text = $"{AreaWidth}";
                        AreaHeightLabel.Text = $"{AreaHeight}";
                    }
                }
            }
        }
    }
}

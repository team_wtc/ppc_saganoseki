﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CES.ThermalPTZ
{
    using Data;

    public partial class AreaSetupForm : Form
    {
        readonly PresetTable _PresetTable = null;
        public AreaSetupForm()
        {
            InitializeComponent();
            _PresetTable = null;
        }

        public AreaSetupForm( PresetTable Table ) : this()
        {
            _PresetTable = Table;
        }

        private void AreaSetupForm_Load( object sender, EventArgs e )
        {
            if ( _PresetTable != null )
            {
                AreaListView.Items.AddRange( _PresetTable.CreateAreaListViewItem() );
            }
        }

        private void NewAreaButton_Click( object sender, EventArgs e )
        {
            if ( _PresetTable != null )
            {
                int? Left = null;
                int? Top = null;
                int? Width = null;
                int? Height = null;

                if ( int.TryParse( AreaLeftTextBox.Text, out int TempLeft ) == true )
                {
                    Left = TempLeft;
                }
                if ( int.TryParse( AreaTopTextBox.Text, out int TempTop ) == true )
                {
                    Top = TempTop;
                }
                if ( int.TryParse( AreaWidthTextBox.Text, out int TempWidth ) == true )
                {
                    Width = TempWidth;
                }
                if ( int.TryParse( AreaHeightTextBox.Text, out int TempHeight ) == true )
                {
                    Height = TempHeight;
                }

                _PresetTable.AddArea( AreaNameTextBox.Text, Left, Top, Width, Height );

                AreaListView.Items.Clear();
                AreaListView.Items.AddRange( _PresetTable.CreateAreaListViewItem() );
            }
        }

        private void UpdateAreaButton_Click( object sender, EventArgs e )
        {
            if ( AreaListView.SelectedItems.Count <= 0 )
            {
                return;
            }

            if ( AreaListView.SelectedItems[ 0 ].Tag is ThermalDataSet.AREARow AreaRow )
            {
                if ( _PresetTable != null )
                {
                    int? Left = null;
                    int? Top = null;
                    int? Width = null;
                    int? Height = null;

                    if ( int.TryParse( AreaLeftTextBox.Text, out int TempLeft ) == true )
                    {
                        Left = TempLeft;
                    }
                    if ( int.TryParse( AreaTopTextBox.Text, out int TempTop ) == true )
                    {
                        Top = TempTop;
                    }
                    if ( int.TryParse( AreaWidthTextBox.Text, out int TempWidth ) == true )
                    {
                        Width = TempWidth;
                    }
                    if ( int.TryParse( AreaHeightTextBox.Text, out int TempHeight ) == true )
                    {
                        Height = TempHeight;
                    }

                    _PresetTable.UpdateArea( AreaRow.area_id, AreaNameTextBox.Text, Left, Top, Width, Height );

                    AreaIdTextBox.Text = string.Empty;
                    AreaNameTextBox.Text = string.Empty;
                    AreaLeftTextBox.Text = string.Empty;
                    AreaTopTextBox.Text = string.Empty;
                    AreaWidthTextBox.Text = string.Empty;
                    AreaHeightTextBox.Text = string.Empty;
                    AreaListView.Items.Clear();
                    AreaListView.Items.AddRange( _PresetTable.CreateAreaListViewItem() );
                }
            }
        }

        private void AreaListView_SelectedIndexChanged( object sender, EventArgs e )
        {
            if ( AreaListView.SelectedItems.Count <= 0 )
            {
                AreaIdTextBox.Text = string.Empty;
                AreaNameTextBox.Text = string.Empty;
                AreaLeftTextBox.Text = string.Empty;
                AreaTopTextBox.Text = string.Empty;
                AreaWidthTextBox.Text = string.Empty;
                AreaHeightTextBox.Text = string.Empty;

                NewAreaButton.Enabled = false;
                UpdateAreaButton.Enabled = false;
                DeleteAreaButton.Enabled = false;
                return;
            }

            if ( AreaListView.SelectedItems[ 0 ].Tag is ThermalDataSet.AREARow AreaRow )
            {
                NewAreaButton.Enabled = true;
                UpdateAreaButton.Enabled = true;
                DeleteAreaButton.Enabled = true;

                AreaIdTextBox.Text = $"{AreaRow.area_id}";
                AreaNameTextBox.Text = AreaRow.area_name;
                if ( AreaRow.Isarea_location_leftNull() == false )
                {
                    AreaLeftTextBox.Text = $"{AreaRow.area_location_left}";
                }
                else
                {
                    AreaLeftTextBox.Text = string.Empty;
                }
                if ( AreaRow.Isarea_location_topNull() == false )
                {
                    AreaTopTextBox.Text = $"{AreaRow.area_location_top}";
                }
                else
                {
                    AreaTopTextBox.Text = string.Empty;
                }
                if ( AreaRow.Isarea_widthNull() == false )
                {
                    AreaWidthTextBox.Text = $"{AreaRow.area_width}";
                }
                else
                {
                    AreaWidthTextBox.Text = string.Empty;
                }
                if ( AreaRow.Isarea_heightNull() == false )
                {
                    AreaHeightTextBox.Text = $"{AreaRow.area_height}";
                }
                else
                {
                    AreaHeightTextBox.Text = string.Empty;
                }
            }
        }

        private void DeleteAreaButton_Click( object sender, EventArgs e )
        {
            if ( AreaListView.SelectedItems.Count <= 0 )
            {
                return;
            }
            if ( AreaListView.SelectedItems[ 0 ].Tag is ThermalDataSet.AREARow AreaRow )
            {
                if ( MessageBox.Show(
                    $"エリア[{AreaRow.area_name}]を削除します。{Environment.NewLine}" +
                    $"エリアを削除すると、対応するプリセット情報も削除されます。{Environment.NewLine}" +
                    $"削除してよろしいですか？",
                    "エリア削除",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question ) != DialogResult.Yes )
                {
                    return;
                }

                _PresetTable?.DeleteArea( AreaRow.area_id );

                AreaIdTextBox.Text = string.Empty;
                AreaNameTextBox.Text = string.Empty;
                AreaLeftTextBox.Text = string.Empty;
                AreaTopTextBox.Text = string.Empty;
                AreaWidthTextBox.Text = string.Empty;
                AreaHeightTextBox.Text = string.Empty;
                AreaListView.Items.Clear();
                AreaListView.Items.AddRange( _PresetTable.CreateAreaListViewItem() );
            }
        }
    }
}

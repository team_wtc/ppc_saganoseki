﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

namespace CES.ThermalPTZ.Log
{
    public class ThermalPTZTraceListener : TraceListener
    {
        const long DEFAULT_MAX_LOG_SIZE = 5L * 1024L * 1024L;

        readonly string _LogBaseFolder;
        readonly long _MaxLogSize;
        readonly object _LockObj = new object();

        StreamWriter _StreamWriter = null;
        string _CurrentLogFolder = null;

        public ThermalPTZTraceListener()
            : this( DEFAULT_MAX_LOG_SIZE )
        {
        }

        public ThermalPTZTraceListener( long MaxSize )
            : this( Path.Combine( Application.StartupPath, "Log" ), MaxSize )
        {
        }

        public ThermalPTZTraceListener( string BaseFolder, long MaxSize )
        {
            _LogBaseFolder = BaseFolder;
            _MaxLogSize = MaxSize;
        }

        public override void Write( string message )
        {

            lock ( _LockObj )
            {
                CreateLogStream();
                _StreamWriter?.Write( message );
                _StreamWriter?.Flush();
            }
        }

        public override void WriteLine( string message )
        {
            lock ( _LockObj )
            {
                CreateLogStream();
                _StreamWriter?.WriteLine( LogString( message ) );
                _StreamWriter?.Flush();
            }
        }

        public override void WriteLine( object o )
        {
            lock ( _LockObj )
            {
                if ( o is Exception e )
                {
                    CreateLogStream();
                    _StreamWriter?.WriteLine( ExceptionString( e ) );
                    _StreamWriter?.Flush();
                }
            }
        }

        private string LogString( string message )
        {
            string ThreadName = Thread.CurrentThread.Name;
            if ( string.IsNullOrWhiteSpace( ThreadName ) == true )
            {
                if ( Thread.CurrentThread.IsThreadPoolThread == true )
                {
                    ThreadName = "(Pool)";
                }
                else
                {
                    ThreadName = $"{Thread.CurrentThread.ManagedThreadId:X10}";
                }
            }
            return
                $"{DateTime.Now.ToString( "HH:mm:ss.fff" )} " +
                $"[{ThreadName,-10}] " +
                $"{message}";
        }
        private string ExceptionString( Exception e )
        {
            StringBuilder Builder = new StringBuilder( $"{LogString( $"{e.GetType()} {e.Message}" )}{Environment.NewLine}" );
            Exception Inner = e.InnerException;
            while ( Inner != null )
            {
                Builder.Append( $"{LogString( $"{Inner.GetType()} {Inner.Message}" )}{Environment.NewLine}" );
                Inner = Inner.InnerException;
            }
            string[] StackTraceStrings = e.StackTrace.Replace( "\r\n", "\n" ).Split( '\n' );
            for ( int i = 0 ; i < StackTraceStrings.Length ; i++ )
            {
                Builder.Append( $"{LogString( StackTraceStrings[ i ] )}" );
                if ( i < ( StackTraceStrings.Length - 1 ) )
                {
                    Builder.AppendLine();
                }
            }
            return Builder.ToString();
        }

        private void CreateLogStream()
        {
            var LogFolder = Path.Combine( _LogBaseFolder, DateTime.Today.ToString( "yyyyMMdd" ) );
            var LogName = Path.Combine( LogFolder, $"{DateTime.Now.ToString( "yyyyMMdd_HHmmss" )}.log" );

            if ( Directory.Exists( LogFolder ) == false )
            {
                Directory.CreateDirectory( LogFolder );
            }

            if ( ( string.IsNullOrWhiteSpace( _CurrentLogFolder ) == true ) ||
                ( LogFolder.Equals( _CurrentLogFolder, StringComparison.CurrentCultureIgnoreCase ) == false ) )
            {
                Close();
            }
            if ( ( _StreamWriter != null ) && ( _StreamWriter.BaseStream.Length > _MaxLogSize ) )
            {
                Close();
            }

            if ( _StreamWriter == null )
            {
                _CurrentLogFolder = LogFolder;
                _StreamWriter = new StreamWriter(
                    LogName,
                    true,
                    Encoding.UTF8 )
                {
                    AutoFlush = true
                };
            }
        }

        public override void Close()
        {
            lock ( _LockObj )
            {
                _StreamWriter?.Flush();
                _StreamWriter?.Close();
                _StreamWriter?.Dispose();
                _StreamWriter = null;
            }
        }
        public override void Flush()
        {
            lock ( _LockObj )
            {
                if ( _StreamWriter != null )
                {
                    _StreamWriter.Flush();
                }
            }
        }
    }
}

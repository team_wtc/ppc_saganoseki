﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CES.ThermalPTZ.Camera
{
    public enum IICAPI_SERVICE
    {
        system, //System Service (system.cgi)
        serial, //Serial Service (serial.cgi)
        security,   //Security Service (security.cgi)
        streaming,  //Streaming Service (streaming.cgi)
        storage,    //Storage Service (storage.cgi)
        custom, //Custom Service (custom.cgi)
        ptz,    //PTZ Service (ptz.cgi)
        thermal,    //Thermal Service (thermal.cgi)
        Firmware    //Firmware Service
    }

    public enum ACTION
    {
        gets,
        sets,
        dels,
        exec,
        getData,
        play,
        stop,
        export,
        import
    }

    public class IICAPI
    {
        const int HTTP_PORT = 80;
        const int HTTP_SSL_PORT = 443;
        const int DEFAULT_CHANNEL = 1;

        static readonly Dictionary<IICAPI_SERVICE, string> CGI_NAME_TABLE = null;
        static readonly Dictionary<ACTION, string> ACTION_NAME_TABLE = null;

        static IICAPI()
        {
            CGI_NAME_TABLE = new Dictionary<IICAPI_SERVICE, string>
            {
                [ IICAPI_SERVICE.system ] = "system.cgi",
                [ IICAPI_SERVICE.serial ] = "serial.cgi",
                [ IICAPI_SERVICE.security ] = "security.cgi",
                [ IICAPI_SERVICE.streaming ] = "streaming.cgi",
                [ IICAPI_SERVICE.storage ] = "storage.cgi",
                [ IICAPI_SERVICE.custom ] = "custom.cgi",
                [ IICAPI_SERVICE.ptz ] = "ptz.cgi",
                [ IICAPI_SERVICE.thermal ] = "thermal.cgi",
                [ IICAPI_SERVICE.Firmware ] = "Firmware",
            };

            ACTION_NAME_TABLE = new Dictionary<ACTION, string>
            {
                [ ACTION.dels ] = "dels",
                [ ACTION.exec ] = "exec",
                [ ACTION.export ] = "export",
                [ ACTION.getData ] = "getData",
                [ ACTION.gets ] = "gets",
                [ ACTION.import ] = "import",
                [ ACTION.play ] = "play",
                [ ACTION.sets ] = "sets",
                [ ACTION.stop ] = "stop",
            };
        }

        readonly string _CameraAddress;
        readonly int _CameraPort;
        readonly int _CameraChannel;

        public IICAPI( string CameraAddress , int CameraPort= HTTP_PORT, int Channel = DEFAULT_CHANNEL )
        {
            _CameraAddress = CameraAddress;
            _CameraPort = CameraPort;
            _CameraChannel = Channel;
        }

        public Uri StillImageUrl()
        {
            const string STILL_IMAGE_SVC = "stillImage";
            return Create( IICAPI_SERVICE.storage, STILL_IMAGE_SVC, ACTION.exec, $"channels=ch{_CameraChannel}" );
        }

        public Uri ExecAutoFocusUrl()
        {
            const string AUTO_FOCUS_SVC = "onepushAutoFocus";
            return Create( IICAPI_SERVICE.ptz, AUTO_FOCUS_SVC, ACTION.exec );
        }

        public Uri GetFocusAreaUrl()
        {
            const string AUTO_FOCUS_AREA_SVC = "autoFocusArea";
            return Create( IICAPI_SERVICE.ptz, AUTO_FOCUS_AREA_SVC, ACTION.gets );
        }

        public Uri TemperatureInfoUrl()
        {
            const string TEMP_INFO_SVC = "tempAdvInfo";
            return Create( IICAPI_SERVICE.thermal, TEMP_INFO_SVC, ACTION.gets );
        }

        public Uri ManualFocusUrl( int FocusValue )
        {
            const string FOCUS_SVC = "focus";
            const string PLAY_PARAM = "nearfar";

            if ( FocusValue == 0 )
            {
                return Create( IICAPI_SERVICE.ptz, FOCUS_SVC, ACTION.stop );
            }
            else
            {
                return Create( IICAPI_SERVICE.ptz, FOCUS_SVC, ACTION.play, $"{PLAY_PARAM}={FocusValue}" );
            }
        }

        public bool AnalyzeTemperatureInfo( string ResponceString, out int PositionX, out int PositionY, out float MaxTemperature )
        {
            const string POS_X_PATTERN = ".*max_temp_pt_x=(?<X>\\d+).*";
            const string POS_Y_PATTERN = ".*max_temp_pt_Y=(?<Y>\\d+).*";
            const string TEMP_PATTERN = "^.*max_temp_value=(?<T>\\d+\\.*\\d*).*";

            var XMatch = Regex.Match( ResponceString, POS_X_PATTERN, RegexOptions.IgnoreCase | RegexOptions.Multiline );
            var YMatch = Regex.Match( ResponceString, POS_Y_PATTERN, RegexOptions.IgnoreCase | RegexOptions.Multiline );
            var TMatch = Regex.Match( ResponceString, TEMP_PATTERN, RegexOptions.IgnoreCase | RegexOptions.Multiline );

            if ( ( XMatch.Success != true ) || ( YMatch.Success != true ) || ( TMatch.Success != true ) )
            {
                PositionX = -1;
                PositionY = -1;
                MaxTemperature = 0F;
                return false;
            }

            try
            {
                PositionX = Convert.ToInt32( XMatch.Groups[ "X" ].Value );
                PositionY = Convert.ToInt32( YMatch.Groups[ "Y" ].Value );
                MaxTemperature = Convert.ToSingle( TMatch.Groups[ "T" ].Value );
                return true;
            }
            catch
            {
                PositionX = -1;
                PositionY = -1;
                MaxTemperature = 0F;
                return false;
            }


        }

        private Uri Create( IICAPI_SERVICE Service, string Svc, ACTION Action, string AddQuery = null )
        {
            var Builder = new UriBuilder()
            {
                Host = _CameraAddress,
                Port = _CameraPort,
                Scheme = ( _CameraPort == HTTP_SSL_PORT ) ? Uri.UriSchemeHttps : Uri.UriSchemeHttp,
                Path = $"/cgi-bin/httpapi/{CGI_NAME_TABLE[ Service ]}",
                Query =$"svc={Svc}&action={ACTION_NAME_TABLE[ Action ]}"
            };

            if ( string.IsNullOrWhiteSpace( AddQuery ) == false )
            {
                Builder.Query += $"&{AddQuery}";
            }

            return Builder.Uri;
        }
    }
}

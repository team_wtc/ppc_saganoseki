﻿namespace CES.ThermalPTZ
{
    partial class SnapshotForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SetAlarmImageButton = new System.Windows.Forms.Button();
            this.ImageSaveButton = new System.Windows.Forms.Button();
            this.CloseButton = new System.Windows.Forms.Button();
            this.SnapshotPictureBox = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.SnapshotPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // SetAlarmImageButton
            // 
            this.SetAlarmImageButton.Location = new System.Drawing.Point(12, 498);
            this.SetAlarmImageButton.Name = "SetAlarmImageButton";
            this.SetAlarmImageButton.Size = new System.Drawing.Size(210, 32);
            this.SetAlarmImageButton.TabIndex = 1;
            this.SetAlarmImageButton.Text = "最新警報画像に設定";
            this.SetAlarmImageButton.UseVisualStyleBackColor = true;
            this.SetAlarmImageButton.Click += new System.EventHandler(this.SetAlarmImageButton_Click);
            // 
            // ImageSaveButton
            // 
            this.ImageSaveButton.Location = new System.Drawing.Point(227, 498);
            this.ImageSaveButton.Name = "ImageSaveButton";
            this.ImageSaveButton.Size = new System.Drawing.Size(210, 32);
            this.ImageSaveButton.TabIndex = 2;
            this.ImageSaveButton.Text = "画像ファイルに保存";
            this.ImageSaveButton.UseVisualStyleBackColor = true;
            this.ImageSaveButton.Click += new System.EventHandler(this.ImageSaveButton_Click);
            // 
            // CloseButton
            // 
            this.CloseButton.Location = new System.Drawing.Point(442, 498);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(210, 32);
            this.CloseButton.TabIndex = 3;
            this.CloseButton.Text = "閉じる";
            this.CloseButton.UseVisualStyleBackColor = true;
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // SnapshotPictureBox
            // 
            this.SnapshotPictureBox.Location = new System.Drawing.Point(12, 12);
            this.SnapshotPictureBox.Name = "SnapshotPictureBox";
            this.SnapshotPictureBox.Size = new System.Drawing.Size(640, 480);
            this.SnapshotPictureBox.TabIndex = 0;
            this.SnapshotPictureBox.TabStop = false;
            // 
            // SnapshotForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(663, 536);
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.ImageSaveButton);
            this.Controls.Add(this.SetAlarmImageButton);
            this.Controls.Add(this.SnapshotPictureBox);
            this.Font = new System.Drawing.Font("メイリオ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SnapshotForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "スナップショット";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SnapshotForm_FormClosing);
            this.Load += new System.EventHandler(this.SnapshotForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.SnapshotPictureBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox SnapshotPictureBox;
        private System.Windows.Forms.Button SetAlarmImageButton;
        private System.Windows.Forms.Button ImageSaveButton;
        private System.Windows.Forms.Button CloseButton;
    }
}
﻿namespace CES.ThermalPTZ
{
    partial class AreaSetupForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.AreaListView = new System.Windows.Forms.ListView();
            this.AreaDummyColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.AreaIdColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.AreaNameColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.AreaLeftColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.AreaTopColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.AreaWidthColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.AreaHeightColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.AreaIdTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.AreaNameTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.AreaLeftTextBox = new System.Windows.Forms.TextBox();
            this.AreaTopTextBox = new System.Windows.Forms.TextBox();
            this.AreaWidthTextBox = new System.Windows.Forms.TextBox();
            this.AreaHeightTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.NewAreaButton = new System.Windows.Forms.Button();
            this.UpdateAreaButton = new System.Windows.Forms.Button();
            this.DeleteAreaButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // AreaListView
            // 
            this.AreaListView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AreaListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.AreaDummyColumnHeader,
            this.AreaIdColumnHeader,
            this.AreaNameColumnHeader,
            this.AreaLeftColumnHeader,
            this.AreaTopColumnHeader,
            this.AreaWidthColumnHeader,
            this.AreaHeightColumnHeader});
            this.AreaListView.FullRowSelect = true;
            this.AreaListView.GridLines = true;
            this.AreaListView.HideSelection = false;
            this.AreaListView.Location = new System.Drawing.Point(12, 12);
            this.AreaListView.MultiSelect = false;
            this.AreaListView.Name = "AreaListView";
            this.AreaListView.Size = new System.Drawing.Size(686, 338);
            this.AreaListView.TabIndex = 0;
            this.AreaListView.UseCompatibleStateImageBehavior = false;
            this.AreaListView.View = System.Windows.Forms.View.Details;
            this.AreaListView.SelectedIndexChanged += new System.EventHandler(this.AreaListView_SelectedIndexChanged);
            // 
            // AreaDummyColumnHeader
            // 
            this.AreaDummyColumnHeader.Text = "";
            this.AreaDummyColumnHeader.Width = 2;
            // 
            // AreaIdColumnHeader
            // 
            this.AreaIdColumnHeader.Text = "ID";
            this.AreaIdColumnHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.AreaIdColumnHeader.Width = 50;
            // 
            // AreaNameColumnHeader
            // 
            this.AreaNameColumnHeader.Text = "エリア名";
            this.AreaNameColumnHeader.Width = 200;
            // 
            // AreaLeftColumnHeader
            // 
            this.AreaLeftColumnHeader.Text = "座標(Left)";
            this.AreaLeftColumnHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.AreaLeftColumnHeader.Width = 100;
            // 
            // AreaTopColumnHeader
            // 
            this.AreaTopColumnHeader.Text = "座標(Top)";
            this.AreaTopColumnHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.AreaTopColumnHeader.Width = 100;
            // 
            // AreaWidthColumnHeader
            // 
            this.AreaWidthColumnHeader.Text = "幅";
            this.AreaWidthColumnHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.AreaWidthColumnHeader.Width = 100;
            // 
            // AreaHeightColumnHeader
            // 
            this.AreaHeightColumnHeader.Text = "高さ";
            this.AreaHeightColumnHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.AreaHeightColumnHeader.Width = 100;
            // 
            // AreaIdTextBox
            // 
            this.AreaIdTextBox.Location = new System.Drawing.Point(188, 356);
            this.AreaIdTextBox.Name = "AreaIdTextBox";
            this.AreaIdTextBox.ReadOnly = true;
            this.AreaIdTextBox.Size = new System.Drawing.Size(100, 31);
            this.AreaIdTextBox.TabIndex = 1;
            this.AreaIdTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(138, 363);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 24);
            this.label1.TabIndex = 2;
            this.label1.Text = "ID：";
            // 
            // AreaNameTextBox
            // 
            this.AreaNameTextBox.Location = new System.Drawing.Point(188, 393);
            this.AreaNameTextBox.Name = "AreaNameTextBox";
            this.AreaNameTextBox.Size = new System.Drawing.Size(221, 31);
            this.AreaNameTextBox.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(76, 396);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(106, 24);
            this.label2.TabIndex = 4;
            this.label2.Text = "エリア名称：";
            // 
            // AreaLeftTextBox
            // 
            this.AreaLeftTextBox.Location = new System.Drawing.Point(188, 430);
            this.AreaLeftTextBox.Name = "AreaLeftTextBox";
            this.AreaLeftTextBox.ReadOnly = true;
            this.AreaLeftTextBox.Size = new System.Drawing.Size(100, 31);
            this.AreaLeftTextBox.TabIndex = 7;
            this.AreaLeftTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // AreaTopTextBox
            // 
            this.AreaTopTextBox.Location = new System.Drawing.Point(294, 430);
            this.AreaTopTextBox.Name = "AreaTopTextBox";
            this.AreaTopTextBox.ReadOnly = true;
            this.AreaTopTextBox.Size = new System.Drawing.Size(100, 31);
            this.AreaTopTextBox.TabIndex = 8;
            this.AreaTopTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // AreaWidthTextBox
            // 
            this.AreaWidthTextBox.Location = new System.Drawing.Point(400, 430);
            this.AreaWidthTextBox.Name = "AreaWidthTextBox";
            this.AreaWidthTextBox.ReadOnly = true;
            this.AreaWidthTextBox.Size = new System.Drawing.Size(100, 31);
            this.AreaWidthTextBox.TabIndex = 9;
            this.AreaWidthTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // AreaHeightTextBox
            // 
            this.AreaHeightTextBox.Location = new System.Drawing.Point(506, 430);
            this.AreaHeightTextBox.Name = "AreaHeightTextBox";
            this.AreaHeightTextBox.ReadOnly = true;
            this.AreaHeightTextBox.Size = new System.Drawing.Size(100, 31);
            this.AreaHeightTextBox.TabIndex = 10;
            this.AreaHeightTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(76, 433);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(106, 24);
            this.label4.TabIndex = 11;
            this.label4.Text = "エリア座標：";
            // 
            // NewAreaButton
            // 
            this.NewAreaButton.Location = new System.Drawing.Point(188, 469);
            this.NewAreaButton.Name = "NewAreaButton";
            this.NewAreaButton.Size = new System.Drawing.Size(152, 34);
            this.NewAreaButton.TabIndex = 12;
            this.NewAreaButton.Text = "新規";
            this.NewAreaButton.UseVisualStyleBackColor = true;
            this.NewAreaButton.Click += new System.EventHandler(this.NewAreaButton_Click);
            // 
            // UpdateAreaButton
            // 
            this.UpdateAreaButton.Location = new System.Drawing.Point(348, 469);
            this.UpdateAreaButton.Name = "UpdateAreaButton";
            this.UpdateAreaButton.Size = new System.Drawing.Size(152, 34);
            this.UpdateAreaButton.TabIndex = 13;
            this.UpdateAreaButton.Text = "更新";
            this.UpdateAreaButton.UseVisualStyleBackColor = true;
            this.UpdateAreaButton.Click += new System.EventHandler(this.UpdateAreaButton_Click);
            // 
            // DeleteAreaButton
            // 
            this.DeleteAreaButton.Location = new System.Drawing.Point(506, 469);
            this.DeleteAreaButton.Name = "DeleteAreaButton";
            this.DeleteAreaButton.Size = new System.Drawing.Size(152, 34);
            this.DeleteAreaButton.TabIndex = 14;
            this.DeleteAreaButton.Text = "削除";
            this.DeleteAreaButton.UseVisualStyleBackColor = true;
            this.DeleteAreaButton.Click += new System.EventHandler(this.DeleteAreaButton_Click);
            // 
            // AreaSetupForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(710, 512);
            this.Controls.Add(this.DeleteAreaButton);
            this.Controls.Add(this.UpdateAreaButton);
            this.Controls.Add(this.NewAreaButton);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.AreaHeightTextBox);
            this.Controls.Add(this.AreaWidthTextBox);
            this.Controls.Add(this.AreaTopTextBox);
            this.Controls.Add(this.AreaLeftTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.AreaNameTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.AreaIdTextBox);
            this.Controls.Add(this.AreaListView);
            this.Font = new System.Drawing.Font("メイリオ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Name = "AreaSetupForm";
            this.Text = "エリア設定";
            this.Load += new System.EventHandler(this.AreaSetupForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView AreaListView;
        private System.Windows.Forms.ColumnHeader AreaDummyColumnHeader;
        private System.Windows.Forms.ColumnHeader AreaIdColumnHeader;
        private System.Windows.Forms.ColumnHeader AreaNameColumnHeader;
        private System.Windows.Forms.ColumnHeader AreaLeftColumnHeader;
        private System.Windows.Forms.ColumnHeader AreaTopColumnHeader;
        private System.Windows.Forms.ColumnHeader AreaWidthColumnHeader;
        private System.Windows.Forms.ColumnHeader AreaHeightColumnHeader;
        private System.Windows.Forms.TextBox AreaIdTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox AreaNameTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox AreaLeftTextBox;
        private System.Windows.Forms.TextBox AreaTopTextBox;
        private System.Windows.Forms.TextBox AreaWidthTextBox;
        private System.Windows.Forms.TextBox AreaHeightTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button NewAreaButton;
        private System.Windows.Forms.Button UpdateAreaButton;
        private System.Windows.Forms.Button DeleteAreaButton;
    }
}
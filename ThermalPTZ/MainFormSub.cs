﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace CES.ThermalPTZ
{
    using Threads;
    using Images;
    using IOBox;
    using Data;
    using static Properties.Settings;

    public partial class MainForm : Form
    {
        const byte MAX_SPEED = 0x3F;
        const byte HOME_POSITION = 0x3F;    //  1

        StreamDecodeThread _StreamDecodeThread = null;
        PanheadAccessThread _PanheadAccessThread = null;
        CameraAccessThread _CameraAccessThread = null;
        IOSetter _IOSetter = null;
        AutoPatrolThread _AutoPatrolThread = null;
        //IOListener _IOListener = null;

        readonly PanTiltImage _PanImage = new PanTiltImage( PanTiltImage.DRAW_TARGET.PAN );
        readonly PanTiltImage _TiltImage = new PanTiltImage( PanTiltImage.DRAW_TARGET.TILT );
        readonly PanTiltImage _LayoutImage = new PanTiltImage( PanTiltImage.DRAW_TARGET.LAYOUT );
        readonly PresetTable _PresetTable = new PresetTable();
        readonly List<AutoPatrolInfo> _AutoPatrolTable = new List<AutoPatrolInfo>();

        PictureBox _FullScreenPictureBox;
        bool _Alarm = false;
        DateTime? _SendAutoStart = null;
        float _PanAngle = float.NaN;
        int? _CurrentAreaNumber = null;

        int _AlarmCount = 0;

        void InitScreen()
        {
#if DEBUG
            if ( Screen.AllScreens.Length > 1 )
            {
                Location = Screen.AllScreens[ 1 ].Bounds.Location;
                Size = Screen.AllScreens[ 1 ].Bounds.Size;
            }
            else
            {
                Location = Screen.PrimaryScreen.Bounds.Location;
                Size = Screen.PrimaryScreen.Bounds.Size;
            }
#else
            Location = Screen.PrimaryScreen.Bounds.Location;
            Size = Screen.PrimaryScreen.Bounds.Size;
#endif

            BaseSplitContainer.SplitterDistance = Default.SplitterDistance;

            AutoPatrolCheckBox.Checked = false;
            AutoPatrolMode( false );
            if ( Default.AutoStartPatrol == true )
            {
                MainTitleLabel.Text = "自動巡回開始準備中";
                _SendAutoStart = DateTime.Now.AddSeconds( 15.0 );
            }

            _FullScreenPictureBox = new PictureBox
            {
                Location = Point.Empty,
                Size = Size,
                BackColor = Color.Black,
                SizeMode = PictureBoxSizeMode.Zoom,
                Visible = false
            };
            _FullScreenPictureBox.MouseDoubleClick += new MouseEventHandler( this.FullScreenPictureBox_MouseDoubleClick );
            Controls.Add( _FullScreenPictureBox );

            LayoutPictureBox.Image = _LayoutImage.BaseImage();


        }

        void InitPanhead()
        {
            _PanheadAccessThread = new PanheadAccessThread(
                Default.PanheadAddress,
                Default.PanheadPort,
                Default.CH555Address );
            _PanheadAccessThread.Position += this._PanheadAccessThread_Position;
            _PanheadAccessThread.PanheadError += _PanheadAccessThread_PanheadError;
            _PanheadAccessThread.Start();
        }

        private void _PanheadAccessThread_PanheadError( object sender, EventArgs e )
        {
            AutoPatrolMode( false );
            MessageBox.Show( $"カメラ雲台との通信異常が発生しました。{Environment.NewLine}雲台の電源を再投入してください。", "巡回" );
            _PanheadAccessThread?.ClearErrorCount();
        }

        void TerminatePanhead()
        {
            if ( _PanheadAccessThread != null )
            {
                if ( _PanheadAccessThread.IsAlive == true )
                {
                    _PanheadAccessThread.Stop();
                }
                else
                {
                    _PanheadAccessThread.Wait();
                }
            }
        }

        private void _PanheadAccessThread_Position( object sender, PanheadPositionEventArgs e )
        {
            _PanAngle = e.Pan;

            _AutoPatrolThread?.SetPosition( e.Pan, e.Tilt );
            PanTiltPositionLabel.Text = $"Pan={e.Pan:F2}°   Tilt={e.Tilt:F2}°";

            LayoutPictureBox.Image?.Dispose();
            if ( AutoPatrolCheckBox.Checked == true )
            {
                if ( ( _CurrentAreaNumber.HasValue == true ) && ( _CurrentAreaNumber.Value >= 0 ) )
                {
                    if ( _PresetTable.AreaRect( _CurrentAreaNumber.Value, out int AreaLeft, out int AreaTop, out int AreaWidth, out int AreaHeight ) == true )
                    {
                        LayoutPictureBox.Image = _LayoutImage.AreaImage( _PanAngle, AreaLeft, AreaTop, AreaWidth, AreaHeight );
                        return;
                    }
                }
            }
            LayoutPictureBox.Image = _LayoutImage.RotateImage( _PanAngle );
        }

        void InitStreaming()
        {
            _StreamDecodeThread = new StreamDecodeThread(
                $"rtsp://{Default.CameraAddress}:{Default.StreamPort}/{Default.StreamURL}",
                Default.CameraAccount,
                Default.CameraPassword );
            _StreamDecodeThread.ImageDecoded += _StreamDecodeThread_ImageDecoded;
            _StreamDecodeThread.Start();
        }

        void TerminateStreaming()
        {
            if ( _StreamDecodeThread != null )
            {
                if ( _StreamDecodeThread.IsAlive == true )
                {
                    _StreamDecodeThread.Stop();
                }
                else
                {
                    _StreamDecodeThread.Wait();
                }
            }
        }

        void InitCameraAccess()
        {
            _CameraAccessThread = new CameraAccessThread(
                Default.CameraAddress,
                Default.CameraAccount,
                Default.CameraPassword );
            _CameraAccessThread.TemperatureInfo += this._CameraAccessThread_TemperatureInfo;
            _CameraAccessThread.TemperatureImage += this._CameraAccessThread_TemperatureImage;
            _CameraAccessThread.Start();
        }

        private void _CameraAccessThread_TemperatureImage( object sender, TemperatureImageEventArgs e )
        {
            if ( e.Alarm == true )
            {
                _PresetTable.AddAlarm( e.AreaNumber, e.Timestamp, e.AreaName, e.Temperature, e.Image );
                MessageBox.Show(
                    $"温度異常警報画像を保存しました。{Environment.NewLine}" +
                    $"履歴参照画面で確認して下さい。",
                    "警報画像保存" );

                if ( _AlarmForm == null )
                {
                    _AlarmForm = new AlarmForm( _PresetTable );
                    _AlarmForm.FormClosed += this._AlarmForm_FormClosed;
                    AddOwnedForm( _AlarmForm );
                    _AlarmForm.Show();
                }
                else
                {
                    _AlarmForm.Select();
                }
                CameraSetupRadioButton.Enabled = false;
                PresetRadioButton.Enabled = false;
                AreaSetupRadioButton.Enabled = false;
                AreaRectSetupRadioButton.Enabled = false;
                HistoryViewRadioButton.Enabled = false;
            }
            else
            {
                if ( e.Image != null )
                {
                    using ( var SnapshotFm = new SnapshotForm( e.Image, _PresetTable ) )
                    {
                        SnapshotFm.ShowDialog();
                    }
                }
                else
                {
                    MessageBox.Show( "画像取得に失敗しました。", "スナップショット" );
                }
            }

            e.Image?.Dispose();
        }

        private void _CameraAccessThread_TemperatureInfo( object sender, TemperatureInfoEventArgs e )
        {
            string PosX = $"{e.X,3}";
            string PosY = $"{e.Y,3}";
            string Temp = $"{e.Temperature,2:F1} ℃";
            if ( e.X < 0 )
            {
                PosX = "---";
            }
            if ( e.Y < 0 )
            {
                PosY = "---";
            }
            if ( e.Temperature.Equals( float.NaN ) == true )
            {
                Temp = "--- ℃";
            }
            MaxPosLabel.Text = $"X座標：{PosX}   Y座標：{PosY}";
            MaxTempLabel.Text = $"{Temp}";

            AlarmCheck( e.X, e.Y, e.Temperature );
        }

        void TerminateCamera()
        {
            if ( _CameraAccessThread != null )
            {
                if ( _CameraAccessThread.IsAlive == true )
                {
                    _CameraAccessThread.Stop();
                }
                else
                {
                    _CameraAccessThread.Wait();
                }
            }
        }

        void InitIOBox()
        {
            _IOSetter = new IOSetter( Default.AlarmAddress, Default.AlarmPort, Default.AlarmLightChannel, Default.AlarmBuzzerChannel );
            _IOSetter.AlarmClearButon += this._IOSetter_AlarmClearButon;
            _IOSetter.Start();
        }

        private void _IOSetter_AlarmClearButon( object sender, EventArgs e )
        {
            AlarmClear();
        }

        void TerminateIOBox()
        {
            if ( _IOSetter != null )
            {
                if ( _IOSetter.IsAlive == true )
                {
                    _IOSetter.Stop();
                }
                else
                {
                    _IOSetter.Wait();
                }
            }
        }

        void _StreamDecodeThread_ImageDecoded( object sender, ImageDecodedEventArgs e )
        {
            if ( e.Image != null )
            {
                if ( LiveImagePictureBox.Visible == true )
                {
                    LiveImagePictureBox.Image?.Dispose();
                    LiveImagePictureBox.Image = e.Image;
                }
                if ( _FullScreenPictureBox.Visible == true )
                {
                    _FullScreenPictureBox.Image?.Dispose();
                    _FullScreenPictureBox.Image = e.Image;
                }
            }
        }

        void AutoPatrolMode( bool PatrolEnable )
        {
            if ( PatrolEnable == true )
            {
                AutoPatrolCheckBox.BackColor = Color.LightCyan;
                AutoPatrolCheckBox.Text = "自動巡回を一時停止する";
                SetRadioButtonEnable( false );
                MainTitleLabel.BackColor = Color.Black;
                MainTitleLabel.ForeColor = Color.Yellow;
                PTZButtonPanel.Enabled = false;
                //PTZCntlPanel.Enabled = false;

                HomePositionSetButton.Enabled = false;
                HomePositionMoveButton.Enabled = false;
                AutoFocusButton.Enabled = false;
                FocusNearButton.Enabled = false;
                FocusFarButton.Enabled = false;
                SnapshotButton.Enabled = false;
                SpeedRadioButton1.Enabled = false;
                SpeedRadioButton2.Enabled = false;
                SpeedRadioButton3.Enabled = false;
                SpeedRadioButton4.Enabled = false;
                SpeedRadioButton5.Enabled = false;

                if ( _AutoPatrolThread?.IsPatrolPause == true )
                {
                    var MsgResult = MessageBox.Show(
                        $"自動巡回を再開します。{Environment.NewLine}" +
                        $"前回中断したプリセット位置から再開してよろしいですか？",
                        "巡回再開",
                        MessageBoxButtons.YesNoCancel,
                        MessageBoxIcon.Question );
                    if ( MsgResult == DialogResult.Yes )
                    {
                        _AutoPatrolThread?.ResumePatrol();
                    }
                    else if ( MsgResult == DialogResult.No )
                    {
                        _AutoPatrolThread?.StartPatrol();
                    }
                    else
                    {
                        AutoPatrolCheckBox.Checked = false;
                        return;
                    }
                }
                else
                {
                    _AutoPatrolThread?.StartPatrol();
                }
                _CameraAccessThread?.StartPatrol();
            }
            else
            {
                if ( _Alarm == true )
                {
                    AutoPatrolCheckBox.BackColor = Color.Yellow;
                    AutoPatrolCheckBox.Text = "温度異常発報中";
                    //_CameraAccessThread?.StopAutoFocus();
                }
                else
                {
                    AutoPatrolCheckBox.BackColor = Color.LightGray;
                    if ( ( _AutoPatrolThread?.IsPatrolRunning == true ) || ( _AutoPatrolThread?.IsPatrolPause == true ) )
                    {
                        MainTitleLabel.Text = "巡回一時停止中";
                        AutoPatrolCheckBox.Text = "自動巡回を再開する";
                    }
                    else
                    {
                        MainTitleLabel.Text = "巡回停止中";
                        AutoPatrolCheckBox.Text = "自動巡回を開始する";
                    }
                    _AutoPatrolThread?.SuspendPatrol();
                    LayoutPictureBox.Image?.Dispose();
                    LayoutPictureBox.Image = _LayoutImage.BaseImage();
                    _CameraAccessThread?.StopPatrol();
                    _PatrolArea = -1;
                }
                PTZButtonPanel.Enabled = true;
                //PTZCntlPanel.Enabled = true;
                HomePositionSetButton.Enabled = true;
                HomePositionMoveButton.Enabled = true;
                AutoFocusButton.Enabled = true;
                FocusNearButton.Enabled = true;
                FocusFarButton.Enabled = true;
                SnapshotButton.Enabled = true;
                SpeedRadioButton1.Enabled = true;
                SpeedRadioButton2.Enabled = true;
                SpeedRadioButton3.Enabled = true;
                SpeedRadioButton4.Enabled = true;
                SpeedRadioButton5.Enabled = true;
                SetRadioButtonEnable( true );
            }
        }

        void SetRadioButtonEnable( bool Enable )
        {
            if ( Enable == true )
            {
                AreaSetupRadioButton.Enabled = true;
                PresetRadioButton.Enabled = true;
                CameraSetupRadioButton.Enabled = true;
                AreaRectSetupRadioButton.Enabled = true;
                HistoryViewRadioButton.Enabled = true;
            }
            else
            {
                AreaSetupRadioButton.Enabled = false;
                PresetRadioButton.Enabled = false;
                CameraSetupRadioButton.Enabled = false;
                HistoryViewRadioButton.Enabled = false;
                AreaRectSetupRadioButton.Enabled = false;

                AreaSetupRadioButton.Checked = false;
                PresetRadioButton.Checked = false;
                CameraSetupRadioButton.Checked = false;
                HistoryViewRadioButton.Checked = false;
                AreaRectSetupRadioButton.Checked = false;
            }
        }


        private void AlarmCheck( int PosX, int PosY, float Temperature )
        {
            if ( Temperature.Equals( float.NaN ) == true )
            {
                _AlarmCount = 0;
                return;
            }

            if ( AutoPatrolCheckBox.Checked == false )
            {
                _AlarmCount = 0;
                return;
            }

            //MaxPosLabel.Text = $"位置：X={PosX} Y={PosY}";
            //MaxTempLabel.Text = $"温度：{Temperature:F1} ℃";
            _AutoPatrolThread.SetTemperaturePosition( PosX, PosY, Temperature );

            float Threshold = Default.TemperatureThreshold;
            if ( Temperature >= Threshold )
            {
                _AlarmCount++;
                if ( _AlarmCount >= Default.AlarmCount )
                {
                    if ( _Alarm == false )
                    {
                        _IOSetter.AlarmOn( Default.AlarmBuzzerEnable );

                        _AutoPatrolThread.AlarmPatrol();

                        SetRadioButtonEnable( false );

                        MainTitleLabel.BackColor = Color.Yellow;
                        MainTitleLabel.ForeColor = Color.Black;
                        MainTitleLabel.Text = "温度異常発生！！";

                        AutoPatrolCheckBox.BackColor = Color.Yellow;
                        AutoPatrolCheckBox.Text = "温度異常発報中";

                        AutoPatrolCheckBox.Enabled = false;

                        _Alarm = true;


                        PTZButtonPanel.Enabled = false;
                        //PTZCntlPanel.Enabled = false;
                        HomePositionSetButton.Enabled = false;
                        HomePositionMoveButton.Enabled = false;
                        AutoFocusButton.Enabled = false;
                        FocusNearButton.Enabled = false;
                        FocusFarButton.Enabled = false;
                        SnapshotButton.Enabled = false;
                        SpeedRadioButton1.Enabled = false;
                        SpeedRadioButton2.Enabled = false;
                        SpeedRadioButton3.Enabled = false;
                        SpeedRadioButton4.Enabled = false;
                        SpeedRadioButton5.Enabled = false;
                    }
                }
            }
            else
            {
                _AlarmCount = 0;
            }
        }

        private void AlarmClear()
        {
            _IOSetter.AlarmOff();
            SetRadioButtonEnable( true );
            AutoPatrolCheckBox.Enabled = true;
            AutoPatrolCheckBox.Checked = false;
            AutoPatrolCheckBox.BackColor = Color.LightGray;
            AutoPatrolCheckBox.Text = "自動巡回を再開する";
            _Alarm = false;
            MainTitleLabel.Text = string.Empty;
            MainTitleLabel.BackColor = Color.Black;
            PTZButtonPanel.Enabled = true;
            //PTZCntlPanel.Enabled = true;
            HomePositionSetButton.Enabled = true;
            HomePositionMoveButton.Enabled = true;
            AutoFocusButton.Enabled = true;
            FocusNearButton.Enabled = true;
            FocusFarButton.Enabled = true;
            SnapshotButton.Enabled = true;
            SpeedRadioButton1.Enabled = true;
            SpeedRadioButton2.Enabled = true;
            SpeedRadioButton3.Enabled = true;
            SpeedRadioButton4.Enabled = true;
            SpeedRadioButton5.Enabled = true;
        }
    }
}

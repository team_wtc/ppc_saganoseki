﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace CES.ThermalPTZ
{
    using Threads;
    using Data;

    public partial class SequenceSetupForm : Form
    {
        readonly PresetTable _PresetTable;
        readonly PanheadAccessThread _PanheadAccessThread;

        public SequenceSetupForm()
        {
            InitializeComponent();
            _PresetTable = null;
            _PanheadAccessThread = null;
        }
        public SequenceSetupForm( PresetTable Table, PanheadAccessThread PanheadThread ) : this()
        {
            _PresetTable = Table;
            _PanheadAccessThread = PanheadThread;
        }

        private void SequenceSetupForm_Load( object sender, EventArgs e )
        {
            if ( _PresetTable != null )
            {
                AreaSelectComboBox.DataSource = _PresetTable.CreateAreaComboItem();
                AreaSelectComboBox.DisplayMember = "Value";
                AreaSelectComboBox.ValueMember = "Key";
            }
        }

        private void AreaSelectComboBox_SelectedIndexChanged( object sender, EventArgs e )
        {
            AutoPatrolListView.Items.Clear();

            if ( AreaSelectComboBox.SelectedItem is KeyValuePair<int, string> AreaInfo )
            {
                AutoPatrolListView.Items.AddRange( _PresetTable.CreatePatrolListViewItem( AreaInfo.Key ) );
            }
        }

        private void PatrolAddButton_Click( object sender, EventArgs e )
        {
            if ( ( AreaSelectComboBox.SelectedItem is KeyValuePair<int, string> AreaInfo ) && ( _PresetTable != null ) )
            {
                int PresetNumber = Convert.ToInt32( PatrolPresetNumericUpDown.Value );

                if ( _PresetTable.FindPreset( PresetNumber ) == true )
                {
                    var MsgResult = MessageBox.Show(
                        $"プリセット番号({PresetNumber})は既に登録されています。{Environment.NewLine}" +
                        $"雲台のプリセット情報を上書きしてよろしいですか？",
                        "プリセット登録",
                        MessageBoxButtons.YesNo,
                        MessageBoxIcon.Question );
                    if ( MsgResult != DialogResult.Yes )
                    {
                        return;
                    }
                }
                else
                {
                    var MsgResult = MessageBox.Show(
                        $"現在の画角をプリセット番号({PresetNumber})に登録します。{Environment.NewLine}" +
                        $"よろしいですか？",
                        "プリセット登録",
                        MessageBoxButtons.YesNo,
                        MessageBoxIcon.Question );
                    if ( MsgResult != DialogResult.Yes )
                    {
                        return;
                    }
                }

                try
                {
                    _PanheadAccessThread.GetPosition( out float CurPan, out float CurTilt );
                    _PanheadAccessThread?.SetPreset( Convert.ToByte( PresetNumber ) );
                    _PresetTable.AddPatrolPreset(
                        AreaInfo.Key,
                        Convert.ToInt32( PatrolPresetNumericUpDown.Value ),
                        CurPan,
                        CurTilt,
                        HighSpeedMoveCheckBox.Checked,
                        AutoFocusCheckBox.Checked,
                        Convert.ToInt32( AutoFocusWaitTimeNumericUpDown.Value ) );

                    AutoPatrolListView.Items.Clear();
                    AutoPatrolListView.Items.AddRange( _PresetTable.CreatePatrolListViewItem( AreaInfo.Key ) );
                }
                catch (Exception Exp )
                {
                    MessageBox.Show( Exp.Message );
                }
            }
        }

        private void AutoPatrolListView_SelectedIndexChanged( object sender, EventArgs e )
        {
            if ( ( AutoPatrolListView.SelectedItems.Count > 0 ) &&
                ( AutoPatrolListView.SelectedItems[ 0 ].Tag is ThermalDataSet.SEQUENCERow SeqRow ) )
            {
                SeqNoNumericUpDown.Value = Convert.ToDecimal( SeqRow.seq_no );
                PatrolPresetNumericUpDown.Value = Convert.ToDecimal( SeqRow.seq_preset_no );
                AutoFocusCheckBox.Checked = SeqRow.seq_auto_focus;
                HighSpeedMoveCheckBox.Checked = SeqRow.seq_high_speed_move;
                if ( SeqRow.Isseq_auto_focus_wait_timeNull() == true )
                {
                    AutoFocusWaitTimeNumericUpDown.Value = 0M;
                }
                else
                {
                    AutoFocusWaitTimeNumericUpDown.Value = Convert.ToDecimal( SeqRow.seq_auto_focus_wait_time );
                }
            }
        }

        private void PatrolUpdateButton_Click( object sender, EventArgs e )
        {
            if ( ( AutoPatrolListView.SelectedItems.Count > 0 ) &&
                ( AutoPatrolListView.SelectedItems[ 0 ].Tag is ThermalDataSet.SEQUENCERow SeqRow ) )
            {
                bool AddPreset = false;
                int PresetNumber = Convert.ToInt32( PatrolPresetNumericUpDown.Value );

                if ( _PresetTable.FindPreset( PresetNumber ) == false )
                {
                    var MsgResult = MessageBox.Show(
                        $"現在の画角をプリセット番号({PresetNumber})に登録します。{Environment.NewLine}" +
                        $"よろしいですか？",
                        "プリセット登録",
                        MessageBoxButtons.YesNo,
                        MessageBoxIcon.Question );
                    if ( MsgResult != DialogResult.Yes )
                    {
                        return;
                    }
                    AddPreset = true;
                }

                try
                {
                    _PresetTable.UpdatePatrolInfo(
                        SeqRow.seq_area_id,
                        Convert.ToInt32( SeqNoNumericUpDown.Value ),
                        PresetNumber,
                        HighSpeedMoveCheckBox.Checked,
                        AutoFocusCheckBox.Checked,
                        Convert.ToInt32( AutoFocusWaitTimeNumericUpDown.Value ) );

                    if ( AddPreset == true )
                    {
                        _PanheadAccessThread.GetPosition( out float CurPan, out float CurTilt );
                        _PanheadAccessThread?.SetPreset( Convert.ToByte( PresetNumber ) );

                        _PresetTable.UpdatePatrolPreset(
                            SeqRow.seq_area_id,
                            Convert.ToInt32( SeqNoNumericUpDown.Value ),
                            PresetNumber,
                            CurPan,
                            CurTilt );
                    }

                    AutoPatrolListView.Items.Clear();
                    AutoPatrolListView.Items.AddRange( _PresetTable.CreatePatrolListViewItem( SeqRow.seq_area_id ) );
                }
                catch (Exception Exp )
                {
                    MessageBox.Show( Exp.Message );
                }
            }
        }

        private void SeqUpButton_Click( object sender, EventArgs e )
        {
            if ( ( AutoPatrolListView.SelectedItems.Count > 0 ) &&
                ( AutoPatrolListView.SelectedItems[ 0 ].Tag is ThermalDataSet.SEQUENCERow SeqRow ) )
            {
                _PresetTable.ExchangePatrol( SeqRow.seq_area_id, SeqRow.seq_no, true );

                AutoPatrolListView.Items.Clear();
                AutoPatrolListView.Items.AddRange( _PresetTable.CreatePatrolListViewItem( SeqRow.seq_area_id ) );
            }
        }

        private void SeqDownButton_Click( object sender, EventArgs e )
        {
            if ( ( AutoPatrolListView.SelectedItems.Count > 0 ) &&
                ( AutoPatrolListView.SelectedItems[ 0 ].Tag is ThermalDataSet.SEQUENCERow SeqRow ) )
            {
                _PresetTable.ExchangePatrol( SeqRow.seq_area_id, SeqRow.seq_no, false );

                AutoPatrolListView.Items.Clear();
                AutoPatrolListView.Items.AddRange( _PresetTable.CreatePatrolListViewItem( SeqRow.seq_area_id ) );
            }
        }

        private void TestMoveButton_Click( object sender, EventArgs e )
        {
            if ( ( AutoPatrolListView.SelectedItems.Count > 0 ) &&
                ( AutoPatrolListView.SelectedItems[ 0 ].Tag is ThermalDataSet.SEQUENCERow SeqRow ) )
            {
                _PanheadAccessThread?.SetPresetSpeed( Convert.ToByte( TestSpeedNumericUpDown.Value ) );
                _PanheadAccessThread?.GotoPreset( Convert.ToByte( SeqRow.seq_preset_no ) );
                //  2020/10/14 現地修正　GotoPreset3回出し
                Thread.Sleep( 500 );
                _PanheadAccessThread?.GotoPreset( Convert.ToByte( SeqRow.seq_preset_no ) );
                Thread.Sleep( 500 );
                _PanheadAccessThread?.GotoPreset( Convert.ToByte( SeqRow.seq_preset_no ) );
            }
        }

        private void TestStopButton_Click( object sender, EventArgs e )
        {
            _PanheadAccessThread?.MoveStop();
        }

        private void PresetUpdateButton_Click( object sender, EventArgs e )
        {
            if ( ( AutoPatrolListView.SelectedItems.Count > 0 ) &&
                ( AutoPatrolListView.SelectedItems[ 0 ].Tag is ThermalDataSet.SEQUENCERow SeqRow ) )
            {
                int PresetNumber = Convert.ToInt32( PatrolPresetNumericUpDown.Value );

                if ( _PresetTable.FindPreset( PresetNumber ) == false )
                {
                    return;
                }

                try
                {
                    _PanheadAccessThread.GetPosition( out float CurPan, out float CurTilt );
                    _PanheadAccessThread?.SetPreset( Convert.ToByte( PresetNumber ) );

                    _PresetTable.UpdatePatrolPreset(
                        SeqRow.seq_area_id,
                        Convert.ToInt32( SeqNoNumericUpDown.Value ),
                        PresetNumber,
                        CurPan,
                        CurTilt );

                    AutoPatrolListView.Items.Clear();
                    AutoPatrolListView.Items.AddRange( _PresetTable.CreatePatrolListViewItem( SeqRow.seq_area_id ) );
                }
                catch ( Exception Exp )
                {
                    MessageBox.Show( Exp.Message );
                }
            }
        }

        private void PatrolDeleteButton_Click( object sender, EventArgs e )
        {
            if ( ( AutoPatrolListView.SelectedItems.Count > 0 ) &&
                ( AutoPatrolListView.SelectedItems[ 0 ].Tag is ThermalDataSet.SEQUENCERow SeqRow ) )
            {
                Int32 AreaId = SeqRow.seq_area_id;
                try
                {
                    _PresetTable.DeletePatrol(
                        SeqRow.seq_area_id,
                        Convert.ToInt32( SeqNoNumericUpDown.Value ) );

                    AutoPatrolListView.Items.Clear();
                    AutoPatrolListView.Items.AddRange( _PresetTable.CreatePatrolListViewItem( AreaId ) );
                }
                catch ( Exception Exp )
                {
                    MessageBox.Show( Exp.Message );
                }
            }
        }
    }
}

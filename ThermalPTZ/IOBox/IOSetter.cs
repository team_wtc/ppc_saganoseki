﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Diagnostics;
using System.Windows.Forms;

namespace CES.ThermalPTZ.IOBox
{
    using Threads;
    using static System.Diagnostics.Trace;

    public class IOSetter : IntervalThread
    {
        public event EventHandler AlarmClearButon;

        const byte PREFIX_BYTE = 0xF0;
        const byte STATUS_BYTE = 0x55;
        const byte DO_CHANNEL_OFF = 0x00;
        const byte DO_CHANNEL_01 = 0x01;
        const byte DO_CHANNEL_02 = 0x02;
        const byte DO_CHANNEL_03 = 0x04;
        const byte DO_CHANNEL_MIN = 1;
        const byte DO_CHANNEL_MAX = 3;

        const ushort DI_CHANNEL_01 = 0x8000;
        const ushort DI_CHANNEL_02 = 0x0001;
        const ushort DI_CHANNEL_03 = 0x0002;
        const ushort DI_CHANNEL_MASK = ( DI_CHANNEL_01 | DI_CHANNEL_02 | DI_CHANNEL_03 );

        readonly string _IOBoxAddress;
        readonly int _IOBoxPort;
        readonly byte _LightChannel;
        readonly byte _BuzzerChannel;
        readonly ushort _ClearChannel;

        TcpClient _IOBoxClient = null;
        NetworkStream _NetworkStream = null;
        byte? _Command = null;
        ushort? _ChannelState = null;

        public IOSetter( string IOBoxAddress, int IOBoxPort, byte LightChannel = 1, byte BuzzerChannel = 2, int ClearChannel = 1 )
            : base( "IO BOX" )
        {
            Assert( ( ( LightChannel >= DO_CHANNEL_MIN ) && ( LightChannel <= DO_CHANNEL_MAX ) ), $"Light Channel Value Error:{LightChannel}" );
            Assert( ( ( BuzzerChannel >= DO_CHANNEL_MIN ) && ( BuzzerChannel <= DO_CHANNEL_MAX ) ), $"Buzzer Channel Value Error:{BuzzerChannel}" );
            Assert( ( LightChannel != BuzzerChannel ), $"Can't Set Same value Light and Buzzer(Light={LightChannel},Buzzer= {BuzzerChannel})" );

            _IOBoxAddress = IOBoxAddress;
            _IOBoxPort = IOBoxPort;

            switch ( LightChannel )
            {
                case 1:
                    _LightChannel = DO_CHANNEL_01;
                    break;
                case 2:
                    _LightChannel = DO_CHANNEL_02;
                    break;
                case 3:
                    _LightChannel = DO_CHANNEL_03;
                    break;
            }

            switch ( BuzzerChannel )
            {
                case 1:
                    _BuzzerChannel = DO_CHANNEL_01;
                    break;
                case 2:
                    _BuzzerChannel = DO_CHANNEL_02;
                    break;
                case 3:
                    _BuzzerChannel = DO_CHANNEL_03;
                    break;
            }

            switch ( ClearChannel )
            {
                case 1:
                    _ClearChannel = DI_CHANNEL_01;
                    break;
                case 2:
                    _ClearChannel = DI_CHANNEL_02;
                    break;
                case 3:
                    _ClearChannel = DI_CHANNEL_03;
                    break;
            }
        }

        public InputCallback Callback => CallbackFunction;

        private void CallbackFunction( bool On, int[] Channel )
        {
            if ( On == true )
            {
                AlarmOff();
            }
        }

        public void AlarmOn( bool WithBuzzer )
        {
            byte Channel = _LightChannel;
            if ( WithBuzzer == true )
            {
                Channel |= _BuzzerChannel;
            }
            _Command = Channel;
            WriteLine( $"IOBox AlarmOn WithBuzzer={WithBuzzer} _Command={_Command.Value:X2}" );
        }

        public void AlarmOff()
        {
            _Command = DO_CHANNEL_OFF;
        }

        protected override bool TimerRoutine()
        {
            try
            {
                if ( Open() == false )
                {
                    return true;
                }

                byte[] Buffer = new byte[ 2 ];
                byte[] RecvBuffer = new byte[ 1024 ];

                if ( _Command.HasValue == true )
                {
                    Buffer[ 0 ] = PREFIX_BYTE;
                    Buffer[ 1 ] = _Command.Value;
                    _Command = null;
                    _NetworkStream?.Write( Buffer, 0, Buffer.Length );
                    WriteLine( $"IOBox Send {Buffer[ 0 ]:X2} {Buffer[ 1 ]:X2}" );

                    while ( _IOBoxClient.Available < 2 )
                    {
                        Thread.Sleep( 10 );
                    }

                    if ( _NetworkStream != null )
                    {
                        int len = _NetworkStream.Read( RecvBuffer, 0, RecvBuffer.Length );
                        string Dump = string.Empty;
                        for ( int i = 0 ; i < len ; i++ )
                        {
                            Dump += $"[{RecvBuffer[ i]:X2}]";
                        }
                        WriteLine( $"IOBox Recv Len = {len} {Dump}" );
                    }
                }
                else
                {
                    Buffer[ 0 ] = STATUS_BYTE;
                    Buffer[ 1 ] = STATUS_BYTE;
                    _NetworkStream?.Write( Buffer, 0, Buffer.Length );
                    while ( _IOBoxClient.Available < 2 )
                    {
                        Thread.Sleep( 10 );
                    }
                    if ( _NetworkStream?.Read( RecvBuffer, 0, RecvBuffer.Length ) >= 2 )
                    {
                        ushort Status = ( ushort )( ( RecvBuffer[ 0 ] << 8 ) | RecvBuffer[ 1 ] );
                        ushort ChState = ( ushort )( Status & DI_CHANNEL_MASK );
                        if ( ( _ChannelState.HasValue == false ) || ( _ChannelState.Value != ChState ) )
                        {
                            _ChannelState = ChState;
                            bool ch1 = ( ( _ChannelState & DI_CHANNEL_01 ) != 0 );
                            bool ch2 = ( ( _ChannelState & DI_CHANNEL_02 ) != 0 );
                            bool ch3 = ( ( _ChannelState & DI_CHANNEL_03 ) != 0 );
                            WriteLine(
                                $"LA-3R3P-P Input Channel Status " +
                                $"CH1={( ( ch1 == true ) ? "ON" : "OFF" )} " +
                                $"CH2={( ( ch2 == true ) ? "ON" : "OFF" )} " +
                                $"CH3={( ( ch3 == true ) ? "ON" : "OFF" )} " );
                            if ( ( _ChannelState & _ClearChannel ) == _ClearChannel )
                            {
                                _Command = DO_CHANNEL_OFF;
                                if ( AlarmClearButon != null )
                                {
                                    if ( ( AlarmClearButon.Target != null ) && ( AlarmClearButon.Target is Control Cntl ) )
                                    {
                                        Cntl.BeginInvoke( AlarmClearButon, this, EventArgs.Empty );
                                    }
                                    else
                                    {
                                        AlarmClearButon( this, EventArgs.Empty );
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch ( ThreadAbortException )
            {
                throw;
            }
            catch(Exception e)
            {
                WriteLine( $"IOBox Send Error. {e.Message}" );
                Close();
            }

            return true;
        }

        private void Close()
        {
            try
            {
                _IOBoxClient?.Client?.Shutdown( SocketShutdown.Both );
                _IOBoxClient?.Close();
                _IOBoxClient?.Dispose();

                _NetworkStream?.Close();
            }
            catch
            {
            }
            finally
            {
                _IOBoxClient = null;
                _NetworkStream = null;
            }
        }

        private bool Open()
        {
            if ( ( _IOBoxClient != null ) && ( _IOBoxClient.Connected == true ) )
            {
                return true;
            }

            try
            {
                _IOBoxClient = new TcpClient();
                _IOBoxClient.Connect( _IOBoxAddress, _IOBoxPort );
                _IOBoxClient.ReceiveTimeout = 3000;
                _NetworkStream = _IOBoxClient.GetStream();
                return true;
            }
            catch ( ThreadAbortException )
            {
                throw;
            }
            catch(Exception e)
            {
                Trace.WriteLine( $"IOBox Error {e.Message}" );
                Close();
                return false;
            }
            finally
            {
            }
        }
    }
}

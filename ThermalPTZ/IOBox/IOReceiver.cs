﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Diagnostics;

namespace CES.ThermalPTZ.IOBox
{
    using Threads;

    public class IOReceiver : OneShotThread
    {
        readonly TcpClient _TcpClient;
        readonly InputCallback _InputCallback;


        public IOReceiver( TcpClient Client, InputCallback Callback ) : base( "IOReceiver" )
        {
            _TcpClient = Client;
            _InputCallback = Callback;
        }

        protected override bool Init()
        {
            return base.Init();
        }

        protected override void Terminate()
        {
            try
            {
                _TcpClient?.Client?.Shutdown( SocketShutdown.Both );
                _TcpClient?.Close();
                _TcpClient?.Dispose();
            }
            catch
            {
            }
            base.Terminate();
        }

        protected override void OneShotRoutine()
        {
            const byte RES_START_BYTE = 0xFA;
            const byte RES_SECOND_BYTE = 0xF0;

            while ( true )
            {
                if ( _TcpClient.Client.Poll( 100 * 1000, SelectMode.SelectRead ) == false )
                {
                    continue;
                }

                if ( _TcpClient.Available <= 0 )
                {
                    //  切断?
                    return;
                }

                if ( _TcpClient.Available < 3 )
                {
                    continue;
                }

                byte[] RecvBuffer = new byte[ 3 ];

                if ( _TcpClient.GetStream().Read( RecvBuffer, 0, RecvBuffer.Length ) <= 0 )
                {
                    //  切断?
                    return;
                }
                Trace.WriteLine( $"IOBox Receiver RECV {RecvBuffer[ 0 ]:X2} {RecvBuffer[ 1 ]:X2} {RecvBuffer[ 2 ]:X2}" );

                byte[] SendBuffer = new byte[ 2 ];
                SendBuffer[ 0 ] = RecvBuffer[ 1 ];
                SendBuffer[ 1 ] = RecvBuffer[ 2 ];
                _TcpClient.GetStream().Write( SendBuffer, 0, SendBuffer.Length );
                Trace.WriteLine( $"IOBox Receiver SEND {SendBuffer[ 0 ]:X2} {SendBuffer[ 1 ]:X2}" );

                if ( ( RecvBuffer[ 0 ] == RES_START_BYTE ) &&
                    ( RecvBuffer[ 1 ] == RES_SECOND_BYTE ) )
                {
                    if ( RecvBuffer[2] == 0x00 )
                    {
                        _InputCallback?.Invoke( false, null );
                    }
                    else
                    {
                        List<int> Channels = new List<int>();
                        if ( ( RecvBuffer[ 2 ] & 0x01 ) == 0x01 )
                        {
                            Channels.Add( 1 );
                        }
                        if ( ( RecvBuffer[ 2 ] & 0x02 ) == 0x02 )
                        {
                            Channels.Add( 2 );
                        }
                        if ( ( RecvBuffer[ 2 ] & 0x04 ) == 0x04 )
                        {
                            Channels.Add( 3 );
                        }
                        _InputCallback?.Invoke( true, Channels.ToArray() );
                    }
                }
            }
        }
    }
}

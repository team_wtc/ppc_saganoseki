﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using System.Net.Sockets;

namespace CES.ThermalPTZ.IOBox
{
    using Threads;

    public delegate void InputCallback( bool On, int[] Channel );

    public class IOListener : OneShotThread
    {
        readonly TcpListener _TcpListener;
        readonly int _BindPort;
        readonly InputCallback _InputCallback;
        readonly List<IOReceiver> _Receivers;

        public IOListener(int Port, InputCallback Callback ) : base( "IOListener" )
        {
            _BindPort = Port;
            _TcpListener = new TcpListener( IPAddress.Any, _BindPort );
            _InputCallback = Callback;
            _Receivers = new List<IOReceiver>();
        }

        protected override bool Init()
        {
            try
            {
                _TcpListener?.Start();
                return true;
            }
            catch
            {
                _TcpListener?.Stop();
                return false;
            }
        }

        protected override void Terminate()
        {
            try
            {
                _TcpListener?.Stop();
            }
            catch
            {
            }

            foreach ( var Recver in _Receivers )
            {
                if ( Recver.IsAlive == true )
                {
                    Recver.Stop();
                }
                else
                {
                    Recver.Wait();
                }
            }
        }

        protected override void OneShotRoutine()
        {
            while ( true )
            {
                try
                {
                    if ( _TcpListener.Pending() == false )
                    {
                        Thread.Sleep( 100 );
                        continue;
                    }

                    var Client = _TcpListener.AcceptTcpClient();
                    IOReceiver Receiver = new IOReceiver( Client, _InputCallback );
                    Receiver.Start();
                }
                catch
                {

                }
            }
        }
    }
}

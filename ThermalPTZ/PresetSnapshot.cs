﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CES.ThermalPTZ
{
    public partial class PresetSnapshot : UserControl
    {
        int _PresetNumber = 0;
        float _MaxTemperature = float.NaN;
        bool _Current = false;

        public PresetSnapshot()
        {
            InitializeComponent();
        }

        public int PresetNumber
        {
            get
            {
                return _PresetNumber;
            }
            set
            {
                if ( _PresetNumber != value )
                {
                    _PresetNumber = value;
                    if ( _PresetNumber > 0 )
                    {
                        PresetNumberLabel.Text = $"Preset : {_PresetNumber}";
                    }
                    else
                    {
                        PresetNumberLabel.Text = string.Empty;
                    }
                }
            }
        }

        public float MaxTemperature
        {
            get
            {
                return _MaxTemperature;
            }
            set
            {
                if ( _MaxTemperature != value )
                {
                    _MaxTemperature = value;
                    if ( _MaxTemperature.Equals( float.NaN ) == false )
                    {
                        MaxTempLabel.Text = $"MAX : {_MaxTemperature:F1}°";
                    }
                    else
                    {
                        MaxTempLabel.Text = string.Empty;
                    }
                }

                if ( _MaxTemperature >= 85F )
                {
                    BackColor = Color.Red;
                    MaxTempLabel.BackColor = Color.Red;
                }
                else if ( _MaxTemperature >= 60F )
                {
                    BackColor = Color.Gold;
                    MaxTempLabel.BackColor = Color.Gold;
                }
                else
                {
                    BackColor = Color.Black;
                    MaxTempLabel.BackColor = Color.WhiteSmoke;
                }
            }
        }

        public bool Current
        {
            get => _Current;
            set
            {
                if ( _Current != value )
                {
                    _Current = value;

                    if ( _Current == true )
                    {
                        BackColor = Color.Cyan;
                    }
                    else
                    {
                        if ( _MaxTemperature >= 85F )
                        {
                            BackColor = Color.Red;
                            MaxTempLabel.BackColor = Color.Red;
                        }
                        else if ( _MaxTemperature >= 60F )
                        {
                            BackColor = Color.Gold;
                            MaxTempLabel.BackColor = Color.Gold;
                        }
                        else
                        {
                            BackColor = Color.Black;
                            MaxTempLabel.BackColor = Color.WhiteSmoke;
                        }
                    }
                }
            }
        }
    }
}

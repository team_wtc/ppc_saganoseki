﻿using System;
using System.Collections.Generic;
#if DEBUG
using System.Diagnostics;
#endif
namespace CES.ThermalPTZ.CH_555
{
    using static System.Diagnostics.Trace;

    public static class PanHeadCommand
    {
        const byte SYNC_BYTE = 0xFF;
        const byte ZERO_BYTE = 0x00;
        const byte SET_PRESET = 0x03;
        const byte CLEAR_PRESET = 0x05;
        const byte GOTO_PRESET = 0x07;
        const byte SET_SPEED = 0x6C;
        //const byte PAN_LEFT = 0x04;
        //const byte PAN_RIGHT = 0x02;
        //const byte PAN_STOP = 0x00;
        //const byte TILT_UP = 0x08;
        //const byte TILT_DOWN = 0x10;
        //const byte TILT_STOP = 0x00;
        const byte PAN_REQUEST = 0x51;
        const byte TILT_REQUEST = 0x53;
        const byte PAN_RESPONSE = 0x59;
        const byte TILT_RESPONSE = 0x5B;
        const byte PAN_TILT_STOP = 0x00;

        public enum POSITION_TYPE
        {
            UNKNOWN,
            PAN,
            TILT,
            ZOOM
        }

        [Flags]
        public enum PAN_TILT_MOVE_MODE : byte
        {
            PAN_STOP = 0x00,
            PAN_LEFT = 0x04,
            PAN_RIGHT = 0x02,
            TILT_STOP = 0x00,
            TILT_UP = 0x08,
            TILT_DOWN = 0x10
        }

        public static byte[] SetPresetCommand( byte PanheadId, byte PresetNomber )
        {
            List<byte> Payload = new List<byte>
            {
                PanheadId,   // Address
                ZERO_BYTE,
                SET_PRESET,
                ZERO_BYTE,
                PresetNomber
            };
            Payload.Add( Checksum( Payload.ToArray() ) );
            Payload.Insert(0, SYNC_BYTE );   // sync
            return Payload.ToArray();
        }

        public static byte[] ClearPresetCommand( byte PanheadId, byte PresetNomber )
        {
            List<byte> Payload = new List<byte>
            {
                PanheadId,   // Address
                ZERO_BYTE,
                CLEAR_PRESET,
                ZERO_BYTE,
                PresetNomber
            };
            Payload.Add( Checksum( Payload.ToArray() ) );
            Payload.Insert( 0, SYNC_BYTE );   // sync
            return Payload.ToArray();
        }

        public static byte[] GotoPresetCommand( byte PanheadId, byte PresetNomber )
        {
            List<byte> Payload = new List<byte>
            {
                PanheadId,   // Address
                ZERO_BYTE,
                GOTO_PRESET,
                ZERO_BYTE,
                PresetNomber
            };
            Payload.Add( Checksum( Payload.ToArray() ) );
            Payload.Insert(0, SYNC_BYTE );   // sync
            return Payload.ToArray();
        }

        public static byte[] SetPresetSpeedCommand( byte PanheadId, byte Speed )
        {
            List<byte> Payload = new List<byte>
            {
                PanheadId,   // Address
                Speed,
                SET_PRESET,
                ZERO_BYTE,
                SET_SPEED
            };
            Payload.Add( Checksum( Payload.ToArray() ) );
            Payload.Insert(0, SYNC_BYTE );   // sync
            return Payload.ToArray();
        }

        public static byte[] PanTiltMove( byte PanheadId, PAN_TILT_MOVE_MODE Pan, PAN_TILT_MOVE_MODE Tilt, byte Speed )
        {
            const byte STOP_SPEED = 0;

            byte PanSpeed = ( Pan == PAN_TILT_MOVE_MODE.PAN_STOP ) ? STOP_SPEED : Speed;
            byte TiltSpeed = ( Tilt == PAN_TILT_MOVE_MODE.TILT_STOP ) ? STOP_SPEED : Speed;
            List<byte> Payload = new List<byte>
            {
                PanheadId,   // Address
                ZERO_BYTE,
                (byte)(Pan|Tilt),
                PanSpeed,
                TiltSpeed,
            };
            Payload.Add( Checksum( Payload.ToArray() ) );
            Payload.Insert( 0, SYNC_BYTE );   // sync
            return Payload.ToArray();
        }

        public static byte[] QueryPosition( byte PanheadId, POSITION_TYPE PositionType )
        {
            System.Diagnostics.Trace.Assert( ( ( PositionType == POSITION_TYPE.PAN ) || ( PositionType == POSITION_TYPE.TILT ) ), $"PositionType不正({PositionType})" );

            List<byte> Payload = new List<byte>
            {
                PanheadId,   // Address
                ZERO_BYTE,
                ( PositionType == POSITION_TYPE.PAN ) ? PAN_REQUEST : TILT_REQUEST,
                ZERO_BYTE,
                ZERO_BYTE
            };
            Payload.Add( Checksum( Payload.ToArray() ) );
            Payload.Insert(0, SYNC_BYTE );   // sync
            return Payload.ToArray();
        }


        public static bool GetPosition( byte PanheadAddress, byte[] QueryPositionResponse, out POSITION_TYPE PositionType, out float PositionValue )
        {
            if ( ( QueryPositionResponse == null ) ||
                ( QueryPositionResponse.Length < 7 ) ||
                ( QueryPositionResponse[ 0 ] != SYNC_BYTE ) ||
                ( QueryPositionResponse[ 1 ] != PanheadAddress ) ||
                ( QueryPositionResponse[ 2 ] != ZERO_BYTE ) )
            {
                PositionType = POSITION_TYPE.UNKNOWN;
                PositionValue = float.NaN;
                return false;
            }

            int PositionData = ( ( QueryPositionResponse[ 4 ] << 8 ) | ( QueryPositionResponse[ 5 ] ) );
            if ( ( QueryPositionResponse[ 4 ] == 0xFF ) && ( QueryPositionResponse[ 5 ] == 0xFF ) )
            {
                PositionValue = float.MaxValue;
            }
            else
            {
                //PositionData += 5;
                //PositionData /= 10;
                //PositionValue = ( float )PositionData / 10F;

                PositionValue = ( float )PositionData / 100F;
            }

            switch ( QueryPositionResponse[ 3 ] )
            {
                case PAN_RESPONSE:
                    PositionType = POSITION_TYPE.PAN;
#if DEBUG
                    Trace.WriteLine( $"★Query Pan Position = {PositionValue:F2}" );
#endif
                    break;
                case TILT_RESPONSE:
                    PositionType = POSITION_TYPE.TILT;
                    break;
                default:
                    PositionType = POSITION_TYPE.UNKNOWN;
                    PositionValue = float.NaN;
                    return false;
            }

            return true;
        }

        private static byte Checksum( byte[] Payload )
        {
            byte Sum = 0;

            unchecked
            {
                foreach ( byte Parameter in Payload )
                {
                    Sum += Parameter;
                }
            }

            return Sum;
        }
    }
}

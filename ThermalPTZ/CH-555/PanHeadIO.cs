﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Diagnostics;

namespace CES.ThermalPTZ.CH_555
{
    public class PanHeadIO : IDisposable
    {
        const byte DEFAULT_PANHEAD_ID = 0x01;
        const Int32 RECV_WAIT_MSEC = 1000;
        const Int32 RECV_WAIT_MICRO_SEC = RECV_WAIT_MSEC * 1000;

        readonly IPEndPoint _RemoteEndPoint;
        readonly byte _PanheadId;

        TcpClient _TcpClient;

        byte? _LastSetSpeed = null;

        public PanHeadIO( string Address, int Port, byte PanheadId = DEFAULT_PANHEAD_ID )
        {
            _RemoteEndPoint = new IPEndPoint( IPAddress.Parse( Address ), Port );
            _PanheadId = PanheadId;
        }

        public void SetPreset( byte PresetNomber )
        {
            Send( PanHeadCommand.SetPresetCommand( _PanheadId, PresetNomber ) );
        }

        public void ClearPreset( byte PresetNomber )
        {
            Send( PanHeadCommand.ClearPresetCommand( _PanheadId, PresetNomber ) );
        }

        public void GotoPreset( byte PresetNomber )
        {
            Send( PanHeadCommand.GotoPresetCommand( _PanheadId, PresetNomber ) );
        }

        public void PresetSpeed( byte Speed )
        {
            if ( ( _LastSetSpeed.HasValue == false ) || ( _LastSetSpeed.Value != Speed ) )
            {
                Send( PanHeadCommand.SetPresetSpeedCommand( _PanheadId, Speed ) );
                _LastSetSpeed = Speed;
                Trace.WriteLine( $"SetPresetSpeed 0x{Speed:X2}" );
            }
        }

        public void PanTilt( PanHeadCommand.PAN_TILT_MOVE_MODE Pan, PanHeadCommand.PAN_TILT_MOVE_MODE Tilt, byte PanSpeed )
        {
            Send( PanHeadCommand.PanTiltMove( _PanheadId, Pan, Tilt, PanSpeed ) );
        }

        public float PanPosition()
        {
            byte[] RecvBuff = Query( PanHeadCommand.QueryPosition( _PanheadId, PanHeadCommand.POSITION_TYPE.PAN ) );
            if ( RecvBuff == null )
            {
                return float.NaN;
            }

            if ( PanHeadCommand.GetPosition( _PanheadId, RecvBuff, out PanHeadCommand.POSITION_TYPE Postype, out float Position ) != true )
            {
                return float.NaN;
            }

            if ( Postype != PanHeadCommand.POSITION_TYPE.PAN )
            {
                Trace.WriteLine("★クエリ応答矛盾　PAN要求に対してTILTが応答された。");
                RecvBuff = Recv();
                if ( PanHeadCommand.GetPosition( _PanheadId, RecvBuff, out PanHeadCommand.POSITION_TYPE Postype2, out float Position2 ) != true )
                {
                    return float.NaN;
                }
                if ( Postype2 != PanHeadCommand.POSITION_TYPE.PAN )
                {
                    return float.NaN;
                }
                Trace.WriteLine( "★クエリ応答矛盾解消　PAN応答受信" );
                return Position2;
            }

            return Position;
        }

        public float TiltPosition()
        {
            byte[] RecvBuff = Query( PanHeadCommand.QueryPosition( _PanheadId, PanHeadCommand.POSITION_TYPE.TILT ) );
            if ( RecvBuff == null )
            {
                return float.NaN;
            }

            if ( PanHeadCommand.GetPosition( _PanheadId, RecvBuff, out PanHeadCommand.POSITION_TYPE Postype, out float Position ) != true )
            {
                return float.NaN;
            }

            if ( Postype != PanHeadCommand.POSITION_TYPE.TILT )
            {
                Trace.WriteLine( "★クエリ応答矛盾　TILT要求に対してPANが応答された。" );
                RecvBuff =Recv();
                if ( PanHeadCommand.GetPosition( _PanheadId, RecvBuff, out PanHeadCommand.POSITION_TYPE Postype2, out float Position2 ) != true )
                {
                    return float.NaN;
                }
                if ( Postype2 != PanHeadCommand.POSITION_TYPE.TILT )
                {
                    return float.NaN;
                }
                Trace.WriteLine( "★クエリ応答矛盾解消　TILT応答受信" );
                return Position2;
            }

            return Position;
        }
        private void PacketDump( string Prefix, byte[] DataBytes )
        {
            var Builder = new StringBuilder( Prefix );
            foreach ( var Data in DataBytes )
            {
                Builder.Append( $" {Data:X2}" );
            }
            Trace.WriteLine( Builder.ToString() );
        }

        private void Send( byte[] Payload )
        {
            try
            {
                Open();

                if ( _TcpClient != null )
                {
                    //  2020/09/24  送信が連続しないように送信前にWAITを入れる H.Onuki
                    Thread.Sleep( 50 );
                    _TcpClient.GetStream().Write( Payload, 0, Payload.Length );
                    PacketDump( "CH-555 SEND", Payload );
                    //Thread.Sleep( 100 );
                }
                else
                {
                    Trace.WriteLine( "CH-555 SEND Socket Not Open." );
                }
            }
            catch ( ThreadAbortException )
            {
                Close();
                throw;
            }
            catch(Exception e)
            {
                Trace.WriteLine( "CH-555 SEND Error." );
                Trace.WriteLine( e );
                //Close();
                //  2020/09/22  Socket再オープン処理追加 H.Onuki
                Trace.WriteLine( "CH-555 Socket Reopen." );
                Close();
            }
        }

        private byte[] Query( byte[] SendData )
        {
            try
            {
                Open();

                if ( _TcpClient != null )
                {
                    //  2020/09/24  送信が連続しないように送信前にWAITを入れる H.Onuki
                    Thread.Sleep( 50 );
                    _TcpClient.GetStream().Write( SendData, 0, SendData.Length );

                    if ( _TcpClient.Client.Poll( RECV_WAIT_MICRO_SEC, SelectMode.SelectRead ) == false )
                    {
                        PacketDump( "CH-555 Receive Timeout. Send =", SendData );
                        return null;
                    }
                    if ( _TcpClient.Available <= 0 )
                    {
                        PacketDump( "CH-555 Socket Closed. Send =", SendData );
                        Close();
                        return null;
                    }

                    byte[] Buffer = new byte[ _TcpClient.Available ];
                    _TcpClient.GetStream().Read( Buffer, 0, Buffer.Length );
                    return Buffer;
                }
                else
                {
                    Trace.WriteLine( "CH-555 SEND Socket Not Open." );
                    return null;
                }
            }
            catch ( ThreadAbortException )
            {
                Close();
                throw;
            }
            catch ( Exception e )
            {
                Trace.WriteLine( "CH-555 Query Error." );
                Trace.WriteLine( e );
                //  2020/09/22  Socket再オープン処理追加 H.Onuki
                Trace.WriteLine( "CH-555 Socket Reopen." );
                Close();
                return null;
            }
        }

        private byte[] Recv()
        {
            try
            {
                if ( _TcpClient != null )
                {
                    if ( _TcpClient.Client.Poll( RECV_WAIT_MICRO_SEC, SelectMode.SelectRead ) == false )
                    {
                        Trace.WriteLine( "CH-555 Receive Timeout." );
                        return null;
                    }
                    if ( _TcpClient.Available <= 0 )
                    {
                        Trace.WriteLine( "CH-555 Socket Closed." );
                        Close();
                        return null;
                    }

                    byte[] Buffer = new byte[ _TcpClient.Available ];
                    _TcpClient.GetStream().Read( Buffer, 0, Buffer.Length );
                    return Buffer;
                }
                else
                {
                    Trace.WriteLine( "CH-555 SEND Socket Not Open." );
                    return null;
                }
            }
            catch ( ThreadAbortException )
            {
                Close();
                throw;
            }
            catch ( Exception e )
            {
                Trace.WriteLine( "CH-555 Receive Error." );
                Trace.WriteLine( e );
                //  2020/09/22  Socket再オープン処理追加 H.Onuki
                Trace.WriteLine( "CH-555 Socket Reopen." );
                Close();
                return null;
            }
        }

        void Open()
        {
            if ( _TcpClient != null )
            {
                return;
            }

            try
            {
                _TcpClient = new TcpClient()
                {
                    ReceiveTimeout = RECV_WAIT_MSEC,
                    SendTimeout = RECV_WAIT_MSEC,
                    NoDelay = true
                };
                _TcpClient.Connect( _RemoteEndPoint );
                Trace.WriteLine( $"CH-555 Socket Open. Remmote = {_RemoteEndPoint}" );
            }
            catch ( Exception e )
            {
                Trace.WriteLine( $"CH-555 Socket Connection Error. Remmote = {_RemoteEndPoint}" );
                Trace.WriteLine( e );
                try
                {
                    _TcpClient?.Client?.Shutdown( SocketShutdown.Both );
                    _TcpClient?.Close();
                }
                catch
                {
                }
                _TcpClient = null;
                //throw e;
            }
        }

        void Close()
        {
            if ( _TcpClient != null )
            {
                try
                {
                    _TcpClient?.Client?.Shutdown( SocketShutdown.Both );
                    _TcpClient?.Close();
                    Trace.WriteLine( $"PanHeadIO Socket Close.({_RemoteEndPoint})" );
                }
                catch
                {
                }
                _TcpClient = null;
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // 重複する呼び出しを検出するには

        protected virtual void Dispose( bool disposing )
        {
            if ( !disposedValue )
            {
                if ( disposing )
                {
                    // TODO: マネージド状態を破棄します (マネージド オブジェクト)。
                    Close();
                }

                // TODO: アンマネージド リソース (アンマネージド オブジェクト) を解放し、下のファイナライザーをオーバーライドします。
                // TODO: 大きなフィールドを null に設定します。

                disposedValue = true;
            }
        }

        // TODO: 上の Dispose(bool disposing) にアンマネージド リソースを解放するコードが含まれる場合にのみ、ファイナライザーをオーバーライドします。
        // ~PanHeadIO() {
        //   // このコードを変更しないでください。クリーンアップ コードを上の Dispose(bool disposing) に記述します。
        //   Dispose(false);
        // }

        // このコードは、破棄可能なパターンを正しく実装できるように追加されました。
        public void Dispose()
        {
            // このコードを変更しないでください。クリーンアップ コードを上の Dispose(bool disposing) に記述します。
            Dispose( true );
            // TODO: 上のファイナライザーがオーバーライドされる場合は、次の行のコメントを解除してください。
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}

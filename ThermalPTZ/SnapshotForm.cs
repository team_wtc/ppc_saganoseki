﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CES.ThermalPTZ
{
    public partial class SnapshotForm : Form
    {
        readonly Data.PresetTable _PresetTable;
        readonly Image _SnapshotImage;

        int? AlarmAreaId = null;
        DateTime? AlarmTimestamp = null;

        public SnapshotForm()
        {
            InitializeComponent();
            _PresetTable = null;
            _SnapshotImage = null;
        }

        public SnapshotForm( Image SnapshotImage, Data.PresetTable PresetTable ) : this()
        {
            _PresetTable = PresetTable;
            _SnapshotImage = SnapshotImage;
        }

        private void CloseButton_Click( object sender, EventArgs e )
        {
            Close();
        }

        private void SetAlarmImageButton_Click( object sender, EventArgs e )
        {
            if ( ( _PresetTable == null ) || ( AlarmAreaId.HasValue == false ) || ( AlarmTimestamp.HasValue == false ) )
            {
                return;
            }

            try
            {
                _PresetTable.ChangeAlarmImage( AlarmAreaId.Value, AlarmTimestamp.Value, _SnapshotImage );
                MessageBox.Show( "警報画像を変更しました。", "警報画像変更" );
            }
            catch (Exception Exp )
            {
                MessageBox.Show( Exp.Message, "警報画像変更" );
            }
        }

        private void SnapshotForm_Load( object sender, EventArgs e )
        {
            if ( _PresetTable != null )
            {
                if ( _PresetTable.LatestAlarm( out int Area, out DateTime Timestamp ) == true )
                {
                    AlarmAreaId = Area;
                    AlarmTimestamp = Timestamp;
                }
            }

            if ( ( AlarmAreaId.HasValue == false ) || ( AlarmTimestamp.HasValue == false ) )
            {
                SetAlarmImageButton.Enabled = false;
            }

            SnapshotPictureBox.Image = _SnapshotImage;
        }

        private void ImageSaveButton_Click( object sender, EventArgs e )
        {
            try
            {
                using ( var Dlg = new SaveFileDialog()
                {
                    AddExtension = true,
                    CheckFileExists = false,
                    CheckPathExists = true,
                    CreatePrompt = false,
                    DefaultExt = ".jpg",
                    Filter = "画像ファイル|*.jpg",
                    InitialDirectory = Environment.GetFolderPath( Environment.SpecialFolder.MyPictures ),
                    OverwritePrompt = true,
                    RestoreDirectory = false,
                    ShowHelp = false,
                    Title = "スナップショット画像保存",
                    ValidateNames = true
                } )
                {
                    if ( Dlg.ShowDialog() == DialogResult.OK )
                    {
                        using ( var Img = new Bitmap( _SnapshotImage ) )
                        {
                            Img.Save( Dlg.FileName, ImageFormat.Jpeg );
                        }
                        MessageBox.Show( $"スナップショット画像を「{Dlg.FileName}」に保存しました。", "スナップショット画像保存" );
                    }
                }
            }
            catch (Exception Exp )
            {
                MessageBox.Show( $"スナップショット画像の保存に失敗しました。{Environment.NewLine}{Exp.Message}", "スナップショット画像保存" );
            }
        }

        private void SnapshotForm_FormClosing( object sender, FormClosingEventArgs e )
        {
            SnapshotPictureBox.Image?.Dispose();
        }
    }
}

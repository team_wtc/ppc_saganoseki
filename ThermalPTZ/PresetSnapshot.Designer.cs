﻿namespace CES.ThermalPTZ
{
    partial class PresetSnapshot
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PresetSnapshot));
            this.SnapshotPictureBox = new System.Windows.Forms.PictureBox();
            this.PresetNumberLabel = new System.Windows.Forms.Label();
            this.MaxTempLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.SnapshotPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // SnapshotPictureBox
            // 
            this.SnapshotPictureBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SnapshotPictureBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("SnapshotPictureBox.BackgroundImage")));
            this.SnapshotPictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.SnapshotPictureBox.Location = new System.Drawing.Point(3, 51);
            this.SnapshotPictureBox.Name = "SnapshotPictureBox";
            this.SnapshotPictureBox.Size = new System.Drawing.Size(192, 146);
            this.SnapshotPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.SnapshotPictureBox.TabIndex = 0;
            this.SnapshotPictureBox.TabStop = false;
            // 
            // PresetNumberLabel
            // 
            this.PresetNumberLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PresetNumberLabel.BackColor = System.Drawing.Color.WhiteSmoke;
            this.PresetNumberLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.PresetNumberLabel.ForeColor = System.Drawing.Color.Black;
            this.PresetNumberLabel.Location = new System.Drawing.Point(3, 2);
            this.PresetNumberLabel.Name = "PresetNumberLabel";
            this.PresetNumberLabel.Size = new System.Drawing.Size(192, 22);
            this.PresetNumberLabel.TabIndex = 1;
            this.PresetNumberLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // MaxTempLabel
            // 
            this.MaxTempLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MaxTempLabel.BackColor = System.Drawing.Color.WhiteSmoke;
            this.MaxTempLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.MaxTempLabel.ForeColor = System.Drawing.Color.Black;
            this.MaxTempLabel.Location = new System.Drawing.Point(3, 26);
            this.MaxTempLabel.Name = "MaxTempLabel";
            this.MaxTempLabel.Size = new System.Drawing.Size(192, 22);
            this.MaxTempLabel.TabIndex = 2;
            this.MaxTempLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // PresetSnapshot
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.Black;
            this.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Controls.Add(this.MaxTempLabel);
            this.Controls.Add(this.PresetNumberLabel);
            this.Controls.Add(this.SnapshotPictureBox);
            this.Font = new System.Drawing.Font("メイリオ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.MaximumSize = new System.Drawing.Size(268, 260);
            this.Name = "PresetSnapshot";
            this.Size = new System.Drawing.Size(200, 200);
            ((System.ComponentModel.ISupportInitialize)(this.SnapshotPictureBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox SnapshotPictureBox;
        private System.Windows.Forms.Label PresetNumberLabel;
        private System.Windows.Forms.Label MaxTempLabel;
    }
}

﻿namespace CES.ThermalPTZ
{
    partial class AlarmForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.AlarmListView = new System.Windows.Forms.ListView();
            this.DateColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.AreaNameColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.TemperatureColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.AlarmImagePictureBox = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.AlarmImagePictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // AlarmListView
            // 
            this.AlarmListView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.AlarmListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.DateColumnHeader,
            this.AreaNameColumnHeader,
            this.TemperatureColumnHeader});
            this.AlarmListView.FullRowSelect = true;
            this.AlarmListView.GridLines = true;
            this.AlarmListView.HideSelection = false;
            this.AlarmListView.Location = new System.Drawing.Point(12, 12);
            this.AlarmListView.MultiSelect = false;
            this.AlarmListView.Name = "AlarmListView";
            this.AlarmListView.Size = new System.Drawing.Size(604, 484);
            this.AlarmListView.TabIndex = 0;
            this.AlarmListView.UseCompatibleStateImageBehavior = false;
            this.AlarmListView.View = System.Windows.Forms.View.Details;
            this.AlarmListView.SelectedIndexChanged += new System.EventHandler(this.AlarmListView_SelectedIndexChanged);
            // 
            // DateColumnHeader
            // 
            this.DateColumnHeader.Text = "発生日時";
            this.DateColumnHeader.Width = 260;
            // 
            // AreaNameColumnHeader
            // 
            this.AreaNameColumnHeader.Text = "エリア";
            this.AreaNameColumnHeader.Width = 200;
            // 
            // TemperatureColumnHeader
            // 
            this.TemperatureColumnHeader.Text = "温度";
            this.TemperatureColumnHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TemperatureColumnHeader.Width = 100;
            // 
            // AlarmImagePictureBox
            // 
            this.AlarmImagePictureBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AlarmImagePictureBox.Location = new System.Drawing.Point(622, 12);
            this.AlarmImagePictureBox.Name = "AlarmImagePictureBox";
            this.AlarmImagePictureBox.Size = new System.Drawing.Size(646, 484);
            this.AlarmImagePictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.AlarmImagePictureBox.TabIndex = 1;
            this.AlarmImagePictureBox.TabStop = false;
            // 
            // AlarmForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1279, 508);
            this.Controls.Add(this.AlarmImagePictureBox);
            this.Controls.Add(this.AlarmListView);
            this.Font = new System.Drawing.Font("メイリオ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Name = "AlarmForm";
            this.Text = "温度異常警報履歴";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AlarmForm_FormClosing);
            this.Load += new System.EventHandler(this.AlarmForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.AlarmImagePictureBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView AlarmListView;
        private System.Windows.Forms.ColumnHeader DateColumnHeader;
        private System.Windows.Forms.ColumnHeader AreaNameColumnHeader;
        private System.Windows.Forms.ColumnHeader TemperatureColumnHeader;
        private System.Windows.Forms.PictureBox AlarmImagePictureBox;
    }
}
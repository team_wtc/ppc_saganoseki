﻿namespace CES.ThermalPTZ
{
    partial class AreaRectSetup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.AreaSelectComboBox = new System.Windows.Forms.ComboBox();
            this.AreaLeftLabel = new System.Windows.Forms.Label();
            this.AreaTopLabel = new System.Windows.Forms.Label();
            this.AreaWidthLabel = new System.Windows.Forms.Label();
            this.AreaHeightLabel = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.NewAreaHeightLabel = new System.Windows.Forms.Label();
            this.NewAreaWidthLabel = new System.Windows.Forms.Label();
            this.NewAreaTopLabel = new System.Windows.Forms.Label();
            this.NewAreaLeftLabel = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.AreaUpdateButton = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // AreaSelectComboBox
            // 
            this.AreaSelectComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.AreaSelectComboBox.FormattingEnabled = true;
            this.AreaSelectComboBox.Location = new System.Drawing.Point(12, 12);
            this.AreaSelectComboBox.Name = "AreaSelectComboBox";
            this.AreaSelectComboBox.Size = new System.Drawing.Size(262, 32);
            this.AreaSelectComboBox.TabIndex = 1;
            this.AreaSelectComboBox.SelectedIndexChanged += new System.EventHandler(this.AreaSelectComboBox_SelectedIndexChanged);
            // 
            // AreaLeftLabel
            // 
            this.AreaLeftLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.AreaLeftLabel.Location = new System.Drawing.Point(16, 75);
            this.AreaLeftLabel.Name = "AreaLeftLabel";
            this.AreaLeftLabel.Size = new System.Drawing.Size(60, 30);
            this.AreaLeftLabel.TabIndex = 2;
            this.AreaLeftLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // AreaTopLabel
            // 
            this.AreaTopLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.AreaTopLabel.Location = new System.Drawing.Point(82, 75);
            this.AreaTopLabel.Name = "AreaTopLabel";
            this.AreaTopLabel.Size = new System.Drawing.Size(60, 30);
            this.AreaTopLabel.TabIndex = 3;
            this.AreaTopLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // AreaWidthLabel
            // 
            this.AreaWidthLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.AreaWidthLabel.Location = new System.Drawing.Point(148, 75);
            this.AreaWidthLabel.Name = "AreaWidthLabel";
            this.AreaWidthLabel.Size = new System.Drawing.Size(60, 30);
            this.AreaWidthLabel.TabIndex = 4;
            this.AreaWidthLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // AreaHeightLabel
            // 
            this.AreaHeightLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.AreaHeightLabel.Location = new System.Drawing.Point(214, 75);
            this.AreaHeightLabel.Name = "AreaHeightLabel";
            this.AreaHeightLabel.Size = new System.Drawing.Size(60, 30);
            this.AreaHeightLabel.TabIndex = 5;
            this.AreaHeightLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 51);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 24);
            this.label5.TabIndex = 6;
            this.label5.Text = "X：";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(82, 51);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(36, 24);
            this.label6.TabIndex = 7;
            this.label6.Text = "Y：";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(148, 51);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 24);
            this.label7.TabIndex = 8;
            this.label7.Text = "W：";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(214, 51);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(38, 24);
            this.label8.TabIndex = 9;
            this.label8.Text = "H：";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(206, 26);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(38, 24);
            this.label9.TabIndex = 17;
            this.label9.Text = "H：";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(140, 26);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(42, 24);
            this.label10.TabIndex = 16;
            this.label10.Text = "W：";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(74, 26);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(36, 24);
            this.label11.TabIndex = 15;
            this.label11.Text = "Y：";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(4, 26);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(37, 24);
            this.label12.TabIndex = 14;
            this.label12.Text = "X：";
            // 
            // NewAreaHeightLabel
            // 
            this.NewAreaHeightLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.NewAreaHeightLabel.Location = new System.Drawing.Point(206, 50);
            this.NewAreaHeightLabel.Name = "NewAreaHeightLabel";
            this.NewAreaHeightLabel.Size = new System.Drawing.Size(60, 30);
            this.NewAreaHeightLabel.TabIndex = 13;
            this.NewAreaHeightLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // NewAreaWidthLabel
            // 
            this.NewAreaWidthLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.NewAreaWidthLabel.Location = new System.Drawing.Point(140, 50);
            this.NewAreaWidthLabel.Name = "NewAreaWidthLabel";
            this.NewAreaWidthLabel.Size = new System.Drawing.Size(60, 30);
            this.NewAreaWidthLabel.TabIndex = 12;
            this.NewAreaWidthLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // NewAreaTopLabel
            // 
            this.NewAreaTopLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.NewAreaTopLabel.Location = new System.Drawing.Point(74, 50);
            this.NewAreaTopLabel.Name = "NewAreaTopLabel";
            this.NewAreaTopLabel.Size = new System.Drawing.Size(60, 30);
            this.NewAreaTopLabel.TabIndex = 11;
            this.NewAreaTopLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // NewAreaLeftLabel
            // 
            this.NewAreaLeftLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.NewAreaLeftLabel.Location = new System.Drawing.Point(8, 50);
            this.NewAreaLeftLabel.Name = "NewAreaLeftLabel";
            this.NewAreaLeftLabel.Size = new System.Drawing.Size(60, 30);
            this.NewAreaLeftLabel.TabIndex = 10;
            this.NewAreaLeftLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox1.Controls.Add(this.AreaUpdateButton);
            this.groupBox1.Controls.Add(this.NewAreaWidthLabel);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.NewAreaLeftLabel);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.NewAreaTopLabel);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.NewAreaHeightLabel);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Location = new System.Drawing.Point(2, 448);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(272, 134);
            this.groupBox1.TabIndex = 18;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "選択領域";
            // 
            // AreaUpdateButton
            // 
            this.AreaUpdateButton.Location = new System.Drawing.Point(8, 93);
            this.AreaUpdateButton.Name = "AreaUpdateButton";
            this.AreaUpdateButton.Size = new System.Drawing.Size(258, 30);
            this.AreaUpdateButton.TabIndex = 18;
            this.AreaUpdateButton.Text = "エリア領域を更新";
            this.AreaUpdateButton.UseVisualStyleBackColor = true;
            this.AreaUpdateButton.Click += new System.EventHandler(this.AreaUpdateButton_Click);
            // 
            // AreaRectSetup
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1094, 588);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.AreaHeightLabel);
            this.Controls.Add(this.AreaWidthLabel);
            this.Controls.Add(this.AreaTopLabel);
            this.Controls.Add(this.AreaLeftLabel);
            this.Controls.Add(this.AreaSelectComboBox);
            this.Font = new System.Drawing.Font("メイリオ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AreaRectSetup";
            this.Text = "エリア領域設定";
            this.Load += new System.EventHandler(this.AlarmRectSetup_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox AreaSelectComboBox;
        private System.Windows.Forms.Label AreaLeftLabel;
        private System.Windows.Forms.Label AreaTopLabel;
        private System.Windows.Forms.Label AreaWidthLabel;
        private System.Windows.Forms.Label AreaHeightLabel;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label NewAreaHeightLabel;
        private System.Windows.Forms.Label NewAreaWidthLabel;
        private System.Windows.Forms.Label NewAreaTopLabel;
        private System.Windows.Forms.Label NewAreaLeftLabel;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button AreaUpdateButton;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace CES.ThermalPTZ
{
    using static System.Diagnostics.Trace;
    using static Properties.Settings;
    using static Properties.Resources;

    public partial class MainForm : Form
    {
        ConfigSetupForm _ConfigSetupForm = null;
        SequenceSetupForm _SequenceSetupForm = null;
        AreaSetupForm _AreaSetupForm = null;
        AlarmForm _AlarmForm = null;
        AreaRectSetup _AreaRectSetup = null;

        int _PatrolArea = -1;
        DateTime? _LogDeleteTime = null;

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load( object sender, EventArgs e )
        {
            WriteLine( $"{Application.ProductName} ({Application.ProductVersion}) Stautup." );

            InitScreen();
            InitStreaming();
            InitPanhead();
            InitCameraAccess();
            InitIOBox();

            _AutoPatrolThread = new Threads.AutoPatrolThread( _PresetTable, _PanheadAccessThread, _CameraAccessThread );
            _AutoPatrolThread.AutoPatrol += this._AutoPatrolThread_AutoPatrol;
            _AutoPatrolThread.Start();

            if ( ( Default.AutoStartPatrol == true ) && ( _SendAutoStart.HasValue == true ) )
            {
                AutoPatrolCheckBox.Text = "自動巡回開始待機中";
                AutoPatrolCheckBox.Enabled = false;
                AutoPatrolCheckBox.Checked = false;
                SetRadioButtonEnable( false );
            }

            MainTimer.Enabled = true;
        }

        private void _AutoPatrolThread_AutoPatrol( object sender, Threads.AutoPatrolEventArgs e )
        {
            if ( ( _CurrentAreaNumber.HasValue == false ) || ( _CurrentAreaNumber.Value != e.AreaNumber ) )
            {
                _CurrentAreaNumber = e.AreaNumber;
            }

            if ( e.AreaNumber < 0 )
            {
                float WaitTime = e.WaitTime;
                if ( WaitTime < 1.0F )
                {
                    WaitTime = 1.0F;
                }
                MainTitleLabel.Text = $"次回の自動巡回実行まで {Convert.ToInt32( WaitTime )} 秒";

                PatrolStatusLabel.Text = string.Empty;
                PanTiltTargetLabel.Text = string.Empty;
                if ( _PatrolArea != e.AreaNumber )
                {
                    try
                    {
                        LayoutPictureBox.Image?.Dispose();
                        LayoutPictureBox.Image = _LayoutImage.RotateImage( _PanAngle );
                    }
                    catch
                    {

                    }
                    _PatrolArea = e.AreaNumber;
                }
            }
            else if ( e.PatrolMode == Threads.AutoPatrolThread.PATROL_MODE.PATROL_WAIT_AUTO_FOCUS )
            {
                float WaitTime = e.WaitTime;
                if ( WaitTime < 1.0F )
                {
                    WaitTime = 1.0F;
                }
                PatrolStatusLabel.Text = $"オートフォーカス実行待機中...　{Convert.ToInt32( WaitTime )} 秒";
            }
            else if ( e.PatrolMode == Threads.AutoPatrolThread.PATROL_MODE.PATROL_WAIT_AREA_CHANGE )
            {
                float WaitTime = e.WaitTime;
                if ( WaitTime < 1.0F )
                {
                    WaitTime = 1.0F;
                }
                PatrolStatusLabel.Text = $"エリア移動待機中...　{Convert.ToInt32( WaitTime )} 秒";
            }
            else if ( e.PatrolMode == Threads.AutoPatrolThread.PATROL_MODE.ALARM_POSITION )
            {
                PatrolStatusLabel.Text = $"異常発生位置へ画角調整中...";
            }
            else
            {
                MainTitleLabel.Text = $"自動巡回中　( {e.AreaName} )";
                PanTiltTargetLabel.Text = $"Pan={e.Pan:F2}°   Tilt={e.Tilt:F2}°";

                if ( _PresetTable.AreaRect( e.AreaNumber, out int AreaLeft, out int AreaTop, out int AreaWidth, out int AreaHeight ) == true )
                {
                    if ( _PatrolArea != e.AreaNumber )
                    {
                        try
                        {
                            LayoutPictureBox.Image?.Dispose();
                            LayoutPictureBox.Image = _LayoutImage.AreaImage( _PanAngle, AreaLeft, AreaTop, AreaWidth, AreaHeight );
                        }
                        catch
                        {

                        }
                        _PatrolArea = e.AreaNumber;
                        PatrolStatusLabel.Text = $"エリア ({e.AreaName}) へ移動中...";
                    }
                    else
                    {
                        PatrolStatusLabel.Text = $"プリセット ({e.PresetNumber}) へ移動中...";
                    }
                }
                else
                {
                    if ( _PatrolArea != e.AreaNumber )
                    {
                        try
                        {
                            LayoutPictureBox.Image?.Dispose();
                            LayoutPictureBox.Image = _LayoutImage.BaseImage();
                        }
                        catch
                        {
                        }
                        _PatrolArea = e.AreaNumber;
                        PatrolStatusLabel.Text = $"エリア ({e.AreaName}) へ移動中...";
                    }
                    else
                    {
                        PatrolStatusLabel.Text = $"プリセット ({e.PresetNumber}) へ移動中...";
                    }
                }
            }
        }

        protected override bool ProcessCmdKey( ref Message msg, Keys keyData )
        {
            const Keys SpecialKey = ( Keys.Control | Keys.Shift | Keys.F12 );

            if ( keyData == Keys.Escape )
            {
                if ( ( _AutoPatrolThread?.IsPatrolStop == true ) || ( _AutoPatrolThread?.IsPatrolPause == true ) )
                {
                    Close();
                    return true;
                }
            }

            if ( keyData == SpecialKey )
            {
                AlarmClear();
            }
            return base.ProcessCmdKey( ref msg, keyData );
        }

        private async void MainTimer_Tick( object sender, EventArgs e )
        {
            ClockLabel.Text = $"{DateTime.Now.ToString( "yyyy年MM月dd日(ddd)  HH:mm:ss" )}";

            if ( ( Default.AutoStartPatrol == true ) &&
                ( _SendAutoStart.HasValue == true ) )
            {
                if ( _SendAutoStart.Value <= DateTime.Now )
                {
                    AutoPatrolCheckBox.Enabled = true;
                    AutoPatrolCheckBox.Checked = true;
                    _SendAutoStart = null;
                }
                else
                {
                    if ( AutoPatrolCheckBox.Enabled != false )
                    {
                        AutoPatrolCheckBox.Enabled = false;
                    }
                    PatrolStatusLabel.Text = $"巡回開始まで{( _SendAutoStart.Value - DateTime.Now ).TotalSeconds:F0}秒";
                }
            }

            if ( ( _LogDeleteTime.HasValue == false ) ||
                ( _LogDeleteTime.Value <= DateTime.Now ) )
            {
                _LogDeleteTime = DateTime.Today.AddDays( 1.0 );

                await Task.Factory.StartNew( () =>
                {
                    _PresetTable.DeleteAlarm( Default.AlarmSaveDays );
                } );

                await Task.Factory.StartNew( () =>
                {
                    DateTime SaveLimit = DateTime.Today.AddDays( -( Default.LogSaveDays ) );

                    var LogBaseFolder = Path.Combine( Application.StartupPath, "Log" );
                    var Dirs = Directory.GetDirectories( LogBaseFolder, "*.", SearchOption.TopDirectoryOnly );


                    foreach ( var SubDir in Dirs )
                    {
                        var DirInfo = new DirectoryInfo( SubDir );
                        DateTime? DirDate = null;

                        if ( DirInfo.Name.Length == 8 )
                        {
                            var yearStr = DirInfo.Name.Substring( 0, 4 );
                            var monthStr = DirInfo.Name.Substring(4, 2 );
                            var dayStr = DirInfo.Name.Substring(6, 2 );

                            try
                            {
                                DirDate = new DateTime( Convert.ToInt32( yearStr ), Convert.ToInt32( monthStr ), Convert.ToInt32( dayStr ) );
                            }
                            catch
                            {
                                DirDate = null;
                            }
                        }

                        if ( DirDate.HasValue == false )
                        {
                            continue;
                        }
                        if ( DirDate.Value >= SaveLimit )
                        {
                            continue;
                        }

                        try
                        {
                            DirInfo.Delete( true );
                            WriteLine( $"ログフォルダー削除　\"{DirInfo.FullName}\"" );
                        }
                        catch
                        {

                        }
                    }
                } );

            }
        }

        private void MainForm_FormClosing( object sender, FormClosingEventArgs e )
        {
            if ( MessageBox.Show(
                "サーマルPTZカメラアプリケーションを終了しますか？",
                "終了",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Question,
                MessageBoxDefaultButton.Button2 ) == DialogResult.No )
            {
                e.Cancel = true;
                return;
            }

            //TerminateIOBox();
            TerminateCamera();
            TerminatePanhead();
            TerminateStreaming();
            WriteLine( $"{Application.ProductName} Terminate." );
        }

        private void AutoPatrolCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            AutoPatrolMode( AutoPatrolCheckBox.Checked );
        }

        private void LiveImagePictureBox_Resize( object sender, EventArgs e )
        {
        }

        private void AutoFocusButton_Click( object sender, EventArgs e )
        {
            _CameraAccessThread.ExecAutoFocus();
        }

        private void MainForm_Shown( object sender, EventArgs e )
        {
        }

        private void ConfigGroupBox_Resize( object sender, EventArgs e )
        {
            const int Span = 18;
            const int SpanCount = 4;

            var BtnWidth = ( ConfigGroupBox.ClientRectangle.Width - ( Span * SpanCount ) ) / 3;

            AreaSetupRadioButton.Left = Span;
            AreaSetupRadioButton.Width = BtnWidth;
            AreaRectSetupRadioButton.Left = AreaSetupRadioButton.Right + Span;
            AreaRectSetupRadioButton.Width = BtnWidth;
            PresetRadioButton.Left = AreaRectSetupRadioButton.Right + Span;
            PresetRadioButton.Width = BtnWidth;

            HistoryViewRadioButton.Left = Span;
            HistoryViewRadioButton.Width = BtnWidth;
            CameraSetupRadioButton.Left = HistoryViewRadioButton.Right + Span;
            CameraSetupRadioButton.Width = BtnWidth;
        }

        private void LiveImagePictureBox_MouseDoubleClick( object sender, MouseEventArgs e )
        {
            if ( ( e.Button & MouseButtons.Left ) == MouseButtons.Left )
            {
                BaseSplitContainer.Visible = false;
                _FullScreenPictureBox.Visible = true;
            }
        }

        private void FullScreenPictureBox_MouseDoubleClick( object sender, MouseEventArgs e )
        {
            if ( ( e.Button & MouseButtons.Left ) == MouseButtons.Left )
            {
                _FullScreenPictureBox.Visible = false;
                BaseSplitContainer.Visible = true;
            }
        }

        private void PresetRadioButton_CheckedChanged( object sender, EventArgs e )
        {
            if ( PresetRadioButton.Checked == false )
            {
                return;
            }

            if ( _SequenceSetupForm == null )
            {
                CameraSetupRadioButton.Enabled = false;
                PresetRadioButton.Enabled = false;
                AreaSetupRadioButton.Enabled = false;
                AreaRectSetupRadioButton.Enabled = false;
                HistoryViewRadioButton.Enabled = false;
                _SequenceSetupForm = new SequenceSetupForm( _PresetTable, _PanheadAccessThread );
                _SequenceSetupForm.FormClosed += this._SequenceSetupForm_FormClosed;
                AddOwnedForm( _SequenceSetupForm );
                _SequenceSetupForm.Show();
            }
        }

        private void _SequenceSetupForm_FormClosed( object sender, FormClosedEventArgs e )
        {
            if ( _SequenceSetupForm != null )
            {
                RemoveOwnedForm( _SequenceSetupForm );
                _SequenceSetupForm = null;
                CameraSetupRadioButton.Enabled = true;
                CameraSetupRadioButton.Checked = false;
                PresetRadioButton.Enabled = true;
                PresetRadioButton.Checked = false;
                AreaSetupRadioButton.Enabled = true;
                AreaSetupRadioButton.Checked = false;
                AreaRectSetupRadioButton.Enabled = true;
                AreaRectSetupRadioButton.Checked = false;
                HistoryViewRadioButton.Enabled = true;
                HistoryViewRadioButton.Checked = false;
            }
        }

        private void CameraSetupRadioButton_CheckedChanged( object sender, EventArgs e )
        {
            if ( CameraSetupRadioButton.Checked == false )
            {
                return;
            }

            if ( _ConfigSetupForm == null )
            {
                CameraSetupRadioButton.Enabled = false;
                PresetRadioButton.Enabled = false;
                AreaSetupRadioButton.Enabled = false;
                AreaRectSetupRadioButton.Enabled = false;
                HistoryViewRadioButton.Enabled = false;
                _ConfigSetupForm = new ConfigSetupForm( _IOSetter );
                _ConfigSetupForm.FormClosed += this._ConfigSetupForm_FormClosed;
                AddOwnedForm( _ConfigSetupForm );
                _ConfigSetupForm.Show();
            }
        }

        private void _ConfigSetupForm_FormClosed( object sender, FormClosedEventArgs e )
        {
            if ( _ConfigSetupForm != null )
            {
                RemoveOwnedForm( _ConfigSetupForm );
                _ConfigSetupForm = null;
                CameraSetupRadioButton.Enabled = true;
                CameraSetupRadioButton.Checked = false;
                PresetRadioButton.Enabled = true;
                PresetRadioButton.Checked = false;
                AreaSetupRadioButton.Enabled = true;
                AreaSetupRadioButton.Checked = false;
                AreaRectSetupRadioButton.Enabled = true;
                AreaRectSetupRadioButton.Checked = false;
                HistoryViewRadioButton.Enabled = true;
                HistoryViewRadioButton.Checked = false;
            }
        }

        private void HomePositionMoveButton_Click( object sender, EventArgs e )
        {
            _PanheadAccessThread.SetPresetSpeed( MAX_SPEED );
            _PanheadAccessThread.GotoPreset( HOME_POSITION );

            //  2020/10/14 現地修正　GotoPreset3回出し
            Thread.Sleep( 500 );
            _PanheadAccessThread.GotoPreset( HOME_POSITION );
            Thread.Sleep( 500 );
            _PanheadAccessThread.GotoPreset( HOME_POSITION );
        }

        private void HomePositionSetButton_Click( object sender, EventArgs e )
        {
            var MsgResult = MessageBox.Show(
                $"現在の画角をホームポジションに設定します。{Environment.NewLine}" +
                $"よろしいですか？",
                "ホーム設定",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Question );
            if ( MsgResult != DialogResult.Yes )
            {
                return;
            }
            _PanheadAccessThread.SetPreset( HOME_POSITION );
        }

        private void AlarmClearButton_Click( object sender, EventArgs e )
        {
            AlarmClear();
        }

        private void PTLeftUpButton_MouseDown( object sender, MouseEventArgs e )
        {
            if ( ( e.Button & MouseButtons.Left ) == MouseButtons.Left )
            {
                PTLeftUpButton.Image = left_up_push;
                _PanheadAccessThread?.PanTilt( false, true, false, true, GetPanTiltSpeed() );
            }
        }

        private void PTLeftUpButton_MouseUp( object sender, MouseEventArgs e )
        {
            if ( ( e.Button & MouseButtons.Left ) == MouseButtons.Left )
            {
                PTLeftUpButton.Image = left_up;
                _PanheadAccessThread?.MoveStop();
            }
        }

        private void PTUpButton_MouseDown( object sender, MouseEventArgs e )
        {
            if ( ( e.Button & MouseButtons.Left ) == MouseButtons.Left )
            {
                PTUpButton.Image = up_push;
                _PanheadAccessThread?.PanTilt( true, true, false, true, GetPanTiltSpeed() );
            }
        }

        private void PTUpButton_MouseUp( object sender, MouseEventArgs e )
        {
            if ( ( e.Button & MouseButtons.Left ) == MouseButtons.Left )
            {
                PTUpButton.Image = up;
                _PanheadAccessThread?.MoveStop();
            }
        }

        private void PTRightUpButton_MouseDown( object sender, MouseEventArgs e )
        {
            if ( ( e.Button & MouseButtons.Left ) == MouseButtons.Left )
            {
                PTRightUpButton.Image = right_up_push;
                _PanheadAccessThread?.PanTilt( false, false, false, true, GetPanTiltSpeed() );
            }
        }

        private void PTRightUpButton_MouseUp( object sender, MouseEventArgs e )
        {
            if ( ( e.Button & MouseButtons.Left ) == MouseButtons.Left )
            {
                PTRightUpButton.Image = right_up;
                _PanheadAccessThread?.MoveStop();
            }
        }

        private void PTLeftButton_MouseDown( object sender, MouseEventArgs e )
        {
            if ( ( e.Button & MouseButtons.Left ) == MouseButtons.Left )
            {
                PTLeftButton.Image = left_push;
                _PanheadAccessThread?.PanTilt( false, true, true, true, GetPanTiltSpeed() );
            }
        }

        private void PTLeftButton_MouseUp( object sender, MouseEventArgs e )
        {
            if ( ( e.Button & MouseButtons.Left ) == MouseButtons.Left )
            {
                PTLeftButton.Image = left;
                _PanheadAccessThread?.MoveStop();
            }
        }

        private void PTRightButton_MouseDown( object sender, MouseEventArgs e )
        {
            if ( ( e.Button & MouseButtons.Left ) == MouseButtons.Left )
            {
                PTRightButton.Image = right_push;
                _PanheadAccessThread?.PanTilt( false, false, true, true, GetPanTiltSpeed() );
            }
        }

        private void PTRightButton_MouseUp( object sender, MouseEventArgs e )
        {
            if ( ( e.Button & MouseButtons.Left ) == MouseButtons.Left )
            {
                PTRightButton.Image = right;
                _PanheadAccessThread?.MoveStop();
            }
        }

        private void PTLeftDownButton_MouseDown( object sender, MouseEventArgs e )
        {
            if ( ( e.Button & MouseButtons.Left ) == MouseButtons.Left )
            {
                PTLeftDownButton.Image = left_down_push;
                _PanheadAccessThread?.PanTilt( false, true, false, false, GetPanTiltSpeed() );
            }
        }

        private void PTLeftDownButton_MouseUp( object sender, MouseEventArgs e )
        {
            if ( ( e.Button & MouseButtons.Left ) == MouseButtons.Left )
            {
                PTLeftDownButton.Image = left_down;
                _PanheadAccessThread?.MoveStop();
            }
        }

        private void PTDownButton_MouseDown( object sender, MouseEventArgs e )
        {
            if ( ( e.Button & MouseButtons.Left ) == MouseButtons.Left )
            {
                PTDownButton.Image = down_push;
                _PanheadAccessThread?.PanTilt( true, true, false, false, GetPanTiltSpeed() );
            }
        }

        private void PTDownButton_MouseUp( object sender, MouseEventArgs e )
        {
            if ( ( e.Button & MouseButtons.Left ) == MouseButtons.Left )
            {
                PTDownButton.Image = down;
                _PanheadAccessThread?.MoveStop();
            }
        }

        private void PTRightDownButton_MouseDown( object sender, MouseEventArgs e )
        {
            if ( ( e.Button & MouseButtons.Left ) == MouseButtons.Left )
            {
                PTRightDownButton.Image = right_down_push;
                _PanheadAccessThread?.PanTilt( false, false, false, false, GetPanTiltSpeed() );
            }
        }

        private void PTRightDownButton_MouseUp( object sender, MouseEventArgs e )
        {
            if ( ( e.Button & MouseButtons.Left ) == MouseButtons.Left )
            {
                PTRightDownButton.Image = right_down;
                _PanheadAccessThread?.MoveStop();
            }
        }

        byte GetPanTiltSpeed()
        {
            if ( SpeedRadioButton1.Checked == true )
            {
                return 0x01;
            }
            else if ( SpeedRadioButton2.Checked == true )
            {
                return 0x10;
            }
            else if ( SpeedRadioButton3.Checked == true )
            {
                return 0x20;
            }
            else if ( SpeedRadioButton4.Checked == true )
            {
                return 0x30;
            }
            else if ( SpeedRadioButton5.Checked == true )
            {
                return 0x3F;
            }
            else
            {
                return 0x20;
            }
        }

        private void PTHomeButton_Click( object sender, EventArgs e )
        {
            _PanheadAccessThread.SetPresetSpeed( MAX_SPEED );
            _PanheadAccessThread.GotoPreset( HOME_POSITION );
            //  2020/10/14 現地修正　GotoPreset3回出し
            Thread.Sleep( 500 );
            _PanheadAccessThread.GotoPreset( HOME_POSITION );
            Thread.Sleep( 500 );
            _PanheadAccessThread.GotoPreset( HOME_POSITION );
        }

        private void AreaSetupRadioButton_CheckedChanged( object sender, EventArgs e )
        {
            if ( AreaSetupRadioButton.Checked == false )
            {
                return;
            }

            if ( _AreaSetupForm == null )
            {
                CameraSetupRadioButton.Enabled = false;
                PresetRadioButton.Enabled = false;
                AreaSetupRadioButton.Enabled = false;
                AreaRectSetupRadioButton.Enabled = false;
                HistoryViewRadioButton.Enabled = false;
                _AreaSetupForm = new AreaSetupForm( _PresetTable );
                _AreaSetupForm.FormClosed += this._AreaSetupForm_FormClosed;
                ;
                AddOwnedForm( _AreaSetupForm );
                _AreaSetupForm.Show();
            }
        }

        private void _AreaSetupForm_FormClosed( object sender, FormClosedEventArgs e )
        {
            if ( _AreaSetupForm != null )
            {
                RemoveOwnedForm( _AreaSetupForm );
                _AreaSetupForm = null;
                CameraSetupRadioButton.Enabled = true;
                CameraSetupRadioButton.Checked = false;
                PresetRadioButton.Enabled = true;
                PresetRadioButton.Checked = false;
                AreaSetupRadioButton.Enabled = true;
                AreaSetupRadioButton.Checked = false;
                AreaRectSetupRadioButton.Enabled = true;
                AreaRectSetupRadioButton.Checked = false;
                HistoryViewRadioButton.Enabled = true;
                HistoryViewRadioButton.Checked = false;
            }
        }

        private void HistoryViewRadioButton_CheckedChanged( object sender, EventArgs e )
        {
            if ( HistoryViewRadioButton.Checked == false )
            {
                return;
            }

            if ( _AlarmForm == null )
            {
                CameraSetupRadioButton.Enabled = false;
                PresetRadioButton.Enabled = false;
                AreaSetupRadioButton.Enabled = false;
                AreaRectSetupRadioButton.Enabled = false;
                HistoryViewRadioButton.Enabled = false;
                _AlarmForm = new AlarmForm( _PresetTable );
                _AlarmForm.FormClosed += this._AlarmForm_FormClosed;
                AddOwnedForm( _AlarmForm );
                _AlarmForm.Show();
            }
        }

        private void _AlarmForm_FormClosed( object sender, FormClosedEventArgs e )
        {
            if ( _AlarmForm != null )
            {
                RemoveOwnedForm( _AlarmForm );
                _AlarmForm = null;
                CameraSetupRadioButton.Enabled = true;
                CameraSetupRadioButton.Checked = false;
                PresetRadioButton.Enabled = true;
                PresetRadioButton.Checked = false;
                AreaSetupRadioButton.Enabled = true;
                AreaSetupRadioButton.Checked = false;
                AreaRectSetupRadioButton.Enabled = true;
                AreaRectSetupRadioButton.Checked = false;
                HistoryViewRadioButton.Enabled = true;
                HistoryViewRadioButton.Checked = false;
            }
        }

        private void AreaRectSetupRadioButton_CheckedChanged( object sender, EventArgs e )
        {
            if ( AreaRectSetupRadioButton.Checked == false )
            {
                return;
            }

            if ( _AreaRectSetup == null )
            {
                CameraSetupRadioButton.Enabled = false;
                PresetRadioButton.Enabled = false;
                AreaSetupRadioButton.Enabled = false;
                AreaRectSetupRadioButton.Enabled = false;
                HistoryViewRadioButton.Enabled = false;
                _AreaRectSetup = new AreaRectSetup( _PresetTable );
                _AreaRectSetup.FormClosed += this._AreaRectSetup_FormClosed;
                AddOwnedForm( _AreaRectSetup );
                _AreaRectSetup.Show();
            }
        }

        private void _AreaRectSetup_FormClosed( object sender, FormClosedEventArgs e )
        {
            if ( _AreaRectSetup != null )
            {
                RemoveOwnedForm( _AreaRectSetup );
                _AreaRectSetup = null;
                CameraSetupRadioButton.Enabled = true;
                CameraSetupRadioButton.Checked = false;
                PresetRadioButton.Enabled = true;
                PresetRadioButton.Checked = false;
                AreaSetupRadioButton.Enabled = true;
                AreaSetupRadioButton.Checked = false;
                AreaRectSetupRadioButton.Enabled = true;
                AreaRectSetupRadioButton.Checked = false;
                HistoryViewRadioButton.Enabled = true;
                HistoryViewRadioButton.Checked = false;
            }
        }

        private void SnapshotButton_Click( object sender, EventArgs e )
        {
            _CameraAccessThread?.GetNormalSnapshot();
        }

        private void FocusNearButton_MouseDown( object sender, MouseEventArgs e )
        {
            if ( e.Button == MouseButtons.Left )
            {
                _CameraAccessThread.SetManualFocus( -1 );
            }
        }

        private void FocusNearButton_MouseUp( object sender, MouseEventArgs e )
        {
            if ( e.Button == MouseButtons.Left )
            {
                _CameraAccessThread.SetManualFocus( 0 );
            }
        }

        private void FocusFarButton_MouseDown( object sender, MouseEventArgs e )
        {
            if ( e.Button == MouseButtons.Left )
            {
                _CameraAccessThread.SetManualFocus( 1 );
            }
        }

        private void FocusFarButton_MouseUp( object sender, MouseEventArgs e )
        {
            if ( e.Button == MouseButtons.Left )
            {
                _CameraAccessThread.SetManualFocus( 0 );
            }
        }

        private void FocusNearButton_Click( object sender, EventArgs e )
        {
            _CameraAccessThread.SetManualFocus( 0 );
        }

        private void FocusFarButton_Click( object sender, EventArgs e )
        {
            _CameraAccessThread.SetManualFocus( 0 );
        }
    }
}

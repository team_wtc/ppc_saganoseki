﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

namespace CES.ThermalPTZ
{
    public class AreaRectPictureBox : PictureBox
    {
        public event EventHandler<NewAreaRectEventArgs> NewAreaRect;

        Rectangle? _AreaRectangle = null;
        Point? _MousePosition = null;
        Point? _StartPosition = null;
        Point? _EndPosition = null;
        bool _MouseDown = false;

        public AreaRectPictureBox() : base()
        {
        }

        public void SetAreaRect( int AreaLeft, int AreaTop, int AreaWidth, int AreaHeight )
        {
            _AreaRectangle = new Rectangle( AreaLeft, AreaTop, AreaWidth, AreaHeight );
            _MouseDown = false;
            _StartPosition = null;
            _EndPosition = null;
            Invalidate();
        }

        public void ClearAreaRect()
        {
            _AreaRectangle = null;
            _MouseDown = false;
            _StartPosition = null;
            _EndPosition = null;
            Invalidate();
        }

        public bool GetAreaRect( out int AreaLeft, out int AreaTop, out int AreaWidth, out int AreaHeight )
        {
            AreaLeft = 0;
            AreaTop = 0;
            AreaWidth = 0;
            AreaHeight = 0;

            Rectangle? NewAreaRect = CalcRectangle();
            if ( NewAreaRect.HasValue == false )
            {
                return false;
            }

            AreaLeft = NewAreaRect.Value.Left;
            AreaTop = NewAreaRect.Value.Top;
            AreaWidth = NewAreaRect.Value.Width;
            AreaHeight = NewAreaRect.Value.Height;
            return true;
        }

        protected override void OnMouseEnter( EventArgs e )
        {
            base.OnMouseEnter( e );
            _MousePosition = MousePosition;
            Invalidate();
        }

        protected override void OnMouseLeave( EventArgs e )
        {
            base.OnMouseLeave( e );
            _MousePosition = null;
            Invalidate();
        }

        protected override void OnMouseMove( MouseEventArgs e )
        {
            base.OnMouseMove( e );
            _MousePosition = e.Location;
            if ( ( _StartPosition.HasValue == true ) && ( _MouseDown == true ) )
            {
                //_EndPosition = e.Location;

                int EndX = e.X;
                int EndY = e.Y;
                if ( e.X < 0 )
                {
                    EndX = 0;
                }
                else if ( e.X > ClientSize.Width )
                {
                    EndX = ClientSize.Width;
                }
                if ( e.Y < 0 )
                {
                    EndY = 0;
                }
                else if ( e.Y > ClientSize.Height )
                {
                    EndY = ClientSize.Height;
                }
                _EndPosition = new Point( EndX, EndY );
            }
            Invalidate();
        }

        protected override void OnMouseDown( MouseEventArgs e )
        {
            base.OnMouseDown( e );

            if ( e.Button == MouseButtons.Left )
            {
                _MouseDown = true;
                _StartPosition = e.Location;
                _EndPosition = e.Location;
            }
        }
        protected override void OnMouseUp( MouseEventArgs e )
        {
            base.OnMouseUp( e );
            if ( e.Button == MouseButtons.Left )
            {
                _MouseDown = false;
                //_EndPosition = e.Location;
                int EndX = e.X;
                int EndY = e.Y;
                if ( e.X < 0 )
                {
                    EndX = 0;
                }
                else if ( e.X > ClientSize.Width )
                {
                    EndX = ClientSize.Width;
                }
                if ( e.Y < 0 )
                {
                    EndY = 0;
                }
                else if ( e.Y > ClientSize.Height )
                {
                    EndY = ClientSize.Height;
                }
                _EndPosition = new Point( EndX, EndY );
            }
        }
        protected override void OnPaint( PaintEventArgs pe )
        {
            base.OnPaint( pe );

            using ( var AreaFillBrush = new SolidBrush( Color.FromArgb( 96, Color.Blue ) ) )
            using ( var AreaDrawPen = new Pen( Color.Blue, 5F ) )

            using ( var RectFillBrush = new SolidBrush( Color.FromArgb( 96, Color.Gold ) ) )
            using ( var RectDrawPen = new Pen( Color.Gold, 5F ) )

            using ( var CursorBrush = new SolidBrush( Color.FromArgb( 96, Color.Gray ) ) )
            using ( var CursorPen = new Pen( CursorBrush, 5F ) )
            {
                if ( _AreaRectangle.HasValue == true )
                {
                    pe.Graphics.FillRectangle( AreaFillBrush, _AreaRectangle.Value );
                    pe.Graphics.DrawRectangle( AreaDrawPen, _AreaRectangle.Value );
                }

                Rectangle? DrawRect = CalcRectangle();
                if ( DrawRect.HasValue == true )
                {
                    pe.Graphics.FillRectangle( RectFillBrush, DrawRect.Value );
                    pe.Graphics.DrawRectangle( RectDrawPen, DrawRect.Value );
                }

                if ( _MousePosition.HasValue == true )
                {
                    pe.Graphics.DrawLine( CursorPen, new Point( _MousePosition.Value.X, 0 ), new Point( _MousePosition.Value.X, ClientSize.Height ) );
                    pe.Graphics.DrawLine( CursorPen, new Point( 0, _MousePosition.Value.Y ), new Point( ClientSize.Width, _MousePosition.Value.Y ) );
                }
            }

            NewAreaRectEvent();
        }

        private Rectangle? CalcRectangle()
        {
            if ( ( _StartPosition.HasValue == false ) || ( _EndPosition.HasValue == false ) )
            {
                return null;
            }

            var x = ( _StartPosition.Value.X <= _EndPosition.Value.X ) ? _StartPosition.Value.X : _EndPosition.Value.X;
            var y = ( _StartPosition.Value.Y <= _EndPosition.Value.Y ) ? _StartPosition.Value.Y : _EndPosition.Value.Y;
            var w = Math.Abs( _StartPosition.Value.X - _EndPosition.Value.X );
            var h = Math.Abs( _StartPosition.Value.Y - _EndPosition.Value.Y );

            return new Rectangle( x, y, w, h );
        }

        private void NewAreaRectEvent()
        {
            if ( NewAreaRect != null )
            {
                if ( ( NewAreaRect.Target != null ) && ( NewAreaRect.Target is Control Cntl ) )
                {
                    Cntl.BeginInvoke( NewAreaRect, this, new NewAreaRectEventArgs( CalcRectangle() ) );
                }
                else
                {
                    NewAreaRect( this, new NewAreaRectEventArgs( CalcRectangle() ) );
                }
            }
        }
    }

    public class NewAreaRectEventArgs : EventArgs
    {
        public NewAreaRectEventArgs( Rectangle? AreaRect )
        {
            HasRect = AreaRect.HasValue;
            if ( HasRect == false )
            {
                Rect = Rectangle.Empty;
            }
            else
            {
                Rect = AreaRect.Value;
            }
        }
        public bool HasRect
        {
            get;
            private set;
        }

        public Rectangle Rect
        {
            get;
            private set;
        }
    }
}

﻿namespace CES.ThermalPTZ
{
    partial class SequenceSetupForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.HighSpeedMoveCheckBox = new System.Windows.Forms.CheckBox();
            this.PatrolUpdateButton = new System.Windows.Forms.Button();
            this.SeqDownButton = new System.Windows.Forms.Button();
            this.SeqUpButton = new System.Windows.Forms.Button();
            this.AutoPatrolListView = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.AreaNoColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SequenceNumberColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SeqPresetColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.PanColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.TiltColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.FastMoveColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.AutoFocusColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.AutoFocusWaitTimeColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.AreaSelectComboBox = new System.Windows.Forms.ComboBox();
            this.SeqNoNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.PatrolPresetNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.PatrolAddButton = new System.Windows.Forms.Button();
            this.TestMoveButton = new System.Windows.Forms.Button();
            this.TestSpeedNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.TestStopButton = new System.Windows.Forms.Button();
            this.AutoFocusCheckBox = new System.Windows.Forms.CheckBox();
            this.AutoFocusWaitTimeNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.PresetUpdateButton = new System.Windows.Forms.Button();
            this.PatrolDeleteButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.SeqNoNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PatrolPresetNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TestSpeedNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AutoFocusWaitTimeNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // HighSpeedMoveCheckBox
            // 
            this.HighSpeedMoveCheckBox.AutoSize = true;
            this.HighSpeedMoveCheckBox.BackColor = System.Drawing.SystemColors.Control;
            this.HighSpeedMoveCheckBox.ForeColor = System.Drawing.SystemColors.ControlText;
            this.HighSpeedMoveCheckBox.Location = new System.Drawing.Point(295, 428);
            this.HighSpeedMoveCheckBox.Name = "HighSpeedMoveCheckBox";
            this.HighSpeedMoveCheckBox.Size = new System.Drawing.Size(173, 28);
            this.HighSpeedMoveCheckBox.TabIndex = 10;
            this.HighSpeedMoveCheckBox.Text = "高速移動を実行する";
            this.HighSpeedMoveCheckBox.UseVisualStyleBackColor = false;
            // 
            // PatrolUpdateButton
            // 
            this.PatrolUpdateButton.ForeColor = System.Drawing.Color.Navy;
            this.PatrolUpdateButton.Location = new System.Drawing.Point(455, 459);
            this.PatrolUpdateButton.Name = "PatrolUpdateButton";
            this.PatrolUpdateButton.Size = new System.Drawing.Size(150, 31);
            this.PatrolUpdateButton.TabIndex = 9;
            this.PatrolUpdateButton.Text = "登録情報更新";
            this.PatrolUpdateButton.UseVisualStyleBackColor = true;
            this.PatrolUpdateButton.Click += new System.EventHandler(this.PatrolUpdateButton_Click);
            // 
            // SeqDownButton
            // 
            this.SeqDownButton.Location = new System.Drawing.Point(1094, 205);
            this.SeqDownButton.Name = "SeqDownButton";
            this.SeqDownButton.Size = new System.Drawing.Size(48, 48);
            this.SeqDownButton.TabIndex = 7;
            this.SeqDownButton.Text = "⇓";
            this.SeqDownButton.UseVisualStyleBackColor = true;
            this.SeqDownButton.Click += new System.EventHandler(this.SeqDownButton_Click);
            // 
            // SeqUpButton
            // 
            this.SeqUpButton.Location = new System.Drawing.Point(1094, 56);
            this.SeqUpButton.Name = "SeqUpButton";
            this.SeqUpButton.Size = new System.Drawing.Size(48, 48);
            this.SeqUpButton.TabIndex = 6;
            this.SeqUpButton.Text = "⇑";
            this.SeqUpButton.UseVisualStyleBackColor = true;
            this.SeqUpButton.Click += new System.EventHandler(this.SeqUpButton_Click);
            // 
            // AutoPatrolListView
            // 
            this.AutoPatrolListView.BackColor = System.Drawing.Color.WhiteSmoke;
            this.AutoPatrolListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.AreaNoColumnHeader,
            this.SequenceNumberColumnHeader,
            this.SeqPresetColumnHeader,
            this.PanColumnHeader,
            this.TiltColumnHeader,
            this.FastMoveColumnHeader,
            this.AutoFocusColumnHeader,
            this.AutoFocusWaitTimeColumnHeader});
            this.AutoPatrolListView.ForeColor = System.Drawing.Color.Navy;
            this.AutoPatrolListView.FullRowSelect = true;
            this.AutoPatrolListView.GridLines = true;
            this.AutoPatrolListView.HideSelection = false;
            this.AutoPatrolListView.Location = new System.Drawing.Point(215, 24);
            this.AutoPatrolListView.MultiSelect = false;
            this.AutoPatrolListView.Name = "AutoPatrolListView";
            this.AutoPatrolListView.Size = new System.Drawing.Size(873, 229);
            this.AutoPatrolListView.TabIndex = 1;
            this.AutoPatrolListView.UseCompatibleStateImageBehavior = false;
            this.AutoPatrolListView.View = System.Windows.Forms.View.Details;
            this.AutoPatrolListView.SelectedIndexChanged += new System.EventHandler(this.AutoPatrolListView_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "";
            this.columnHeader1.Width = 8;
            // 
            // AreaNoColumnHeader
            // 
            this.AreaNoColumnHeader.Text = "エリア";
            this.AreaNoColumnHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.AreaNoColumnHeader.Width = 80;
            // 
            // SequenceNumberColumnHeader
            // 
            this.SequenceNumberColumnHeader.Text = "Seq No.";
            this.SequenceNumberColumnHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.SequenceNumberColumnHeader.Width = 80;
            // 
            // SeqPresetColumnHeader
            // 
            this.SeqPresetColumnHeader.Text = "プリセット";
            this.SeqPresetColumnHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.SeqPresetColumnHeader.Width = 100;
            // 
            // PanColumnHeader
            // 
            this.PanColumnHeader.Text = "PAN";
            this.PanColumnHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.PanColumnHeader.Width = 100;
            // 
            // TiltColumnHeader
            // 
            this.TiltColumnHeader.Text = "TILT";
            this.TiltColumnHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TiltColumnHeader.Width = 100;
            // 
            // FastMoveColumnHeader
            // 
            this.FastMoveColumnHeader.Text = "移動速度";
            this.FastMoveColumnHeader.Width = 120;
            // 
            // AutoFocusColumnHeader
            // 
            this.AutoFocusColumnHeader.Text = "オートフォーカス";
            this.AutoFocusColumnHeader.Width = 150;
            // 
            // AutoFocusWaitTimeColumnHeader
            // 
            this.AutoFocusWaitTimeColumnHeader.Text = "待機時間";
            this.AutoFocusWaitTimeColumnHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.AutoFocusWaitTimeColumnHeader.Width = 100;
            // 
            // AreaSelectComboBox
            // 
            this.AreaSelectComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.AreaSelectComboBox.FormattingEnabled = true;
            this.AreaSelectComboBox.Location = new System.Drawing.Point(12, 24);
            this.AreaSelectComboBox.Name = "AreaSelectComboBox";
            this.AreaSelectComboBox.Size = new System.Drawing.Size(188, 32);
            this.AreaSelectComboBox.TabIndex = 11;
            this.AreaSelectComboBox.SelectedIndexChanged += new System.EventHandler(this.AreaSelectComboBox_SelectedIndexChanged);
            // 
            // SeqNoNumericUpDown
            // 
            this.SeqNoNumericUpDown.Location = new System.Drawing.Point(435, 261);
            this.SeqNoNumericUpDown.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.SeqNoNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.SeqNoNumericUpDown.Name = "SeqNoNumericUpDown";
            this.SeqNoNumericUpDown.ReadOnly = true;
            this.SeqNoNumericUpDown.Size = new System.Drawing.Size(82, 31);
            this.SeqNoNumericUpDown.TabIndex = 12;
            this.SeqNoNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.SeqNoNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(291, 264);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(138, 24);
            this.label1.TabIndex = 13;
            this.label1.Text = "シーケンス番号：";
            // 
            // PatrolPresetNumericUpDown
            // 
            this.PatrolPresetNumericUpDown.Location = new System.Drawing.Point(435, 299);
            this.PatrolPresetNumericUpDown.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.PatrolPresetNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.PatrolPresetNumericUpDown.Name = "PatrolPresetNumericUpDown";
            this.PatrolPresetNumericUpDown.Size = new System.Drawing.Size(82, 31);
            this.PatrolPresetNumericUpDown.TabIndex = 14;
            this.PatrolPresetNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.PatrolPresetNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(291, 305);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(138, 24);
            this.label2.TabIndex = 15;
            this.label2.Text = "プリセット番号：";
            // 
            // PatrolAddButton
            // 
            this.PatrolAddButton.ForeColor = System.Drawing.Color.Navy;
            this.PatrolAddButton.Location = new System.Drawing.Point(295, 459);
            this.PatrolAddButton.Name = "PatrolAddButton";
            this.PatrolAddButton.Size = new System.Drawing.Size(150, 31);
            this.PatrolAddButton.TabIndex = 19;
            this.PatrolAddButton.Text = "新規";
            this.PatrolAddButton.UseVisualStyleBackColor = true;
            this.PatrolAddButton.Click += new System.EventHandler(this.PatrolAddButton_Click);
            // 
            // TestMoveButton
            // 
            this.TestMoveButton.ForeColor = System.Drawing.Color.Navy;
            this.TestMoveButton.Location = new System.Drawing.Point(972, 261);
            this.TestMoveButton.Name = "TestMoveButton";
            this.TestMoveButton.Size = new System.Drawing.Size(116, 31);
            this.TestMoveButton.TabIndex = 20;
            this.TestMoveButton.Text = "移動テスト";
            this.TestMoveButton.UseVisualStyleBackColor = true;
            this.TestMoveButton.Click += new System.EventHandler(this.TestMoveButton_Click);
            // 
            // TestSpeedNumericUpDown
            // 
            this.TestSpeedNumericUpDown.Location = new System.Drawing.Point(920, 261);
            this.TestSpeedNumericUpDown.Maximum = new decimal(new int[] {
            63,
            0,
            0,
            0});
            this.TestSpeedNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.TestSpeedNumericUpDown.Name = "TestSpeedNumericUpDown";
            this.TestSpeedNumericUpDown.Size = new System.Drawing.Size(46, 31);
            this.TestSpeedNumericUpDown.TabIndex = 21;
            this.TestSpeedNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TestSpeedNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(872, 264);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 24);
            this.label5.TabIndex = 22;
            this.label5.Text = "速度";
            // 
            // TestStopButton
            // 
            this.TestStopButton.ForeColor = System.Drawing.Color.Navy;
            this.TestStopButton.Location = new System.Drawing.Point(972, 299);
            this.TestStopButton.Name = "TestStopButton";
            this.TestStopButton.Size = new System.Drawing.Size(116, 31);
            this.TestStopButton.TabIndex = 23;
            this.TestStopButton.Text = "停止";
            this.TestStopButton.UseVisualStyleBackColor = true;
            this.TestStopButton.Click += new System.EventHandler(this.TestStopButton_Click);
            // 
            // AutoFocusCheckBox
            // 
            this.AutoFocusCheckBox.AutoSize = true;
            this.AutoFocusCheckBox.BackColor = System.Drawing.SystemColors.Control;
            this.AutoFocusCheckBox.ForeColor = System.Drawing.SystemColors.ControlText;
            this.AutoFocusCheckBox.Location = new System.Drawing.Point(295, 351);
            this.AutoFocusCheckBox.Name = "AutoFocusCheckBox";
            this.AutoFocusCheckBox.Size = new System.Drawing.Size(333, 28);
            this.AutoFocusCheckBox.TabIndex = 25;
            this.AutoFocusCheckBox.Text = "ワンショットオートフォーカスを実行する";
            this.AutoFocusCheckBox.UseVisualStyleBackColor = false;
            // 
            // AutoFocusWaitTimeNumericUpDown
            // 
            this.AutoFocusWaitTimeNumericUpDown.Location = new System.Drawing.Point(435, 380);
            this.AutoFocusWaitTimeNumericUpDown.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.AutoFocusWaitTimeNumericUpDown.Name = "AutoFocusWaitTimeNumericUpDown";
            this.AutoFocusWaitTimeNumericUpDown.Size = new System.Drawing.Size(82, 31);
            this.AutoFocusWaitTimeNumericUpDown.TabIndex = 26;
            this.AutoFocusWaitTimeNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(211, 382);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(218, 24);
            this.label3.TabIndex = 27;
            this.label3.Text = "オートフォーカス待機時間：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(523, 382);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(26, 24);
            this.label4.TabIndex = 28;
            this.label4.Text = "秒";
            // 
            // PresetUpdateButton
            // 
            this.PresetUpdateButton.ForeColor = System.Drawing.Color.Navy;
            this.PresetUpdateButton.Location = new System.Drawing.Point(615, 459);
            this.PresetUpdateButton.Name = "PresetUpdateButton";
            this.PresetUpdateButton.Size = new System.Drawing.Size(150, 31);
            this.PresetUpdateButton.TabIndex = 29;
            this.PresetUpdateButton.Text = "位置情報更新";
            this.PresetUpdateButton.UseVisualStyleBackColor = true;
            this.PresetUpdateButton.Click += new System.EventHandler(this.PresetUpdateButton_Click);
            // 
            // PatrolDeleteButton
            // 
            this.PatrolDeleteButton.ForeColor = System.Drawing.Color.Navy;
            this.PatrolDeleteButton.Location = new System.Drawing.Point(775, 459);
            this.PatrolDeleteButton.Name = "PatrolDeleteButton";
            this.PatrolDeleteButton.Size = new System.Drawing.Size(150, 31);
            this.PatrolDeleteButton.TabIndex = 30;
            this.PatrolDeleteButton.Text = "削除";
            this.PatrolDeleteButton.UseVisualStyleBackColor = true;
            this.PatrolDeleteButton.Click += new System.EventHandler(this.PatrolDeleteButton_Click);
            // 
            // SequenceSetupForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1155, 502);
            this.Controls.Add(this.PatrolDeleteButton);
            this.Controls.Add(this.PresetUpdateButton);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.AutoFocusWaitTimeNumericUpDown);
            this.Controls.Add(this.AutoFocusCheckBox);
            this.Controls.Add(this.TestStopButton);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.TestSpeedNumericUpDown);
            this.Controls.Add(this.TestMoveButton);
            this.Controls.Add(this.PatrolAddButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.PatrolPresetNumericUpDown);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.SeqNoNumericUpDown);
            this.Controls.Add(this.AreaSelectComboBox);
            this.Controls.Add(this.SeqDownButton);
            this.Controls.Add(this.PatrolUpdateButton);
            this.Controls.Add(this.SeqUpButton);
            this.Controls.Add(this.HighSpeedMoveCheckBox);
            this.Controls.Add(this.AutoPatrolListView);
            this.Font = new System.Drawing.Font("メイリオ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Name = "SequenceSetupForm";
            this.Text = "自動巡回設定";
            this.Load += new System.EventHandler(this.SequenceSetupForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.SeqNoNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PatrolPresetNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TestSpeedNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AutoFocusWaitTimeNumericUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.CheckBox HighSpeedMoveCheckBox;
        private System.Windows.Forms.Button PatrolUpdateButton;
        private System.Windows.Forms.Button SeqDownButton;
        private System.Windows.Forms.Button SeqUpButton;
        private System.Windows.Forms.ListView AutoPatrolListView;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader SequenceNumberColumnHeader;
        private System.Windows.Forms.ColumnHeader SeqPresetColumnHeader;
        private System.Windows.Forms.ColumnHeader FastMoveColumnHeader;
        private System.Windows.Forms.ComboBox AreaSelectComboBox;
        private System.Windows.Forms.ColumnHeader AutoFocusColumnHeader;
        private System.Windows.Forms.ColumnHeader AreaNoColumnHeader;
        private System.Windows.Forms.NumericUpDown SeqNoNumericUpDown;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown PatrolPresetNumericUpDown;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button PatrolAddButton;
        private System.Windows.Forms.Button TestMoveButton;
        private System.Windows.Forms.NumericUpDown TestSpeedNumericUpDown;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button TestStopButton;
        private System.Windows.Forms.CheckBox AutoFocusCheckBox;
        private System.Windows.Forms.ColumnHeader PanColumnHeader;
        private System.Windows.Forms.ColumnHeader TiltColumnHeader;
        private System.Windows.Forms.ColumnHeader AutoFocusWaitTimeColumnHeader;
        private System.Windows.Forms.NumericUpDown AutoFocusWaitTimeNumericUpDown;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button PresetUpdateButton;
        private System.Windows.Forms.Button PatrolDeleteButton;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CES.ThermalPTZ
{
    using static Properties.Settings;

    public partial class ConfigSetupForm : Form
    {
        readonly IOBox.IOSetter _IOSetter = null;

        public ConfigSetupForm()
        {
            InitializeComponent();
            _IOSetter = null;
        }

        public ConfigSetupForm( IOBox.IOSetter Setter ):this()
        {
            _IOSetter = Setter;
        }

        private void ConfigSetupForm_Load( object sender, EventArgs e )
        {
            LoadConfig();
        }


        private void LoadConfig()
        {
            TemperatureThresholdNumericUpDown.Value = Convert.ToDecimal( Default.TemperatureThreshold );
            AreaChangeWaitNumericUpDown.Value = Convert.ToDecimal( Default.AreaChangeWaitTime );
            MoveSpeedLowNumericUpDown.Value = Convert.ToDecimal( Default.PatrolPresetSpeed );
            MoveSpeedHighNumericUpDown.Value = Convert.ToDecimal( Default.AreaChangePresetSpeed );
            EnableAlarmBuzzerCheckBox.Checked = Default.AlarmBuzzerEnable;
            EnablePositionTweakCheckBox.Checked = Default.PositionTweak;
            TemperatureWaitNumericUpDown.Value = Convert.ToDecimal( Default.TemperatureWaitTime );
            AutoPatrolIntervalNumericUpDown.Value = Convert.ToDecimal( Default.AutoPatrolInterval );
            AlarmCountNumericUpDown.Value = Convert.ToDecimal( Default.AlarmCount );
        }

        private void UpdateConfig()
        {
            Default.TemperatureThreshold = Convert.ToSingle( TemperatureThresholdNumericUpDown.Value );
            Default.AreaChangeWaitTime = Convert.ToDouble( AreaChangeWaitNumericUpDown.Value );
            Default.PatrolPresetSpeed = Convert.ToByte( MoveSpeedLowNumericUpDown.Value );
            Default.AreaChangePresetSpeed = Convert.ToByte( MoveSpeedHighNumericUpDown.Value );
            Default.AlarmBuzzerEnable = EnableAlarmBuzzerCheckBox.Checked;
            Default.PositionTweak = EnablePositionTweakCheckBox.Checked;
            Default.TemperatureWaitTime = Convert.ToDouble( TemperatureWaitNumericUpDown.Value );
            Default.AutoPatrolInterval = Convert.ToInt32( AutoPatrolIntervalNumericUpDown.Value );
            Default.AlarmCount = Convert.ToInt32( AlarmCountNumericUpDown.Value );
            Default.Save();
        }

        private void AlarmTestButton_Click( object sender, EventArgs e )
        {
            _IOSetter?.AlarmOn( EnableAlarmBuzzerCheckBox.Checked );
        }

        private void ConfigUpdateButton_Click( object sender, EventArgs e )
        {
            UpdateConfig();
        }
    }
}

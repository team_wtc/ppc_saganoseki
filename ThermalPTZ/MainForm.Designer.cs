﻿namespace CES.ThermalPTZ
{
    partial class MainForm
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.LiveImagePanel = new System.Windows.Forms.Panel();
            this.MainTitleLabel = new System.Windows.Forms.Label();
            this.AutoFocusButton = new System.Windows.Forms.Button();
            this.MainTimer = new System.Windows.Forms.Timer(this.components);
            this.ClockLabel = new System.Windows.Forms.Label();
            this.MainPanel = new System.Windows.Forms.Panel();
            this.PTZCntlPanel = new System.Windows.Forms.Panel();
            this.HomePositionSetButton = new System.Windows.Forms.Button();
            this.HomePositionMoveButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SpeedRadioButton5 = new System.Windows.Forms.RadioButton();
            this.SpeedRadioButton1 = new System.Windows.Forms.RadioButton();
            this.SpeedRadioButton4 = new System.Windows.Forms.RadioButton();
            this.SpeedRadioButton2 = new System.Windows.Forms.RadioButton();
            this.SpeedRadioButton3 = new System.Windows.Forms.RadioButton();
            this.PTZButtonPanel = new System.Windows.Forms.Panel();
            this.PTHomeButton = new System.Windows.Forms.Button();
            this.PanTiltPositionLabel = new System.Windows.Forms.Label();
            this.AutoPatrolCheckBox = new System.Windows.Forms.CheckBox();
            this.ConfigGroupBox = new System.Windows.Forms.GroupBox();
            this.AreaRectSetupRadioButton = new System.Windows.Forms.RadioButton();
            this.HistoryViewRadioButton = new System.Windows.Forms.RadioButton();
            this.AreaSetupRadioButton = new System.Windows.Forms.RadioButton();
            this.CameraSetupRadioButton = new System.Windows.Forms.RadioButton();
            this.PresetRadioButton = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.PanTiltTargetLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.PatrolStatusLabel = new System.Windows.Forms.Label();
            this.MaxTempLabel = new System.Windows.Forms.Label();
            this.MaxPosLabel = new System.Windows.Forms.Label();
            this.BaseSplitContainer = new System.Windows.Forms.SplitContainer();
            this.FocusNearButton = new System.Windows.Forms.Button();
            this.FocusFarButton = new System.Windows.Forms.Button();
            this.SnapshotButton = new System.Windows.Forms.Button();
            this.LiveImagePictureBox = new System.Windows.Forms.PictureBox();
            this.PTLeftUpButton = new System.Windows.Forms.Button();
            this.PTUpButton = new System.Windows.Forms.Button();
            this.PTRightUpButton = new System.Windows.Forms.Button();
            this.PTLeftButton = new System.Windows.Forms.Button();
            this.PTRightButton = new System.Windows.Forms.Button();
            this.PTLeftDownButton = new System.Windows.Forms.Button();
            this.PTDownButton = new System.Windows.Forms.Button();
            this.PTRightDownButton = new System.Windows.Forms.Button();
            this.LayoutPictureBox = new System.Windows.Forms.PictureBox();
            this.LiveImagePanel.SuspendLayout();
            this.MainPanel.SuspendLayout();
            this.PTZCntlPanel.SuspendLayout();
            this.PTZButtonPanel.SuspendLayout();
            this.ConfigGroupBox.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BaseSplitContainer)).BeginInit();
            this.BaseSplitContainer.Panel1.SuspendLayout();
            this.BaseSplitContainer.Panel2.SuspendLayout();
            this.BaseSplitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LiveImagePictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // LiveImagePanel
            // 
            this.LiveImagePanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LiveImagePanel.Controls.Add(this.MainTitleLabel);
            this.LiveImagePanel.Controls.Add(this.LiveImagePictureBox);
            this.LiveImagePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LiveImagePanel.Location = new System.Drawing.Point(0, 0);
            this.LiveImagePanel.Name = "LiveImagePanel";
            this.LiveImagePanel.Size = new System.Drawing.Size(714, 1061);
            this.LiveImagePanel.TabIndex = 4;
            // 
            // MainTitleLabel
            // 
            this.MainTitleLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MainTitleLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.MainTitleLabel.Font = new System.Drawing.Font("メイリオ", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.MainTitleLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.MainTitleLabel.Location = new System.Drawing.Point(0, -2);
            this.MainTitleLabel.Name = "MainTitleLabel";
            this.MainTitleLabel.Size = new System.Drawing.Size(712, 60);
            this.MainTitleLabel.TabIndex = 10;
            this.MainTitleLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // AutoFocusButton
            // 
            this.AutoFocusButton.BackColor = System.Drawing.Color.WhiteSmoke;
            this.AutoFocusButton.Font = new System.Drawing.Font("メイリオ", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.AutoFocusButton.ForeColor = System.Drawing.Color.Navy;
            this.AutoFocusButton.Location = new System.Drawing.Point(14, 12);
            this.AutoFocusButton.Name = "AutoFocusButton";
            this.AutoFocusButton.Size = new System.Drawing.Size(160, 33);
            this.AutoFocusButton.TabIndex = 9;
            this.AutoFocusButton.Text = "オートフォーカス";
            this.AutoFocusButton.UseVisualStyleBackColor = false;
            this.AutoFocusButton.Click += new System.EventHandler(this.AutoFocusButton_Click);
            // 
            // MainTimer
            // 
            this.MainTimer.Interval = 250;
            this.MainTimer.Tick += new System.EventHandler(this.MainTimer_Tick);
            // 
            // ClockLabel
            // 
            this.ClockLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ClockLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ClockLabel.Font = new System.Drawing.Font("メイリオ", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ClockLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ClockLabel.Location = new System.Drawing.Point(0, 0);
            this.ClockLabel.Name = "ClockLabel";
            this.ClockLabel.Size = new System.Drawing.Size(522, 60);
            this.ClockLabel.TabIndex = 9;
            this.ClockLabel.Text = "yyyy年mm月dd日(ddd) HH:mm:ss";
            this.ClockLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // MainPanel
            // 
            this.MainPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MainPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.MainPanel.Controls.Add(this.PTZCntlPanel);
            this.MainPanel.Controls.Add(this.PTZButtonPanel);
            this.MainPanel.Controls.Add(this.LayoutPictureBox);
            this.MainPanel.Location = new System.Drawing.Point(0, 117);
            this.MainPanel.Name = "MainPanel";
            this.MainPanel.Size = new System.Drawing.Size(522, 540);
            this.MainPanel.TabIndex = 10;
            // 
            // PTZCntlPanel
            // 
            this.PTZCntlPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.PTZCntlPanel.Controls.Add(this.SnapshotButton);
            this.PTZCntlPanel.Controls.Add(this.FocusFarButton);
            this.PTZCntlPanel.Controls.Add(this.FocusNearButton);
            this.PTZCntlPanel.Controls.Add(this.HomePositionSetButton);
            this.PTZCntlPanel.Controls.Add(this.AutoFocusButton);
            this.PTZCntlPanel.Controls.Add(this.HomePositionMoveButton);
            this.PTZCntlPanel.Controls.Add(this.label1);
            this.PTZCntlPanel.Controls.Add(this.SpeedRadioButton5);
            this.PTZCntlPanel.Controls.Add(this.SpeedRadioButton1);
            this.PTZCntlPanel.Controls.Add(this.SpeedRadioButton4);
            this.PTZCntlPanel.Controls.Add(this.SpeedRadioButton2);
            this.PTZCntlPanel.Controls.Add(this.SpeedRadioButton3);
            this.PTZCntlPanel.Location = new System.Drawing.Point(3, 366);
            this.PTZCntlPanel.Name = "PTZCntlPanel";
            this.PTZCntlPanel.Size = new System.Drawing.Size(346, 171);
            this.PTZCntlPanel.TabIndex = 17;
            // 
            // HomePositionSetButton
            // 
            this.HomePositionSetButton.BackColor = System.Drawing.Color.WhiteSmoke;
            this.HomePositionSetButton.Font = new System.Drawing.Font("メイリオ", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.HomePositionSetButton.ForeColor = System.Drawing.Color.Navy;
            this.HomePositionSetButton.Location = new System.Drawing.Point(14, 98);
            this.HomePositionSetButton.Name = "HomePositionSetButton";
            this.HomePositionSetButton.Size = new System.Drawing.Size(160, 33);
            this.HomePositionSetButton.TabIndex = 13;
            this.HomePositionSetButton.Text = "ホームを設定";
            this.HomePositionSetButton.UseVisualStyleBackColor = false;
            this.HomePositionSetButton.Click += new System.EventHandler(this.HomePositionSetButton_Click);
            // 
            // HomePositionMoveButton
            // 
            this.HomePositionMoveButton.BackColor = System.Drawing.Color.WhiteSmoke;
            this.HomePositionMoveButton.Font = new System.Drawing.Font("メイリオ", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.HomePositionMoveButton.ForeColor = System.Drawing.Color.Navy;
            this.HomePositionMoveButton.Location = new System.Drawing.Point(179, 98);
            this.HomePositionMoveButton.Name = "HomePositionMoveButton";
            this.HomePositionMoveButton.Size = new System.Drawing.Size(160, 33);
            this.HomePositionMoveButton.TabIndex = 12;
            this.HomePositionMoveButton.Text = "ホームへ移動";
            this.HomePositionMoveButton.UseVisualStyleBackColor = false;
            this.HomePositionMoveButton.Click += new System.EventHandler(this.HomePositionMoveButton_Click);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("メイリオ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label1.Location = new System.Drawing.Point(10, 135);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 33);
            this.label1.TabIndex = 15;
            this.label1.Text = "速度：";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // SpeedRadioButton5
            // 
            this.SpeedRadioButton5.Appearance = System.Windows.Forms.Appearance.Button;
            this.SpeedRadioButton5.Font = new System.Drawing.Font("メイリオ", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.SpeedRadioButton5.ForeColor = System.Drawing.Color.Navy;
            this.SpeedRadioButton5.Location = new System.Drawing.Point(284, 137);
            this.SpeedRadioButton5.Name = "SpeedRadioButton5";
            this.SpeedRadioButton5.Size = new System.Drawing.Size(55, 28);
            this.SpeedRadioButton5.TabIndex = 14;
            this.SpeedRadioButton5.Text = "最高";
            this.SpeedRadioButton5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.SpeedRadioButton5.UseVisualStyleBackColor = true;
            // 
            // SpeedRadioButton1
            // 
            this.SpeedRadioButton1.Appearance = System.Windows.Forms.Appearance.Button;
            this.SpeedRadioButton1.Font = new System.Drawing.Font("メイリオ", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.SpeedRadioButton1.ForeColor = System.Drawing.Color.Navy;
            this.SpeedRadioButton1.Location = new System.Drawing.Point(73, 137);
            this.SpeedRadioButton1.Name = "SpeedRadioButton1";
            this.SpeedRadioButton1.Size = new System.Drawing.Size(55, 28);
            this.SpeedRadioButton1.TabIndex = 10;
            this.SpeedRadioButton1.Text = "最低";
            this.SpeedRadioButton1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.SpeedRadioButton1.UseVisualStyleBackColor = true;
            // 
            // SpeedRadioButton4
            // 
            this.SpeedRadioButton4.Appearance = System.Windows.Forms.Appearance.Button;
            this.SpeedRadioButton4.Font = new System.Drawing.Font("メイリオ", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.SpeedRadioButton4.ForeColor = System.Drawing.Color.Navy;
            this.SpeedRadioButton4.Location = new System.Drawing.Point(234, 137);
            this.SpeedRadioButton4.Name = "SpeedRadioButton4";
            this.SpeedRadioButton4.Size = new System.Drawing.Size(44, 28);
            this.SpeedRadioButton4.TabIndex = 13;
            this.SpeedRadioButton4.Text = "高";
            this.SpeedRadioButton4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.SpeedRadioButton4.UseVisualStyleBackColor = true;
            // 
            // SpeedRadioButton2
            // 
            this.SpeedRadioButton2.Appearance = System.Windows.Forms.Appearance.Button;
            this.SpeedRadioButton2.Font = new System.Drawing.Font("メイリオ", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.SpeedRadioButton2.ForeColor = System.Drawing.Color.Navy;
            this.SpeedRadioButton2.Location = new System.Drawing.Point(134, 137);
            this.SpeedRadioButton2.Name = "SpeedRadioButton2";
            this.SpeedRadioButton2.Size = new System.Drawing.Size(44, 28);
            this.SpeedRadioButton2.TabIndex = 11;
            this.SpeedRadioButton2.Text = "低";
            this.SpeedRadioButton2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.SpeedRadioButton2.UseVisualStyleBackColor = true;
            // 
            // SpeedRadioButton3
            // 
            this.SpeedRadioButton3.Appearance = System.Windows.Forms.Appearance.Button;
            this.SpeedRadioButton3.Checked = true;
            this.SpeedRadioButton3.Font = new System.Drawing.Font("メイリオ", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.SpeedRadioButton3.ForeColor = System.Drawing.Color.Navy;
            this.SpeedRadioButton3.Location = new System.Drawing.Point(184, 137);
            this.SpeedRadioButton3.Name = "SpeedRadioButton3";
            this.SpeedRadioButton3.Size = new System.Drawing.Size(44, 28);
            this.SpeedRadioButton3.TabIndex = 12;
            this.SpeedRadioButton3.TabStop = true;
            this.SpeedRadioButton3.Text = "中";
            this.SpeedRadioButton3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.SpeedRadioButton3.UseVisualStyleBackColor = true;
            // 
            // PTZButtonPanel
            // 
            this.PTZButtonPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.PTZButtonPanel.Controls.Add(this.PTLeftUpButton);
            this.PTZButtonPanel.Controls.Add(this.PTUpButton);
            this.PTZButtonPanel.Controls.Add(this.PTRightUpButton);
            this.PTZButtonPanel.Controls.Add(this.PTHomeButton);
            this.PTZButtonPanel.Controls.Add(this.PTLeftButton);
            this.PTZButtonPanel.Controls.Add(this.PTRightButton);
            this.PTZButtonPanel.Controls.Add(this.PTLeftDownButton);
            this.PTZButtonPanel.Controls.Add(this.PTDownButton);
            this.PTZButtonPanel.Controls.Add(this.PTRightDownButton);
            this.PTZButtonPanel.Location = new System.Drawing.Point(348, 366);
            this.PTZButtonPanel.Name = "PTZButtonPanel";
            this.PTZButtonPanel.Size = new System.Drawing.Size(168, 171);
            this.PTZButtonPanel.TabIndex = 16;
            // 
            // PTHomeButton
            // 
            this.PTHomeButton.Font = new System.Drawing.Font("メイリオ", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.PTHomeButton.Location = new System.Drawing.Point(60, 59);
            this.PTHomeButton.Name = "PTHomeButton";
            this.PTHomeButton.Size = new System.Drawing.Size(50, 50);
            this.PTHomeButton.TabIndex = 5;
            this.PTHomeButton.Text = "home";
            this.PTHomeButton.UseVisualStyleBackColor = true;
            this.PTHomeButton.Click += new System.EventHandler(this.PTHomeButton_Click);
            // 
            // PanTiltPositionLabel
            // 
            this.PanTiltPositionLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PanTiltPositionLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanTiltPositionLabel.Font = new System.Drawing.Font("メイリオ", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.PanTiltPositionLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.PanTiltPositionLabel.Location = new System.Drawing.Point(185, 27);
            this.PanTiltPositionLabel.Name = "PanTiltPositionLabel";
            this.PanTiltPositionLabel.Size = new System.Drawing.Size(328, 33);
            this.PanTiltPositionLabel.TabIndex = 11;
            this.PanTiltPositionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // AutoPatrolCheckBox
            // 
            this.AutoPatrolCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AutoPatrolCheckBox.Appearance = System.Windows.Forms.Appearance.Button;
            this.AutoPatrolCheckBox.Font = new System.Drawing.Font("メイリオ", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.AutoPatrolCheckBox.ForeColor = System.Drawing.Color.Navy;
            this.AutoPatrolCheckBox.Location = new System.Drawing.Point(1, 63);
            this.AutoPatrolCheckBox.Name = "AutoPatrolCheckBox";
            this.AutoPatrolCheckBox.Size = new System.Drawing.Size(521, 48);
            this.AutoPatrolCheckBox.TabIndex = 0;
            this.AutoPatrolCheckBox.Text = "自動巡回実行中";
            this.AutoPatrolCheckBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.AutoPatrolCheckBox.UseVisualStyleBackColor = true;
            this.AutoPatrolCheckBox.CheckedChanged += new System.EventHandler(this.AutoPatrolCheckBox_CheckedChanged);
            // 
            // ConfigGroupBox
            // 
            this.ConfigGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ConfigGroupBox.Controls.Add(this.AreaRectSetupRadioButton);
            this.ConfigGroupBox.Controls.Add(this.HistoryViewRadioButton);
            this.ConfigGroupBox.Controls.Add(this.AreaSetupRadioButton);
            this.ConfigGroupBox.Controls.Add(this.CameraSetupRadioButton);
            this.ConfigGroupBox.Controls.Add(this.PresetRadioButton);
            this.ConfigGroupBox.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.ConfigGroupBox.Location = new System.Drawing.Point(1, 929);
            this.ConfigGroupBox.Name = "ConfigGroupBox";
            this.ConfigGroupBox.Size = new System.Drawing.Size(524, 131);
            this.ConfigGroupBox.TabIndex = 4;
            this.ConfigGroupBox.TabStop = false;
            this.ConfigGroupBox.Text = "設定";
            this.ConfigGroupBox.Resize += new System.EventHandler(this.ConfigGroupBox_Resize);
            // 
            // AreaRectSetupRadioButton
            // 
            this.AreaRectSetupRadioButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.AreaRectSetupRadioButton.Font = new System.Drawing.Font("メイリオ", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.AreaRectSetupRadioButton.ForeColor = System.Drawing.Color.Navy;
            this.AreaRectSetupRadioButton.Location = new System.Drawing.Point(168, 30);
            this.AreaRectSetupRadioButton.Name = "AreaRectSetupRadioButton";
            this.AreaRectSetupRadioButton.Size = new System.Drawing.Size(156, 43);
            this.AreaRectSetupRadioButton.TabIndex = 5;
            this.AreaRectSetupRadioButton.TabStop = true;
            this.AreaRectSetupRadioButton.Text = "エリア領域設定";
            this.AreaRectSetupRadioButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.AreaRectSetupRadioButton.UseVisualStyleBackColor = true;
            this.AreaRectSetupRadioButton.CheckedChanged += new System.EventHandler(this.AreaRectSetupRadioButton_CheckedChanged);
            // 
            // HistoryViewRadioButton
            // 
            this.HistoryViewRadioButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.HistoryViewRadioButton.Font = new System.Drawing.Font("メイリオ", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.HistoryViewRadioButton.ForeColor = System.Drawing.Color.Navy;
            this.HistoryViewRadioButton.Location = new System.Drawing.Point(6, 79);
            this.HistoryViewRadioButton.Name = "HistoryViewRadioButton";
            this.HistoryViewRadioButton.Size = new System.Drawing.Size(250, 43);
            this.HistoryViewRadioButton.TabIndex = 4;
            this.HistoryViewRadioButton.TabStop = true;
            this.HistoryViewRadioButton.Text = "履歴参照";
            this.HistoryViewRadioButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.HistoryViewRadioButton.UseVisualStyleBackColor = true;
            this.HistoryViewRadioButton.CheckedChanged += new System.EventHandler(this.HistoryViewRadioButton_CheckedChanged);
            // 
            // AreaSetupRadioButton
            // 
            this.AreaSetupRadioButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.AreaSetupRadioButton.Font = new System.Drawing.Font("メイリオ", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.AreaSetupRadioButton.ForeColor = System.Drawing.Color.Navy;
            this.AreaSetupRadioButton.Location = new System.Drawing.Point(6, 30);
            this.AreaSetupRadioButton.Name = "AreaSetupRadioButton";
            this.AreaSetupRadioButton.Size = new System.Drawing.Size(156, 43);
            this.AreaSetupRadioButton.TabIndex = 3;
            this.AreaSetupRadioButton.TabStop = true;
            this.AreaSetupRadioButton.Text = "監視エリア設定";
            this.AreaSetupRadioButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.AreaSetupRadioButton.UseVisualStyleBackColor = true;
            this.AreaSetupRadioButton.CheckedChanged += new System.EventHandler(this.AreaSetupRadioButton_CheckedChanged);
            // 
            // CameraSetupRadioButton
            // 
            this.CameraSetupRadioButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.CameraSetupRadioButton.Font = new System.Drawing.Font("メイリオ", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.CameraSetupRadioButton.ForeColor = System.Drawing.Color.Navy;
            this.CameraSetupRadioButton.Location = new System.Drawing.Point(262, 79);
            this.CameraSetupRadioButton.Name = "CameraSetupRadioButton";
            this.CameraSetupRadioButton.Size = new System.Drawing.Size(250, 43);
            this.CameraSetupRadioButton.TabIndex = 2;
            this.CameraSetupRadioButton.TabStop = true;
            this.CameraSetupRadioButton.Text = "巡回動作設定";
            this.CameraSetupRadioButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.CameraSetupRadioButton.UseVisualStyleBackColor = true;
            this.CameraSetupRadioButton.CheckedChanged += new System.EventHandler(this.CameraSetupRadioButton_CheckedChanged);
            // 
            // PresetRadioButton
            // 
            this.PresetRadioButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.PresetRadioButton.Font = new System.Drawing.Font("メイリオ", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.PresetRadioButton.ForeColor = System.Drawing.Color.Navy;
            this.PresetRadioButton.Location = new System.Drawing.Point(371, 30);
            this.PresetRadioButton.Name = "PresetRadioButton";
            this.PresetRadioButton.Size = new System.Drawing.Size(141, 43);
            this.PresetRadioButton.TabIndex = 0;
            this.PresetRadioButton.TabStop = true;
            this.PresetRadioButton.Text = "プリセット設定";
            this.PresetRadioButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.PresetRadioButton.UseVisualStyleBackColor = true;
            this.PresetRadioButton.CheckedChanged += new System.EventHandler(this.PresetRadioButton_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.PanTiltTargetLabel);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.PatrolStatusLabel);
            this.groupBox1.Controls.Add(this.PanTiltPositionLabel);
            this.groupBox1.Controls.Add(this.MaxTempLabel);
            this.groupBox1.Controls.Add(this.MaxPosLabel);
            this.groupBox1.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox1.Location = new System.Drawing.Point(0, 730);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(524, 193);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "位置・温度情報";
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("メイリオ", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label5.Location = new System.Drawing.Point(9, 27);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(170, 33);
            this.label5.TabIndex = 15;
            this.label5.Text = "Pan/Tilt 現在位置：";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("メイリオ", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label4.Location = new System.Drawing.Point(9, 69);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(170, 33);
            this.label4.TabIndex = 14;
            this.label4.Text = "Pan/Tilt 目標位置：";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // PanTiltTargetLabel
            // 
            this.PanTiltTargetLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PanTiltTargetLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanTiltTargetLabel.Font = new System.Drawing.Font("メイリオ", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.PanTiltTargetLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.PanTiltTargetLabel.Location = new System.Drawing.Point(185, 69);
            this.PanTiltTargetLabel.Name = "PanTiltTargetLabel";
            this.PanTiltTargetLabel.Size = new System.Drawing.Size(328, 33);
            this.PanTiltTargetLabel.TabIndex = 13;
            this.PanTiltTargetLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("メイリオ", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label2.Location = new System.Drawing.Point(9, 111);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(170, 33);
            this.label2.TabIndex = 12;
            this.label2.Text = "画角内最高温度情報：";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // PatrolStatusLabel
            // 
            this.PatrolStatusLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PatrolStatusLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PatrolStatusLabel.Font = new System.Drawing.Font("メイリオ", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.PatrolStatusLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.PatrolStatusLabel.Location = new System.Drawing.Point(13, 153);
            this.PatrolStatusLabel.Name = "PatrolStatusLabel";
            this.PatrolStatusLabel.Size = new System.Drawing.Size(500, 33);
            this.PatrolStatusLabel.TabIndex = 11;
            this.PatrolStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // MaxTempLabel
            // 
            this.MaxTempLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MaxTempLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.MaxTempLabel.Font = new System.Drawing.Font("メイリオ", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.MaxTempLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.MaxTempLabel.Location = new System.Drawing.Point(426, 111);
            this.MaxTempLabel.Name = "MaxTempLabel";
            this.MaxTempLabel.Size = new System.Drawing.Size(87, 33);
            this.MaxTempLabel.TabIndex = 7;
            this.MaxTempLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // MaxPosLabel
            // 
            this.MaxPosLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.MaxPosLabel.Font = new System.Drawing.Font("メイリオ", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.MaxPosLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.MaxPosLabel.Location = new System.Drawing.Point(185, 111);
            this.MaxPosLabel.Name = "MaxPosLabel";
            this.MaxPosLabel.Size = new System.Drawing.Size(235, 33);
            this.MaxPosLabel.TabIndex = 5;
            this.MaxPosLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // BaseSplitContainer
            // 
            this.BaseSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BaseSplitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.BaseSplitContainer.IsSplitterFixed = true;
            this.BaseSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.BaseSplitContainer.Name = "BaseSplitContainer";
            // 
            // BaseSplitContainer.Panel1
            // 
            this.BaseSplitContainer.Panel1.Controls.Add(this.LiveImagePanel);
            // 
            // BaseSplitContainer.Panel2
            // 
            this.BaseSplitContainer.Panel2.Controls.Add(this.ConfigGroupBox);
            this.BaseSplitContainer.Panel2.Controls.Add(this.ClockLabel);
            this.BaseSplitContainer.Panel2.Controls.Add(this.AutoPatrolCheckBox);
            this.BaseSplitContainer.Panel2.Controls.Add(this.groupBox1);
            this.BaseSplitContainer.Panel2.Controls.Add(this.MainPanel);
            this.BaseSplitContainer.Size = new System.Drawing.Size(1243, 1061);
            this.BaseSplitContainer.SplitterDistance = 714;
            this.BaseSplitContainer.TabIndex = 13;
            // 
            // FocusNearButton
            // 
            this.FocusNearButton.BackColor = System.Drawing.Color.WhiteSmoke;
            this.FocusNearButton.Font = new System.Drawing.Font("メイリオ", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.FocusNearButton.ForeColor = System.Drawing.Color.Navy;
            this.FocusNearButton.Location = new System.Drawing.Point(180, 12);
            this.FocusNearButton.Name = "FocusNearButton";
            this.FocusNearButton.Size = new System.Drawing.Size(76, 33);
            this.FocusNearButton.TabIndex = 16;
            this.FocusNearButton.Text = "Near";
            this.FocusNearButton.UseVisualStyleBackColor = false;
            this.FocusNearButton.Click += new System.EventHandler(this.FocusNearButton_Click);
            this.FocusNearButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FocusNearButton_MouseDown);
            this.FocusNearButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.FocusNearButton_MouseUp);
            // 
            // FocusFarButton
            // 
            this.FocusFarButton.BackColor = System.Drawing.Color.WhiteSmoke;
            this.FocusFarButton.Font = new System.Drawing.Font("メイリオ", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.FocusFarButton.ForeColor = System.Drawing.Color.Navy;
            this.FocusFarButton.Location = new System.Drawing.Point(263, 12);
            this.FocusFarButton.Name = "FocusFarButton";
            this.FocusFarButton.Size = new System.Drawing.Size(76, 33);
            this.FocusFarButton.TabIndex = 17;
            this.FocusFarButton.Text = "Far";
            this.FocusFarButton.UseVisualStyleBackColor = false;
            this.FocusFarButton.Click += new System.EventHandler(this.FocusFarButton_Click);
            this.FocusFarButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FocusFarButton_MouseDown);
            this.FocusFarButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.FocusFarButton_MouseUp);
            // 
            // SnapshotButton
            // 
            this.SnapshotButton.BackColor = System.Drawing.Color.WhiteSmoke;
            this.SnapshotButton.Font = new System.Drawing.Font("メイリオ", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.SnapshotButton.ForeColor = System.Drawing.Color.Navy;
            this.SnapshotButton.Location = new System.Drawing.Point(14, 51);
            this.SnapshotButton.Name = "SnapshotButton";
            this.SnapshotButton.Size = new System.Drawing.Size(160, 33);
            this.SnapshotButton.TabIndex = 18;
            this.SnapshotButton.Text = "スナップショット";
            this.SnapshotButton.UseVisualStyleBackColor = false;
            this.SnapshotButton.Click += new System.EventHandler(this.SnapshotButton_Click);
            // 
            // LiveImagePictureBox
            // 
            this.LiveImagePictureBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LiveImagePictureBox.BackColor = System.Drawing.Color.Black;
            this.LiveImagePictureBox.Location = new System.Drawing.Point(1, 59);
            this.LiveImagePictureBox.Name = "LiveImagePictureBox";
            this.LiveImagePictureBox.Size = new System.Drawing.Size(708, 997);
            this.LiveImagePictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.LiveImagePictureBox.TabIndex = 0;
            this.LiveImagePictureBox.TabStop = false;
            this.LiveImagePictureBox.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.LiveImagePictureBox_MouseDoubleClick);
            this.LiveImagePictureBox.Resize += new System.EventHandler(this.LiveImagePictureBox_Resize);
            // 
            // PTLeftUpButton
            // 
            this.PTLeftUpButton.Image = global::CES.ThermalPTZ.Properties.Resources.left_up;
            this.PTLeftUpButton.Location = new System.Drawing.Point(4, 3);
            this.PTLeftUpButton.Name = "PTLeftUpButton";
            this.PTLeftUpButton.Size = new System.Drawing.Size(50, 50);
            this.PTLeftUpButton.TabIndex = 3;
            this.PTLeftUpButton.UseVisualStyleBackColor = true;
            this.PTLeftUpButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PTLeftUpButton_MouseDown);
            this.PTLeftUpButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PTLeftUpButton_MouseUp);
            // 
            // PTUpButton
            // 
            this.PTUpButton.Image = global::CES.ThermalPTZ.Properties.Resources.up;
            this.PTUpButton.Location = new System.Drawing.Point(60, 3);
            this.PTUpButton.Name = "PTUpButton";
            this.PTUpButton.Size = new System.Drawing.Size(50, 50);
            this.PTUpButton.TabIndex = 1;
            this.PTUpButton.UseVisualStyleBackColor = true;
            this.PTUpButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PTUpButton_MouseDown);
            this.PTUpButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PTUpButton_MouseUp);
            // 
            // PTRightUpButton
            // 
            this.PTRightUpButton.Image = global::CES.ThermalPTZ.Properties.Resources.right_up;
            this.PTRightUpButton.Location = new System.Drawing.Point(116, 3);
            this.PTRightUpButton.Name = "PTRightUpButton";
            this.PTRightUpButton.Size = new System.Drawing.Size(50, 50);
            this.PTRightUpButton.TabIndex = 2;
            this.PTRightUpButton.UseVisualStyleBackColor = true;
            this.PTRightUpButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PTRightUpButton_MouseDown);
            this.PTRightUpButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PTRightUpButton_MouseUp);
            // 
            // PTLeftButton
            // 
            this.PTLeftButton.Image = global::CES.ThermalPTZ.Properties.Resources.left;
            this.PTLeftButton.Location = new System.Drawing.Point(4, 59);
            this.PTLeftButton.Name = "PTLeftButton";
            this.PTLeftButton.Size = new System.Drawing.Size(50, 50);
            this.PTLeftButton.TabIndex = 4;
            this.PTLeftButton.UseVisualStyleBackColor = true;
            this.PTLeftButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PTLeftButton_MouseDown);
            this.PTLeftButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PTLeftButton_MouseUp);
            // 
            // PTRightButton
            // 
            this.PTRightButton.Image = global::CES.ThermalPTZ.Properties.Resources.right;
            this.PTRightButton.Location = new System.Drawing.Point(116, 59);
            this.PTRightButton.Name = "PTRightButton";
            this.PTRightButton.Size = new System.Drawing.Size(50, 50);
            this.PTRightButton.TabIndex = 6;
            this.PTRightButton.UseVisualStyleBackColor = true;
            this.PTRightButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PTRightButton_MouseDown);
            this.PTRightButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PTRightButton_MouseUp);
            // 
            // PTLeftDownButton
            // 
            this.PTLeftDownButton.Image = global::CES.ThermalPTZ.Properties.Resources.left_down;
            this.PTLeftDownButton.Location = new System.Drawing.Point(4, 115);
            this.PTLeftDownButton.Name = "PTLeftDownButton";
            this.PTLeftDownButton.Size = new System.Drawing.Size(50, 50);
            this.PTLeftDownButton.TabIndex = 7;
            this.PTLeftDownButton.UseVisualStyleBackColor = true;
            this.PTLeftDownButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PTLeftDownButton_MouseDown);
            this.PTLeftDownButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PTLeftDownButton_MouseUp);
            // 
            // PTDownButton
            // 
            this.PTDownButton.Image = global::CES.ThermalPTZ.Properties.Resources.down;
            this.PTDownButton.Location = new System.Drawing.Point(60, 115);
            this.PTDownButton.Name = "PTDownButton";
            this.PTDownButton.Size = new System.Drawing.Size(50, 50);
            this.PTDownButton.TabIndex = 8;
            this.PTDownButton.UseVisualStyleBackColor = true;
            this.PTDownButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PTDownButton_MouseDown);
            this.PTDownButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PTDownButton_MouseUp);
            // 
            // PTRightDownButton
            // 
            this.PTRightDownButton.Image = global::CES.ThermalPTZ.Properties.Resources.right_down;
            this.PTRightDownButton.Location = new System.Drawing.Point(116, 115);
            this.PTRightDownButton.Name = "PTRightDownButton";
            this.PTRightDownButton.Size = new System.Drawing.Size(50, 50);
            this.PTRightDownButton.TabIndex = 9;
            this.PTRightDownButton.UseVisualStyleBackColor = true;
            this.PTRightDownButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PTRightDownButton_MouseDown);
            this.PTRightDownButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PTRightDownButton_MouseUp);
            // 
            // LayoutPictureBox
            // 
            this.LayoutPictureBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LayoutPictureBox.Location = new System.Drawing.Point(3, 3);
            this.LayoutPictureBox.Name = "LayoutPictureBox";
            this.LayoutPictureBox.Size = new System.Drawing.Size(512, 359);
            this.LayoutPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.LayoutPictureBox.TabIndex = 0;
            this.LayoutPictureBox.TabStop = false;
            // 
            // MainForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(1243, 1061);
            this.Controls.Add(this.BaseSplitContainer);
            this.Font = new System.Drawing.Font("メイリオ", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.ShowIcon = false;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.Shown += new System.EventHandler(this.MainForm_Shown);
            this.LiveImagePanel.ResumeLayout(false);
            this.MainPanel.ResumeLayout(false);
            this.PTZCntlPanel.ResumeLayout(false);
            this.PTZButtonPanel.ResumeLayout(false);
            this.ConfigGroupBox.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.BaseSplitContainer.Panel1.ResumeLayout(false);
            this.BaseSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.BaseSplitContainer)).EndInit();
            this.BaseSplitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LiveImagePictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutPictureBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel LiveImagePanel;
        private System.Windows.Forms.PictureBox LiveImagePictureBox;
        private System.Windows.Forms.Timer MainTimer;
        private System.Windows.Forms.Label ClockLabel;
        private System.Windows.Forms.Panel MainPanel;
        private System.Windows.Forms.Label MainTitleLabel;
        private System.Windows.Forms.CheckBox AutoPatrolCheckBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label MaxTempLabel;
        private System.Windows.Forms.Label MaxPosLabel;
        private System.Windows.Forms.PictureBox LayoutPictureBox;
        private System.Windows.Forms.SplitContainer BaseSplitContainer;
        private System.Windows.Forms.Button AutoFocusButton;
        private System.Windows.Forms.GroupBox ConfigGroupBox;
        private System.Windows.Forms.RadioButton CameraSetupRadioButton;
        private System.Windows.Forms.RadioButton PresetRadioButton;
        private System.Windows.Forms.Label PatrolStatusLabel;
        private System.Windows.Forms.Button HomePositionMoveButton;
        private System.Windows.Forms.Button HomePositionSetButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton SpeedRadioButton5;
        private System.Windows.Forms.RadioButton SpeedRadioButton4;
        private System.Windows.Forms.RadioButton SpeedRadioButton3;
        private System.Windows.Forms.RadioButton SpeedRadioButton2;
        private System.Windows.Forms.RadioButton SpeedRadioButton1;
        private System.Windows.Forms.Button PTRightDownButton;
        private System.Windows.Forms.Button PTDownButton;
        private System.Windows.Forms.Button PTLeftDownButton;
        private System.Windows.Forms.Button PTRightButton;
        private System.Windows.Forms.Button PTHomeButton;
        private System.Windows.Forms.Button PTLeftButton;
        private System.Windows.Forms.Button PTLeftUpButton;
        private System.Windows.Forms.Button PTRightUpButton;
        private System.Windows.Forms.Button PTUpButton;
        private System.Windows.Forms.RadioButton AreaSetupRadioButton;
        private System.Windows.Forms.Label PanTiltPositionLabel;
        private System.Windows.Forms.Panel PTZButtonPanel;
        private System.Windows.Forms.Panel PTZCntlPanel;
        private System.Windows.Forms.RadioButton HistoryViewRadioButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label PanTiltTargetLabel;
        private System.Windows.Forms.RadioButton AreaRectSetupRadioButton;
        private System.Windows.Forms.Button SnapshotButton;
        private System.Windows.Forms.Button FocusFarButton;
        private System.Windows.Forms.Button FocusNearButton;
    }
}


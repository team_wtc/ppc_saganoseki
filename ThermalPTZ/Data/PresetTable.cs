﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text.RegularExpressions;
using System.Data;
using System.IO;
using System.Windows.Forms;
using System.Diagnostics;

namespace CES.ThermalPTZ.Data
{
    public sealed class PresetTable
    {
        public const int PRESET_NO_MIN = 1;
        public const int PRESET_NO_MAX = 60;

        const string DATASET_FILE = @".\Data\ThermalDataSet.xml";
        //const int MAX_HISTORY_COUNT = 10;
        //const int HISTORY_PRESET_NO_MIN = 71;

        readonly ThermalDataSet _ThermalDataSet;

        public PresetTable()
        {
            _ThermalDataSet = Load();
        }

        public void AddArea( string AreaName, int? Left = null, int? Top = null, int? Width = null, int? Height = null )
        {
            try
            {
                var NewAreaRow = _ThermalDataSet.AREA.NewAREARow();
                NewAreaRow.area_name = AreaName;

                if ( Left.HasValue == true )
                {
                    NewAreaRow.area_location_left = Left.Value;
                }
                else
                {
                    NewAreaRow.Setarea_location_leftNull();
                }
                if ( Top.HasValue == true )
                {
                    NewAreaRow.area_location_top = Top.Value;
                }
                else
                {
                    NewAreaRow.Setarea_location_topNull();
                }
                if ( Width.HasValue == true )
                {
                    NewAreaRow.area_width = Width.Value;
                }
                else
                {
                    NewAreaRow.Setarea_widthNull();
                }
                if ( Height.HasValue == true )
                {
                    NewAreaRow.area_height = Height.Value;
                }
                else
                {
                    NewAreaRow.Setarea_heightNull();
                }
                _ThermalDataSet.AREA.AddAREARow( NewAreaRow );
                Save();
            }
            catch
            {

            }
        }

        public void UpdateArea( int AreaId, string AreaName, int? Left = null, int? Top = null, int? Width = null, int? Height = null )
        {
            try
            {
                if ( _ThermalDataSet.AREA.FindByarea_id( AreaId ) is ThermalDataSet.AREARow AreaRow )
                {
                    AreaRow.area_name = AreaName;

                    if ( Left.HasValue == true )
                    {
                        AreaRow.area_location_left = Left.Value;
                    }
                    else
                    {
                        AreaRow.Setarea_location_leftNull();
                    }
                    if ( Top.HasValue == true )
                    {
                        AreaRow.area_location_top = Top.Value;
                    }
                    else
                    {
                        AreaRow.Setarea_location_topNull();
                    }
                    if ( Width.HasValue == true )
                    {
                        AreaRow.area_width = Width.Value;
                    }
                    else
                    {
                        AreaRow.Setarea_widthNull();
                    }
                    if ( Height.HasValue == true )
                    {
                        AreaRow.area_height = Height.Value;
                    }
                    else
                    {
                        AreaRow.Setarea_heightNull();
                    }
                    Save();
                }
            }
            catch
            {

            }
        }

        public void DeleteArea( int AreaId )
        {
            if ( _ThermalDataSet.AREA.FindByarea_id( AreaId ) is ThermalDataSet.AREARow AreaRow )
            {
                if ( _ThermalDataSet.SEQUENCE.Select( $"{_ThermalDataSet.SEQUENCE.seq_area_idColumn.ColumnName}={AreaId}" ) is ThermalDataSet.SEQUENCERow[] SeqRows )
                {
                    foreach ( var SeqRow in SeqRows )
                    {
                        SeqRow.Delete();
                    }
                }

                AreaRow.Delete();
                Save();
            }
        }

        public void UpdateAreaRect( int AreaId, int Left, int Top, int Width, int Height )
        {
            try
            {
                if ( _ThermalDataSet.AREA.FindByarea_id( AreaId ) is ThermalDataSet.AREARow AreaRow )
                {
                    AreaRow.area_location_left = Left;
                    AreaRow.area_location_top = Top;
                    AreaRow.area_width = Width;
                    AreaRow.area_height = Height;
                    Save();
                }
            }
            catch
            {

            }
        }

        public string AreaName( int AreaNo )
        {
            if ( _ThermalDataSet.AREA.FindByarea_id( AreaNo ) is ThermalDataSet.AREARow AreaRow )
            {
                return AreaRow.area_name;
            }

            return string.Empty;
        }

        public bool AreaRect( int AreaNo, out int Left, out int Top, out int Width, out int Height )
        {
            Left = 0;
            Top = 0;
            Width = 0;
            Height = 0;
            if ( _ThermalDataSet.AREA.FindByarea_id( AreaNo ) is ThermalDataSet.AREARow AreaRow )
            {
                if ( AreaRow.Isarea_location_leftNull() == true )
                {
                    return false;
                }
                Left = AreaRow.area_location_left;
                if ( AreaRow.Isarea_location_topNull() == true )
                {
                    return false;
                }
                Top = AreaRow.area_location_top;
                if ( AreaRow.Isarea_widthNull() == true )
                {
                    return false;
                }
                Width = AreaRow.area_width;
                if ( AreaRow.Isarea_heightNull() == true )
                {
                    return false;
                }
                Height = AreaRow.area_height;
                return true;
            }

            return false;
        }

        public ListViewItem[] CreateAreaListViewItem()
        {
            var Items = new List<ListViewItem>();
            var View = new DataView(
                _ThermalDataSet.AREA,
                string.Empty,
                _ThermalDataSet.AREA.area_idColumn.ColumnName,
                 DataViewRowState.CurrentRows );

            foreach ( DataRowView RowView in View )
            {
                if ( RowView.Row is ThermalDataSet.AREARow AreaRow )
                {
                    var Item = new ListViewItem( new string[]
                    {
                        string.Empty,
                        AreaRow.area_id.ToString(),
                        AreaRow.area_name,
                        (AreaRow.Isarea_location_leftNull()==false)?AreaRow.area_location_left.ToString():string.Empty,
                        (AreaRow.Isarea_location_topNull()==false)?AreaRow.area_location_top.ToString():string.Empty,
                        (AreaRow.Isarea_widthNull()==false)?AreaRow.area_width.ToString():string.Empty,
                        (AreaRow.Isarea_heightNull()==false)?AreaRow.area_height.ToString():string.Empty
                    } )
                    {
                        Tag = AreaRow
                    };
                    Items.Add( Item );
                }
            }

            return Items.ToArray();
        }

        public void AddAlarm( int Area, DateTime Timestamp, string AreaName, float Temperature, Image Snapshot )
        {
            try
            {
                var ImageFolder = Path.Combine( Application.StartupPath, "ALARM_IMAGE" );
                var ImagePath = Path.Combine( ImageFolder, $"{Timestamp.ToString( "yyyyMMdd_HHmmss" )}.jpg" );
                if ( Directory.Exists( ImageFolder ) == false )
                {
                    Directory.CreateDirectory( ImageFolder );
                }
                using ( var Img = new Bitmap( Snapshot ) )
                {
                    Img.Save( ImagePath, ImageFormat.Jpeg );
                }

                var AlarmRow = _ThermalDataSet.ALARM.NewALARMRow();
                AlarmRow.alm_area_id = Area;
                AlarmRow.alm_area_name = AreaName;
                AlarmRow.alm_image_path = ImagePath;
                AlarmRow.alm_temperature = Temperature;
                AlarmRow.alm_timestamp = Timestamp;

                _ThermalDataSet.ALARM.AddALARMRow( AlarmRow );
                Save();
            }
            catch (Exception e )
            {
                Trace.WriteLine( e );
            }
        }

        public bool LatestAlarm( out int AreaId, out DateTime Timestamp )
        {
            try
            {
                var View = new DataView(
                    _ThermalDataSet.ALARM,
                    string.Empty,
                    $"{_ThermalDataSet.ALARM.alm_timestampColumn.ColumnName} DESC",
                    DataViewRowState.CurrentRows );


                if ( ( View.Count > 0 ) && ( View[ 0 ].Row is ThermalDataSet.ALARMRow AlarmRow ) )
                {
                    AreaId = AlarmRow.alm_area_id;
                    Timestamp = AlarmRow.alm_timestamp;
                    return true;
                }
            }
            catch (Exception e )
            {
                Trace.WriteLine( e );
            }

            AreaId = 0;
            Timestamp = DateTime.MinValue;
            return false;
        }

        public void ChangeAlarmImage( int AlarmAreaId, DateTime AlarmTimestamp, Image AlarmImage )
        {
            if ( _ThermalDataSet.ALARM.FindByalm_area_idalm_timestamp( AlarmAreaId, AlarmTimestamp ) is ThermalDataSet.ALARMRow AlarmRow )
            {
                if ( File.Exists( AlarmRow.alm_image_path ) == true )
                {
                    int Index = 1;
                    string BaseFileName = Path.GetFileNameWithoutExtension( AlarmRow.alm_image_path );
                    string RenamePath = string.Empty;

                    while ( true )
                    {
                        string RenameFileName = $"{BaseFileName}({Index})";
                        RenamePath = AlarmRow.alm_image_path.Replace( BaseFileName, RenameFileName );

                        if ( File.Exists( RenamePath ) == false )
                        {
                            break;
                        }

                        Index++;
                    }

                    File.Move( AlarmRow.alm_image_path, RenamePath );
                }

                using ( var Img = new Bitmap( AlarmImage ) )
                {
                    Img.Save( AlarmRow.alm_image_path, ImageFormat.Jpeg );
                }
            }
        }

        public ListViewItem[] CreateAlarmListViewItem()
        {
            var Items = new List<ListViewItem>();
            var View = new DataView(
                _ThermalDataSet.ALARM,
                string.Empty,
                $"{_ThermalDataSet.ALARM.alm_timestampColumn.ColumnName} DESC",
                 DataViewRowState.CurrentRows );

            foreach ( DataRowView RowView in View )
            {
                if ( RowView.Row is ThermalDataSet.ALARMRow AlarmRow )
                {
                    var Item = new ListViewItem( new string[]
                    {
                        AlarmRow.alm_timestamp.ToString("yyyy年MM月dd日 HH時mm分ss秒"),
                        AlarmRow.alm_area_name,
                        $"{AlarmRow.alm_temperature:F1}℃"
                    } )
                    {
                        Tag = AlarmRow
                    };
                    Items.Add( Item );
                }
            }

            return Items.ToArray();
        }

        public void DeleteAlarm( int SaveDays )
        {
            DateTime SaveLimit = DateTime.Today.AddDays( -SaveDays );
            var ImageFolder = Path.Combine( Application.StartupPath, "ALARM_IMAGE" );

            //  2020/09/22  古い警報の削除ロジックを修正  H.Onuki
            List<ThermalDataSet.ALARMRow> DeleteAlarmList = new List<ThermalDataSet.ALARMRow>();

            foreach ( var AlarmRow in _ThermalDataSet.ALARM )
            {
                if ( AlarmRow.alm_timestamp < SaveLimit )
                {
                    try
                    {
                        File.Delete( AlarmRow.alm_image_path );
                    }
                    catch ( Exception e )
                    {
                        Trace.WriteLine( e );
                        //continue;
                    }

                    DeleteAlarmList.Add( AlarmRow );
                    //AlarmRow.Delete();
                }
            }

            foreach ( var Row in DeleteAlarmList )
            {
                Row.Delete();
            }

            Save();
        }

        public KeyValuePair<int, string>[] CreateAreaComboItem()
        {
            var Items = new List<KeyValuePair<int, string>>();
            var View = new DataView(
                _ThermalDataSet.AREA,
                string.Empty,
                _ThermalDataSet.AREA.area_idColumn.ColumnName,
                 DataViewRowState.CurrentRows );

            foreach ( DataRowView RowView in View )
            {
                if ( RowView.Row is ThermalDataSet.AREARow AreaRow )
                {
                    Items.Add( new KeyValuePair<int, string>( AreaRow.area_id, AreaRow.area_name ) );
                }
            }

            return Items.ToArray();
        }

        public ListViewItem[] CreatePatrolListViewItem( int AreaNo )
        {
            var Items = new List<ListViewItem>();
            var View = new DataView(
                _ThermalDataSet.SEQUENCE,
                $"{_ThermalDataSet.SEQUENCE.seq_area_idColumn.ColumnName}={AreaNo}",
                _ThermalDataSet.SEQUENCE.seq_noColumn.ColumnName,
                 DataViewRowState.CurrentRows );

            foreach ( DataRowView RowView in View )
            {
                if ( RowView.Row is ThermalDataSet.SEQUENCERow SeqRow )
                {
                    var Item = new ListViewItem( new string[]
                    {
                        string.Empty,
                        SeqRow.seq_area_id.ToString(),
                        SeqRow.seq_no.ToString(),
                        SeqRow.seq_preset_no.ToString(),
                        $"{SeqRow.seq_preset_pan:F2}",
                        $"{SeqRow.seq_preset_tilt:F2}",
                        (SeqRow.seq_high_speed_move==true)?"高速移動":"低速移動",
                        (SeqRow.seq_auto_focus==true)? "実行する":"実行しない",
                        (SeqRow.Isseq_auto_focus_wait_timeNull()==true)?"0":$"{SeqRow.seq_auto_focus_wait_time}"
                    } )
                    {
                        Tag = SeqRow
                    };
                    Items.Add( Item );
                }
            }

            return Items.ToArray();
        }

        public bool AddPatrolPreset( int AreaNo, int PresetNumber, float Pan, float Tilt,  bool HighSpeed, bool AutoFocus, int AutoFocusWaitTime )
        {
            if ( ( Pan.Equals( float.NaN ) == true ) || ( Tilt.Equals( float.NaN ) == true ) )
            {
                return false;
            }

            try
            {
                var SeqRow = _ThermalDataSet.SEQUENCE.NewSEQUENCERow();
                SeqRow.seq_area_id = AreaNo;
                SeqRow.seq_no = NextPatrolNumber( AreaNo );
                SeqRow.seq_preset_no = PresetNumber;
                SeqRow.seq_high_speed_move = HighSpeed;
                SeqRow.seq_auto_focus = AutoFocus;
                SeqRow.seq_preset_pan = Pan;
                SeqRow.seq_preset_tilt = Tilt;
                SeqRow.seq_auto_focus_wait_time = AutoFocusWaitTime;
                _ThermalDataSet.SEQUENCE.AddSEQUENCERow( SeqRow );
                Save();

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool FindPreset( int PresetNumber )
        {
            try
            {
                if ( ( _ThermalDataSet.SEQUENCE.Select(
                    $"{_ThermalDataSet.SEQUENCE.seq_preset_noColumn.ColumnName}={PresetNumber}" ) is DataRow[] Rows ) &&
                    ( Rows.Length > 0 ) )
                {
                    return true;
                }
            }
            catch
            {
            }
            return false;
        }

        public void UpdatePatrolPreset( int AreaNo, int PatrolNumber, int PresetNumber, float Pan, float Tilt )
        {
            if ( _ThermalDataSet.SEQUENCE.FindByseq_area_idseq_no( AreaNo, PatrolNumber ) is ThermalDataSet.SEQUENCERow SeqRow )
            {
                SeqRow.seq_preset_no = PresetNumber;
                SeqRow.seq_preset_pan = Pan;
                SeqRow.seq_preset_tilt = Tilt;
                Save();
            }
        }

        public void UpdatePatrolInfo( int AreaNo, int PatrolNumber, int PresetNumber, bool HighSpeed, bool AutoFocus, int AutoFocusWaitTime )
        {
            if ( _ThermalDataSet.SEQUENCE.FindByseq_area_idseq_no( AreaNo, PatrolNumber ) is ThermalDataSet.SEQUENCERow SeqRow )
            {
                SeqRow.seq_high_speed_move = HighSpeed;
                SeqRow.seq_auto_focus = AutoFocus;
                SeqRow.seq_auto_focus_wait_time = AutoFocusWaitTime;
                if ( SeqRow.seq_preset_no != PresetNumber )
                {
                    if ( _ThermalDataSet.SEQUENCE.Select( $"{_ThermalDataSet.SEQUENCE.seq_preset_noColumn.ColumnName}={PresetNumber}" ) is ThermalDataSet.SEQUENCERow[] PresetRows )
                    {
                        SeqRow.seq_preset_pan = PresetRows[ 0 ].seq_preset_pan;
                        SeqRow.seq_preset_tilt = PresetRows[ 0 ].seq_preset_tilt;
                    }
                    else
                    {
                        SeqRow.seq_preset_pan = 0;
                        SeqRow.seq_preset_tilt = 0;
                    }

                    SeqRow.seq_preset_no = PresetNumber;
                }
                Save();
            }
        }

        public void DeletePatrol( int AreaNo, int PatrolNumber )
        {
            if ( _ThermalDataSet.SEQUENCE.FindByseq_area_idseq_no( AreaNo, PatrolNumber ) is ThermalDataSet.SEQUENCERow SeqRow )
            {
                SeqRow.Delete();
                Save();
            }
        }

        public void ExchangePatrol( int AreaNo, int SequenceNumber, bool Prev )
        {
            if ( _ThermalDataSet.SEQUENCE.FindByseq_area_idseq_no( AreaNo, SequenceNumber ) is ThermalDataSet.SEQUENCERow SeqRow )
            {
                var TargetNumber = ( Prev == true ) ? ( SequenceNumber - 1 ) : ( SequenceNumber + 1 );

                if ( _ThermalDataSet.SEQUENCE.FindByseq_area_idseq_no( AreaNo, TargetNumber ) is ThermalDataSet.SEQUENCERow TargetSeqRow )
                {
                    var TmpPresetNo = TargetSeqRow.seq_preset_no;
                    var TmpFastSpeed = TargetSeqRow.seq_high_speed_move;
                    var TmpAutoFocus = TargetSeqRow.seq_auto_focus;
                    var TmpPresetPan = TargetSeqRow.seq_preset_pan;
                    var TmpPresetTilt = TargetSeqRow.seq_preset_tilt;
                    var TmpAutoFocusWaitTime = ( TargetSeqRow.Isseq_auto_focus_wait_timeNull() == true ) ? 0 : TargetSeqRow.seq_auto_focus_wait_time;

                    TargetSeqRow.seq_preset_no = SeqRow.seq_preset_no;
                    TargetSeqRow.seq_high_speed_move = SeqRow.seq_high_speed_move;
                    TargetSeqRow.seq_auto_focus = SeqRow.seq_auto_focus;
                    TargetSeqRow.seq_preset_pan = SeqRow.seq_preset_pan;
                    TargetSeqRow.seq_preset_tilt = SeqRow.seq_preset_tilt;
                    if ( SeqRow.Isseq_auto_focus_wait_timeNull() == true )
                    {
                        TargetSeqRow.seq_auto_focus_wait_time = 0;
                    }
                    else
                    {
                        TargetSeqRow.seq_auto_focus_wait_time = SeqRow.seq_auto_focus_wait_time;
                    }

                    SeqRow.seq_preset_no = TmpPresetNo;
                    SeqRow.seq_high_speed_move = TmpFastSpeed;
                    SeqRow.seq_auto_focus = TmpAutoFocus;
                    SeqRow.seq_preset_pan = TmpPresetPan;
                    SeqRow.seq_preset_tilt = TmpPresetTilt;
                    SeqRow.seq_auto_focus_wait_time = TmpAutoFocusWaitTime;

                    Save();
                }
            }
        }

        //public bool SetAreaTopSequence( int AreaNo )
        //{
        //    try
        //    {
        //        var View = new DataView(
        //            _ThermalDataSet.SEQUENCE,
        //            $"{_ThermalDataSet.SEQUENCE.seq_area_idColumn.ColumnName}={AreaNo}",
        //            _ThermalDataSet.SEQUENCE.seq_noColumn.ColumnName,
        //             DataViewRowState.CurrentRows );

        //        if ( View.Count > 0 )
        //        {
        //            if ( View[ 0 ].Row is ThermalDataSet.SEQUENCERow SeqRow )
        //            {
        //                if ( _ThermalDataSet.AREA.FindByarea_id( AreaNo ) is ThermalDataSet.AREARow AreaRow )
        //                {
        //                    AreaRow.area_top_seq = SeqRow.seq_no;
        //                }
        //            }

        //            Save();
        //            return true;
        //        }
        //    }
        //    catch
        //    {
        //    }

        //    return false;
        //}

        public int NextPatrolNumber( int AreaNo )
        {
            if ( _ThermalDataSet.SEQUENCE.Count <= 0 )
            {
                return 1;
            }

            try
            {
                if ( _ThermalDataSet.SEQUENCE.Compute(
                    $"MAX({_ThermalDataSet.SEQUENCE.seq_noColumn.ColumnName})",
                    $"{_ThermalDataSet.SEQUENCE.seq_area_idColumn.ColumnName}={AreaNo}" ) is int MaxNumber )
                {
                    return ( MaxNumber + 1 );
                }

                return 1;
            }
            catch ( Exception e )
            {
                MessageBox.Show(
                    $"Dataset Query Error({e.Message})",
                    "Dataset Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error );
                return -1;
            }
        }

        public Dictionary<int, List< AutoPatrolInfo>> AutoPatrolTable()
        {
            try
            {
                var Items = new Dictionary<int, List<AutoPatrolInfo>>();

                var View = new DataView(
                    _ThermalDataSet.SEQUENCE,
                    string.Empty,
                    $"{_ThermalDataSet.SEQUENCE.seq_area_idColumn.ColumnName},{_ThermalDataSet.SEQUENCE.seq_noColumn.ColumnName}",
                     DataViewRowState.CurrentRows );

                foreach ( DataRowView RowView in View )
                {
                    if ( RowView.Row is ThermalDataSet.SEQUENCERow SeqRow )
                    {
                        if ( Items.ContainsKey( SeqRow.seq_area_id ) == false )
                        {
                            Items[ SeqRow.seq_area_id ] = new List<AutoPatrolInfo>();
                        }

                        Items[ SeqRow.seq_area_id ].Add( new AutoPatrolInfo(
                            SeqRow.seq_area_id,
                            SeqRow.seq_no,
                            SeqRow.seq_preset_no,
                            SeqRow.seq_preset_pan,
                            SeqRow.seq_preset_tilt,
                            SeqRow.seq_high_speed_move,
                            SeqRow.seq_auto_focus,
                            ( SeqRow.Isseq_auto_focus_wait_timeNull() == true ) ? 0 : SeqRow.seq_auto_focus_wait_time ) );
                    }
                }

                return Items;
            }
            catch
            {
                return new Dictionary<int, List<AutoPatrolInfo>>();
            }

        }

        private ThermalDataSet Load()
        {
            ThermalDataSet ds = new ThermalDataSet();
            if ( File.Exists( DATASET_FILE ) == false )
            {
                return ds;
            }

            try
            {
                ds.ReadXml( DATASET_FILE );
                return ds;
            }
            catch ( Exception e )
            {
                MessageBox.Show(
                    $"データファイル[{DATASET_FILE}]の読み込みに失敗しました。({e.Message})",
                    "データファイル読込",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error );
                ds?.Dispose();
                return new ThermalDataSet();
            }
        }

        private void Save()
        {
            var Dir = Path.GetDirectoryName( DATASET_FILE );
            if ( Directory.Exists( Dir ) == false )
            {
                try
                {
                    Directory.CreateDirectory( Dir );
                }
                catch ( Exception e )
                {
                    MessageBox.Show(
                        $"データディレクトリ[{Dir}]を作成できません。({e.Message})",
                        "ディレクトリ作成",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error );
                    return;
                }
            }

            if ( _ThermalDataSet == null )
            {
                return;
            }

            try
            {
                //  2020/09/22  データファイル書き込みロジックを修正  H.Onuki
                _ThermalDataSet.AcceptChanges();
                _ThermalDataSet.WriteXml( DATASET_FILE );
                //_ThermalDataSet.AcceptChanges();
            }
            catch ( Exception e )
            {
                MessageBox.Show(
                    $"データファイル[{DATASET_FILE}]の書き込みに失敗しました。({e.Message})",
                    "データファイル保存",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error );
            }
        }
    }

    public class AutoPatrolInfo
    {
        public AutoPatrolInfo( int Area, int Seq, int Preset, float PresetPan, float PresetTilt,  bool Fast, bool PresetAutoFocus, int PresetAutoFocusWaitTime )
        {
            AreaNumber = Area;
            SequenceNumber = Seq;
            PresetNumber = Preset;
            FastMove = Fast;
            AutoFocus = PresetAutoFocus;
            Pan = PresetPan;
            Tilt = PresetTilt;
            AutoFocusWaitTime = PresetAutoFocusWaitTime;
        }

        public int AreaNumber
        {
            get;
            private set;
        }

        public int SequenceNumber
        {
            get;
            private set;
        }

        public int PresetNumber
        {
            get;
            private set;
        }

        public bool FastMove
        {
            get;
            private set;
        }

        public bool AutoFocus
        {
            get;
            private set;
        }

        public float Pan
        {
            get;
            private set;
        }

        public float Tilt
        {
            get;
            private set;
        }

        public int AutoFocusWaitTime
        {
            get;
            private set;
        }
    }
}

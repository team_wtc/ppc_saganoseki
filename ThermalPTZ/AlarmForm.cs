﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CES.ThermalPTZ
{
    public partial class AlarmForm : Form
    {
        readonly Data.PresetTable _PresetTable = null;

        public AlarmForm()
        {
            InitializeComponent();
        }

        public AlarmForm( Data.PresetTable PresetTable )
        {
            InitializeComponent();
            _PresetTable = PresetTable;
        }

        private void AlarmForm_Load( object sender, EventArgs e )
        {
            if ( _PresetTable != null )
            {
                AlarmListView.Items.AddRange( _PresetTable.CreateAlarmListViewItem() );
                if ( AlarmListView.Items.Count > 0 )
                {
                    AlarmListView.Items[ 0 ].Selected = true;
                }
            }
        }

        protected override bool ProcessCmdKey( ref Message msg, Keys keyData )
        {
            if ( keyData == Keys.F5 )
            {
                AlarmListView.Items.Clear();

                if ( _PresetTable != null )
                {
                    AlarmListView.Items.AddRange( _PresetTable.CreateAlarmListViewItem() );
                    if ( AlarmListView.Items.Count > 0 )
                    {
                        AlarmListView.Items[ 0 ].Selected = true;
                    }
                }
            }
            return base.ProcessCmdKey( ref msg, keyData );
        }

        private void AlarmListView_SelectedIndexChanged( object sender, EventArgs e )
        {
            if ( AlarmListView.SelectedItems.Count <= 0 )
            {
                AlarmImagePictureBox.Image?.Dispose();
                AlarmImagePictureBox.Image = null;
                return;
            }

            if ( AlarmListView.SelectedItems[ 0 ].Tag is Data.ThermalDataSet.ALARMRow AlarmRow )
            {
                AlarmImagePictureBox.Image?.Dispose();
                if ( System.IO.File.Exists( AlarmRow.alm_image_path ) == true )
                {
                    using ( var Img = Image.FromFile( AlarmRow.alm_image_path ) )
                    {
                        AlarmImagePictureBox.Image = new Bitmap( Img );
                    }
                }
            }
        }

        private void AlarmForm_FormClosing( object sender, FormClosingEventArgs e )
        {
            AlarmImagePictureBox.Image?.Dispose();
        }
    }
}

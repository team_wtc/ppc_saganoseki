﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace CES.ThermalPTZ.Images
{
    public class PanTiltImage
    {
        public enum DRAW_TARGET
        {
            PAN,
            TILT,
            LAYOUT
        }
        const string CAMERA_PAN_IMAGE = @".\Images\cox_camera_top_small.png";
        const string CAMERA_PAN_BACK_IMAGE = @".\Images\cox_camera_top_back.png";
        const string CAMERA_TILT_IMAGE = @".\Images\cox_camera_side_small.png";
        const string CAMERA_TILT_BACK_IMAGE = @".\Images\cox_camera_side_back.png";
        const string LAYOUT_CAMERA_IMAGE = @".\Images\layout_camera.png";
        const string LAYOUT_BACK_IMAGE = @".\Images\layout.jpg";

        readonly Bitmap _BaseImage;
        readonly Bitmap _BackImage;

        readonly DRAW_TARGET _DrawTarget;
        readonly Color _TransparentColor;
        readonly int _CenterX;
        readonly int _CenterY;
        readonly int _DrawPosX;
        readonly int _DrawPosY;

        public PanTiltImage( DRAW_TARGET Target )
        {
            _DrawTarget = Target;

            switch ( _DrawTarget )
            {
                case DRAW_TARGET.PAN:
                    _BaseImage = Image.FromFile( CAMERA_PAN_IMAGE ) as Bitmap;
                    _BackImage = Image.FromFile( CAMERA_PAN_BACK_IMAGE ) as Bitmap;
                    _CenterX = _BackImage.Width / 2;
                    _CenterY = _BackImage.Height / 2;
                    _DrawPosX = 0;
                    _DrawPosY = 0;
                    break;
                case DRAW_TARGET.TILT:
                    _BaseImage = Image.FromFile( CAMERA_TILT_IMAGE ) as Bitmap;
                    _BackImage = Image.FromFile( CAMERA_TILT_BACK_IMAGE ) as Bitmap;
                    _CenterX = _BackImage.Width / 2;
                    _CenterY = _BackImage.Height / 2;
                    _DrawPosX = 0;
                    _DrawPosY = 0;
                    break;
                case DRAW_TARGET.LAYOUT:
                    _BaseImage = Image.FromFile( LAYOUT_CAMERA_IMAGE ) as Bitmap;
                    _BackImage = Image.FromFile( LAYOUT_BACK_IMAGE ) as Bitmap;
                    _CenterX = Properties.Settings.Default.LayoutCameraPositionX;
                    _CenterY = Properties.Settings.Default.LayoutCameraPositionY;
                    _DrawPosX = Properties.Settings.Default.LayoutCameraPositionX;
                    _DrawPosY = Properties.Settings.Default.LayoutCameraPositionY
                        - _BaseImage.Height;
                    break;
            }
            _TransparentColor = _BaseImage.GetPixel( 0, 0 );
            _BaseImage.MakeTransparent( _TransparentColor );
        }

        public Color BkColor
        {
            get;
            set;
        } = Color.Blue;

        public Bitmap RotateImage( float Angle )
        {
            var NewImage = new Bitmap( _BackImage );

            if ( Angle.Equals( float.NaN ) == false )
            {
                float AdjustAngle = Angle + ( ( _DrawTarget == DRAW_TARGET.LAYOUT ) ? Properties.Settings.Default.LayoutCameraAdjust : 0F );
                using ( var Graph = Graphics.FromImage( NewImage ) )
                using ( var BackBrush = new SolidBrush( BkColor ) )
                {
                    //Graph.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    //Graph.CompositingQuality = CompositingQuality.GammaCorrected;

                    //Graph.FillRectangle( BackBrush, new Rectangle( 0, 0, _ScaleWidth, _ScaleHeight ) );

                    Graph.TranslateTransform( -_CenterX, -_CenterY );
                    Graph.RotateTransform( AdjustAngle, MatrixOrder.Append );
                    Graph.TranslateTransform( _CenterX, _CenterY, MatrixOrder.Append );

                    Graph.DrawImageUnscaled( _BaseImage, _DrawPosX, _DrawPosY );
                }
            }

            return NewImage;
        }

        public Bitmap AreaImage( float Angle, int AreaLeft, int AreaTop, int AreaWidth, int AreaHeight )
        {
            var NewImage = new Bitmap( _BackImage );
            float AdjustAngle = Angle + ( ( _DrawTarget == DRAW_TARGET.LAYOUT ) ? Properties.Settings.Default.LayoutCameraAdjust : 0F );

            using ( var Graph = Graphics.FromImage( NewImage ) )
            using ( var AreaBrush = new SolidBrush( Color.FromArgb( 128, BkColor.R, BkColor.G, BkColor.B ) ) )
            {
                Graph.InterpolationMode = InterpolationMode.HighQualityBicubic;
                Graph.CompositingQuality = CompositingQuality.GammaCorrected;

                Graph.FillRectangle( AreaBrush, new Rectangle( AreaLeft, AreaTop, AreaWidth, AreaHeight ) );

                if ( Angle.Equals( float.NaN ) == false )
                {
                    Graph.TranslateTransform( -_CenterX, -_CenterY );
                    Graph.RotateTransform( AdjustAngle, MatrixOrder.Append );
                    Graph.TranslateTransform( _CenterX, _CenterY, MatrixOrder.Append );
                    Graph.DrawImageUnscaled( _BaseImage, _DrawPosX, _DrawPosY );
                }
            }

            return NewImage;
        }

        public Bitmap BaseImage()
        {
            return new Bitmap( _BackImage );
        }
    }
}
